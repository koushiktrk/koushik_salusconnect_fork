/*jslint jasmine: true */
(function (_) {
	'use strict';

	var key,
		jasmineSpecificRequirePaths = {
			"jasmine": "../../../tests/js/lib/jasmine/jasmine",
			"jasmine-html": "../../../tests/js/lib/jasmine/jasmine-html",
			"jasmine-boot": "../../../tests/js/lib/jasmine/boot",
			"spec": "../../../tests/js/spec"
		},
		specs = [
			"spec/appSpec",
			"spec/routerSpec",
			"spec/util/CoreValidatorSpec",
			"spec/model/api/SessionManagerSpec",
			"spec/model/api/apiConnectorSpec",
			"spec/model/ayla/UserSpec",
			"spec/model/ayla/DeviceSpec",
			"spec/model/ayla/DeviceCollectionSpec",
			"spec/model/ayla/AddressSpec",
			"spec/model/ayla/GroupSpec",
			"spec/model/ayla/GroupCollectionSpec"
		],
		jasmineSpecificRequireConfig = {
			"shim": {
				"jasmine-html": {
					"deps": ["jasmine"]
				},
				"jasmine-boot": {
					"deps": ["jasmine", "jasmine-html"]
				}
			},
			baseUrl: ".." + window.requireConfig.baseUrl
		},
		config = window.requireConfig;

	for (key in jasmineSpecificRequireConfig) {
		if (jasmineSpecificRequireConfig.hasOwnProperty(key)) {
			var jasmineValue = jasmineSpecificRequireConfig[key];

			if (typeof jasmineValue === 'string') {
				config[key] = jasmineValue;
			} else {
				config[key] = _.extend(config[key], jasmineValue);
			}
		}
	}

	for (key in jasmineSpecificRequirePaths) {
		if (jasmineSpecificRequirePaths.hasOwnProperty(key)) {
			config.paths = config.paths || {};
			config.paths[key] = jasmineSpecificRequirePaths[key];
		}
	}

	require.config(config);

	require(["jquery", "jasmine", "jasmine-html", "backbone.advice", "spec/SpecHelper"], function () {
		require(specs, function () {
			window.onload();
		});
	});
})(window._);