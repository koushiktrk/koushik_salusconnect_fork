/*jslint jasmine: true */
define([
	"spec/SpecHelper",
	"common/AylaConfig",
	"common/model/api/SessionManager",
	"common/util/CookieMgr"
], function (SpecHelper, AylaConfig, SessionManager, CookieMgr) {
	'use strict';

	describe("Session Manager Spec", function () {
		var sm;

		beforeEach(function () {
			sm = new SessionManager();
			SpecHelper.ajax.setup();
		});

		afterEach(function () {
			SpecHelper.ajax.cleanup();
			sm = null;
		});

		it("Expect the session manager to be constructable", function () {
			expect(sm).not.toBeNull();
		});

		it("Expect login to call ajax and load results", function (done) {
			SpecHelper.ajax.registerHandler("post", AylaConfig.endpoints.login,
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("POST");
					expect(data).not.toBeNull();
					data = JSON.parse(data);
					expect(data.user).not.toBeNull();
					expect(data.user.email).toBe("joe@example.com");
					expect(data.user.password).toBe("password1");
					expect(data.user.application).not.toBeNull();

					success({
						"access_token": "access_token_here",
						"refresh_token": "refresh_token_here",
						"expires_in": 86400,
						"role": "EndUser",
						"role_tags": []
					});
				});

			sm.loginWithUsername("joe@example.com", "password1");

			setTimeout(function () {
				expect(sm.get("accessToken")).toBe("access_token_here");
				expect(sm.get("refreshToken")).toBe("refresh_token_here");
				done();
			}, 1);
		});

		//todo confirm put, move to session mgr spec
		it("expect confirm with token API call to put appropriate data", function (done) {
			SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.auth.confirmUserToken,
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("PUT");
					expect(data).not.toBeNull();
					expect(data.confirmation_token).toBe("my>token");

					success({ // based on API response Jul 6. 2015
						admin: false,
						approved: true,
						country: "USA",
						created_at: "2015-07-06T20:38:39Z",
						email: "salus@digitalfoundry.com",
						firstname: "df",
						id: 13885,
						lastname: "salus",
						oem_approved: false,
						origin_oem_id: 213,
						terms_accepted: false,
						terms_accepted_at: "2015-07-06T20:38:39Z",
						updated_at: "2015-07-06T20:43:23Z"
					});
				});

			sm.confirmUserWithEmailVerificationToken("my>token").then(
				function (success) {
					done(); // we expect this to get called
				},
				function (fail) {
					jasmine.fail();
				}
			);
		});

		it("expect request emailed password reset token to post the proper data", function (done) {
			SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.auth.mgPassword,
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("POST");
					expect(raw.type).toBe("post");
					expect(data).toBeTruthy();
					expect(data.user.email).toBe("foo");

					success();

				});

			sm.sendResetPasswordToken("foo").then(
				function (success) {
					done(); // we expect this to get called
				},
				function (fail) {
					jasmine.fail();
				}
			);
		});

		it("expect reset password with emailed password reset token to post the proper data", function (done) {
			SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.auth.mgPassword,
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("PUT");
					expect(raw.type).toBe("put");
					expect(data).toBeTruthy();
					expect(data.user.reset_password_token).toBe("foo");
					expect(data.user.password).toBe("bar");
					expect(data.user.password_confirmation).toBe("bar");

					success();
				});

			sm.setPasswordWithToken("bar", "bar", "foo").then(
				function (success) {
					done(); // we expect this to get called
				},
				function (fail) {
					jasmine.fail();
				}
			);
		});
		
// TODO FIX THIS
//		it("should log the user out if a bad token is given", function () {
//			SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.refreshToken,
//				function (proto, url, verb, data, success, fail, raw) {
//					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
//					expect(verb).toBe("POST");
//					debugger;
//
//					fail({});
//			});
//
//			var sessionMgr = new SessionManager();
//			debugger;
//			sessionMgr.start();
//		});
	});
});