/*jslint jasmine: true */
define([
	"underscore",
	"spec/SpecHelper",
	"common/AylaConfig",
	"common/model/api/SalusConnector",
	"common/model/ayla/DeviceCollection"
], function (_, SpecHelper, AylaConfig, SalusConnector, AylaDeviceCollection) {
	'use strict';

	var cannedWeatherJson = {
		weather: [
			{
				id: "555",
				main: "Rain",
				description: "Its a swimming pool outside"
			}
		],
		main: {
			temp: 200
		},
		wind: {
			speed: "fast"
		},
		clouds: {
			all: "very cloudy"
		}
	};

	var addDefaultHandlers = function () {
		SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.metaData.fetch)({"dsn": "VXSA00000000003"}),
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("GET");

					success({"datum": { "value": "{}"}});
				});

		SpecHelper.ajax.registerHandler("ajax", encodeURIComponent(_.template(AylaConfig.endpoints.device.weather)({"dsn": "VXSA00000000003"})),
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("GET");

					success(cannedWeatherJson);
				});

		SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.metaData.fetch)({"dsn": "VXSA00000000001"}),
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("GET");

					success({"datum": { "value": "{}"}});
				});

		SpecHelper.ajax.registerHandler("ajax", encodeURIComponent(_.template(AylaConfig.endpoints.device.weather)({"dsn": "VXSA00000000001"})),
				function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("GET");

					success(cannedWeatherJson);
				});
	};

	describe("Ayla Device Collection ", function () {
		describe("when loading", function () {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});

			it("expect load() to load all devices", function (done) {
				var deviceObj = {
					"product_name": "My Gateway 1",
					"model": "TEST",
					"dsn": "VXSA00000000003",
					"oem_model": "OTEST",
					"template_id": null,
					"mac": "abcd12345603",
					"lan_ip": null,
					"connected_at": null,
					"key": 55219,
					"lan_enabled": false,
					"has_properties": true,
					"product_class": "",
					"connection_status": null,
					"lat": null,
					"lng": null,
					"device_type": "Wifi"
				};

				var compareObj = {
					GatewaySoftwareVersion: null,
					GatewayHardwareVersion: null,
					NetworkWiFiMAC: null,
					NetworkWiFiIP: null,
					NetworkLANMAC: null,
					NetworkLANIP: null,
					NetworkSSID: null,
					NetworkPassword: null,
					ErrorGatewayUART: null,
					SetNetworkSSID: null,
					SetNetworkPassword: null
				};

				_.extend(compareObj, deviceObj);

				SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.device.list,
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("GET");

						success(// based on API call to ayla Tue, 07 Jul 2015 23:29:54 GMT
							[
								{
									"device": deviceObj
								}
							]
						);
					});

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.metaData.fetch)({"dsn": "VXSA00000000003"}),
						function (proto, url, verb, data, success, fail, raw) {
							expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
							expect(verb).toBe("GET");

							success({"datum": { "value": "{}"}});
						});

				SpecHelper.ajax.registerHandler("ajax", encodeURIComponent(_.template(AylaConfig.endpoints.device.weather)({"dsn": "VXSA00000000003"})),
						function (proto, url, verb, data, success, fail, raw) {
							expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
							expect(verb).toBe("GET");

							success(cannedWeatherJson);
						});

				var deviceCollection = new AylaDeviceCollection();

				deviceCollection.load().then(function () {
					expect(deviceCollection.first()).toMatchObjectPropertiesExcept(compareObj, ["address", "weather"]);
					expect(_.keys(deviceCollection.first()._listeners).length).toBe(2);
					done();
				});
			});

		});

		describe("When removing a device", function () {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});

			it("expect the device to be removed from the collection and listeners to be cleaned up", function (done) {
				var deviceObjs = [
					{
						device: {
							"product_name": "My Gateway 1",
							"model": "TEST",
							"dsn": "VXSA00000000003",
							"key": 55219,
							"oem_model": "OTEST",
							"template_id": null
						}
					},
					{
						device: {
							"product_name": "My Gateway 0",
							"model": "TEST",
							"dsn": "VXSA00000000001",
							"key": 55217,
							"oem_model": "OTEST",
							"template_id": null
						}
					}
				];

				addDefaultHandlers();

				SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.device.list,
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("GET");

						success(// based on API call to ayla Tue, 07 Jul 2015 23:29:54 GMT
							deviceObjs
						);
					});

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.remove)({id: 55219}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("DELETE");

						success();
					});

				var deviceCollection = new AylaDeviceCollection();

				deviceCollection.refresh().then(function (result) {
					expect(deviceCollection.length).toBe(2);

					var first = deviceCollection.first(),
						last = deviceCollection.last();

					expect(first.get("key")).toBe(55219);
					expect(_.keys(first._listeners).length).toBe(2);

					expect(last.get("key")).toBe(55217);
					expect(_.keys(last._listeners).length).toBe(2);

					first.unregister();

					window.setTimeout(function () {
						expect(deviceCollection.length).toBe(1);
						expect(_.keys(first._listeners).length).toBe(1);
						done();
					}, 1);
				});
			});
		});


		describe("When loaded", function() {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});



			it("Should expect getGateways to return only gateways", function (done) {
				var deviceObjs = [
					{
						device: {
							"product_name": "My Gateway 1",
							"model": "TEST",
							"dsn": "VXSA00000000003",
							"key": 55219,
							"oem_model": "TTEST",
							"template_id": null
						}
					},
					{
						device: {
							"product_name": "My Gateway 0",
							"model": "TEST",
							"dsn": "VXSA00000000001",
							"key": 55217,
							"oem_model": "OTEST",
							"template_id": null
						}
					}
				];

				addDefaultHandlers();

				SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.device.list,
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("GET");

						success(// based on API call to ayla Tue, 07 Jul 2015 23:29:54 GMT
							deviceObjs
						);
					});

				var deviceCollection = new AylaDeviceCollection();

				deviceCollection.refresh().then(function (result) {
					expect(deviceCollection.length).toBe(2);

					var gateways = deviceCollection.getGateways();
					expect(gateways.length).toBe(1);
					done();
				});
			});


		});

	});
});