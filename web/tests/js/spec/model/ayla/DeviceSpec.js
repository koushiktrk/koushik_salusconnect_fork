/*jslint jasmine: true */
define([
	"app",
	"underscore",
	"backbone",
	"spec/SpecHelper",
	"common/AylaConfig",
	"common/model/api/SalusConnector",
	"common/model/ayla/deviceTypes/DefaultAylaDevice"
], function (App, _, B, SpecHelper, AylaConfig, SaluConnector, TestDevice) {
	'use strict';

	describe("Ayla Device ", function () {
		describe("When loading", function () {
			beforeEach(function (done) {
				SpecHelper.ajax.setup();
				App.start();

				require(["common/model/api/SalusConnector"], function (SalusConnector) {

					App.salusConnector = new SalusConnector();
					B.history.start();

					done();
				});
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});

			it("expect calling loadDetails() to load data properly", function (done) {
				var deviceObj = {
					"id": 55219,
					"product_name": "My Gateway 1",
					"model": "TEST",
					"dsn": "VXSA00000000003",
					"oem": "b15b3f53",
					"oem_model": "OTEST",
					"sw_version": "unknown",
					"user_id": 12590,
					"template_id": null,
					"mac": "abcd12345603",
					"ip": null,
					"lan_ip": null,
					"ssid": null,
					"connected_at": null,
					"key": 55219,
					"product_class": "",
					"has_properties": true,
					"lan_enabled": false,
					"enable_ssl": false,
					"ans_enabled": true,
					"ans_server": "ans.aylanetworks.com",
					"log_enabled": false,
					"registered": true,
					"connection_status": null,
					"registration_type": "Dsn",
					"lat": null,
					"lng": null,
					"homekit": null,
					"module_updated_at": null,
					"registrable": true,
					"regtoken": "2a6b8e",
					"provisional": true,
					"device_type": "Wifi"
				};

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.detail)({id: 56}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("GET");

						success(// based on API call to ayla Tue, 07 Jul 2015 23:29:54 GMT
							{
								"device": deviceObj
							}
						);
					});

				var device = new TestDevice({key: 56});
				expect(device).propEquals("key", 56);

				device.refresh().then(function (result) {
					expect(device).toMatchObjectPropertiesExcept(deviceObj, ["address"]);
					done();
				});
			});

		});

		describe("When adding a device", function () {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});

			it("expect calling loadDetails() to load data properly", function (done) {
				var createTriggered, detailsTriggered, didSync, addDone, addTriggered;
				var deviceObj = {
					"id": 55219,
					"product_name": "My Gateway 1",
					"model": "TEST",
					"dsn": "VXSA00000000003",
					"oem": "b15b3f53",
					"oem_model": "OTEST",
					"sw_version": "unknown",
					"user_id": 12590,
					"key": 55219
				};

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.add)({id: 55219}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("POST");
						addTriggered = true;

						success({"device": {
							"dsn": "VXSA00000000003",
							"model": "TEST",
							"product_name": "My Gateway 1",
							"connected_at": null,
							"key": 55219,
							"device_type": "Wifi"
						}});
					});

				SpecHelper.ajax.registerHandler("ajax", _.template(AylaConfig.endpoints.device.detail)({id: 55219}),
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("GET");

						detailsTriggered = true;
						success([
							{device: deviceObj}
						]);
					});

				var device = new TestDevice();

				/// because test is synchronous, created is actually called after sync...
				device.on("created", function () {
					createTriggered = true;
				});

				device.on("sync", function () {
					didSync = true;
				});

				device.set(deviceObj);

				// because page is synchronous, then is called after created and sync
				device.add().then(function (result) {
					addDone = true;

					expect(addTriggered).toBeTruthy();
					expect(didSync).toBeTruthy();
					expect(addDone).toBeTruthy();
					expect(detailsTriggered).toBeTruthy();
					expect(createTriggered).toBeTruthy();
					done();
				});
			});
		});
	});
});