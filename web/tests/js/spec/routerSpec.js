/*jslint jasmine: true */
define([
	"app",
	"underscore",
	"backbone",
	"consumer/login/login"
], function (App, _, B) {
	'use strict';


	describe("Router Spec", function () {
		var router, routerLoginSpy, trigger = {trigger: true};

		beforeEach(function (done) {
			App.start();

			// setup salus connector on app
			require(["common/model/api/SalusConnector"], function (SalusConnector) {

				App.salusConnector = new SalusConnector();

				// don't start the history twice
				if (!B.History.started) {
					B.history.start();
				}

				done();
			});

		});

		afterEach(function () {
			window.location.hash = "";
			B.history.stop();
		});

		/**
		 * Router route arguments are what we expect to see once the router decides which route and params to send
		 * format: ["type of route", "route params detected in URL", null];
		 *
		 * example 1:
		 * hitting url: login/changePasswordWithToken
		 * should produce args: ["changePasswordWithToken", null, null], would not have a token
 		 */
		it('should not parse a token out of the "login/changePasswordWithToken" route URL', function () {
			router = App.Consumer.Login.router;

			routerLoginSpy = spyOn(router, "execute").and.callFake(function (callback, args, route) {
				expect(args).toEqual(["changePasswordWithToken", null, null]);
			});

			App.navigate('login/changePasswordWithToken', trigger);
			expect(routerLoginSpy).toHaveBeenCalled();
		});

		/**
		 * example 2:
		 * hitting url: login/changePasswordWithToken?confirmation_token=abcdefg
		 * should produce args: ["changePasswordWithToken", "abcdefg", null], would have a token of "abcdefg"
		 */
		it('should parse a token out of the "login/changePasswordWithToken?confirmation_token=abcdefg" route URL', function () {
			router = App.Consumer.Login.router;

			routerLoginSpy = spyOn(router, "execute").and.callFake(function (callback, args, route) {
				expect(args).toEqual(["changePasswordWithToken", "abcdefg", null]);
			});

			App.navigate('login/changePasswordWithToken?confirmation_token=abcdefg', trigger);
			expect(routerLoginSpy).toHaveBeenCalled();
		});
	});
});