/*jslint jasmine: true */
define([
	"jquery",
	"underscore",
	"backbone"
], function ($, _, B) {
	"use strict";

	// makes sure we don't call ayla in tests
	$.ajax = function () {
		return $.Deferred();
	};

	$.post = function () {
		return $.Deferred();
	};

	define("common/util/CookieMgr", [], function () {
		return {
			createCookie: _.noop,
			readCookie: _.noop,
			eraseCookie: _.noop
		};
	});

	/**
	 * this builds a pretty printed comparison of a and b labeling them with their names.
	 * We could use this to actually compute equality, but the jasmine equality function will do this for us,
	 * and it is better tested still
	 * @param aName
	 * @param inA
	 * @param bName
	 * @param inB
	 * @param util
	 * @returns {string}
	 */
	var buildObjectComparison = function (aName, inA, bName, inB, util) {
		var a = _.clone(inA),
			b = _.clone(inB),
			result = {aOnly: [], bOnly: [], different: []},
			out = [];

		/**
		 * record a notable prop
		 * @param list
		 * @param prop
		 * @param model model is either the model to get the prop from, or a string message
		 */
		var addResultEntry = function (list, prop, model) {
			var msg = (typeof model === "string") ? model : null;

			list.push({"key": prop, "val": model[prop], msg: msg});
		};

		/**
		 * build the output message for an entry
		 * @param entry
		 */
		var buildMessageForEntry = function (entry) {
			var valString = entry.msg || ("value: '" + entry.val + "'");

			out.push("\t'" + entry.key + "'- " + valString);
		};

		/**
		 * build result array messages
		 * @param title
		 * @param list
		 */
		var processEntryType = function (title, list) {
			if (list.length > 0) {
				out.push(title + ":");
				_.each(list, buildMessageForEntry);
			}
		};

		var allKeys = _.union(_.keys(a), _.keys(b));

		_.each(allKeys, function (key) {
			if (!_.has(a, key)) {
				addResultEntry(result.bOnly, key, b);
				return;
			}

			if (!_.has(b, key)) {
				addResultEntry(result.aOnly, key, a);
				return;
			}

			if (!util.equals(a[key], b[key])) {
				addResultEntry(result.different, key, "'" + a[key] + "' not equal to '" + b[key] + "'");
			}
		});

		processEntryType("Keys only in " + aName, result.aOnly);
		processEntryType("Keys only in " + bName, result.bOnly);
		processEntryType("Values that don't match", result.different);

		return out.join("\n");
	};

	var matchObjectPropertiesComparitor = function (model, jsObject, util, customEqualityTesters) {
		var result = {},
			modelProps = model && _.isFunction(model.toJSON) ? model.toJSON() : model;

		result.pass = util.equals(modelProps, jsObject);

		if (result.pass) {
			result.message = "expected model properties to match object properties.";
		} else {
			result.message = "expected model properties to match object properties.\n" +
				buildObjectComparison("Backbone Model", modelProps, "expected", jsObject, util);
		}

		return result;
	};

	beforeEach(function () {
		jasmine.addMatchers({
			toMatchObjectProperties: function (util, customEqualityTesters) {
				return {
					compare: function (model, jsObject) {
						return matchObjectPropertiesComparitor(model, jsObject, util, customEqualityTesters);
					}
				};
			},
			toMatchObjectPropertiesExcept: function (util, customEqualityTesters) {
				return {
					compare: function (model, jsObject, exceptList) {
						var lModel = model ? _.omit(model.toJSON(), exceptList) : model;
						var lJsObject = _.omit(jsObject, exceptList);

						return matchObjectPropertiesComparitor(lModel, lJsObject, util, customEqualityTesters);
					}
				};
			},
			propEquals: function (util, customEqualityTesters) {
				return {
					compare: function (model, propertyName, expected) {
						var result = {},
							propValue = model.get(propertyName);

						result.pass = util.equals(propValue, expected);

						if (result.pass) {
							result.message = "expected model to have property '" + propertyName + "' with value '" + expected + "'";
						} else {
							result.message = "expected model to have property '" + propertyName + "' with value '" + expected + "' but was '" + propValue + "'";
						}

						return result;
					}
				};
			}
		});
	});

	var mockAjax = (function () {
		var ajaxHandlers = null;

		var _setupInternal = function () {
			if (!ajaxHandlers) {
				ajaxHandlers = {"ajax": {}, "post": {}, "getJSON": {}};
			}
		};

		var setup = function () {
			spyOn($, "ajax").and.callFake(function (ajaxData) {
				return handleCall("ajax", ajaxData.url, ajaxData.data, ajaxData.success, ajaxData);
			});

			spyOn($, "post").and.callFake(function (url, data, success) {
				return handleCall("post", url, data, success);
			});
		};

		var handleCall = function (callType, url, data, success, raw) {
			var foundHandler = null,
				splitUrl = url.split("://"),
				proto = splitUrl[0],
				matchURL = splitUrl[1];

			_setupInternal();

			_.each(ajaxHandlers[callType], function (handle, hPath) {
				if (!foundHandler && matchURL.indexOf(hPath) !== -1) {
					foundHandler = handle;
				}
			});

			var p = $.Deferred();

			var mngSuccess = function (result) {
				if (success) {
					success(result);
				} else {
					p.resolve(result);
				}
			};

			var mngFail = function (result) {
				p.reject(result);
			};

			if (foundHandler) {
				foundHandler(proto, matchURL, (callType === "ajax" ? raw.type : callType).toUpperCase(), data, mngSuccess, mngFail, raw);
				return p;
			} else {
				if (console && console.log) {
					console.log("using default handler for url " + url + "  with data: " + data);
				}

				if (success) {
					success(true);
				}
			}

			return p;
		};

		var registerHandler = function (name, path, handler) {
			_setupInternal();
			ajaxHandlers[name.toLowerCase()][path] = handler;
		};

		var cleanup = function () {
			if ($.post.reset) {
				$.post.reset();
			}

			if ($.ajax.reset) {
				$.ajax.reset();
			}

			ajaxHandlers = null;
		};

		return {
			setup: setup,
			registerHandler: registerHandler,
			cleanup: cleanup
		};
	})();

	return {ajax: mockAjax};
});