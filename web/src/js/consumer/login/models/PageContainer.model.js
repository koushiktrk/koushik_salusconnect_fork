"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Login.Models", function (Models, App, B) {
		Models.PageContainerModel = B.Model.extend({
			defaults: {
				"contentWellType": null,
				"contentTemplate": null,
				"params": null
			}
		});
	});

	return App.Consumer.Login.Models.PageContainerModel;
});