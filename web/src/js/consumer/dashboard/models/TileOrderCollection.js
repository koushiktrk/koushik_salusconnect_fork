"use strict";

/**
 * This is a collection of dashboard tile models representing the order on the dashboard.
 */
define([
	"app",
	"consumer/dashboard/models/Tile.model"
], function (App) {

	App.module("Consumer.Dashboard.Models", function (Models, App, B, Mn, $, _) {
		Models.TileOrderCollection = B.Collection.extend({
			model: Models.TileModel,

			initialize: function (data, gatewayDsn) {
				this.gatewayDsn = gatewayDsn;

				this.listenTo(App.salusConnector, "loaded:ruleCollection", this._triggerRuleDashUpdate);
			},

			update: function (newOrder) {
				var that = this, any;
				
				that.reset();
				_.each(newOrder, function (newModel, index) {
					if (index >= that.length) {
						that.add(newModel);
					} else if (that.at(index).get("referenceId") !== newModel.referenceId) {
						
						//that.at(index).set(newModel);
						that.remove(that.at(index));
						that.add(newModel, {at: index});
					}
					any = true;
				});

				if (any) {
					// triggers save flow
					this.trigger("reorder:tileOrder", this);
				}
			},

			isLoaded: function () {
				return this.length > 0;
			},

			_triggerRuleDashUpdate: function () {
				var that = this;
				this.each(function (tileModel) {
					if (tileModel.get("referenceType") === "rule") {
						var rule = App.salusConnector.getRule(tileModel.get("referenceId"));
						if (rule) {
							rule._updateIsInDash(that);
						}
					}
				});
			},

			_findAndRemoveHelper: function (tileRef, refType) {
				var findWhere;

				if (refType === "welcome") {
					findWhere = this.findWhere({
						referenceType: "welcome"
					});
				} else {
					findWhere = this.findWhere({
						referenceId: tileRef.id,
						referenceType: refType
					});
				}

				if (findWhere) {
					findWhere.destroy();
				}
			},

			findAndRemove: function (tileReference, referenceType) {
				var that = this, userTileOrderCollection = App.salusConnector.getSessionUser().get("tileOrderCollection");

				if (!this.isLoaded()) {
					return userTileOrderCollection.load().then(function () {
						that._findAndRemoveHelper(tileReference, referenceType);
						that.trigger("tileOrder:modification", that);
					});
				}

				this._findAndRemoveHelper(tileReference, referenceType);
				that.trigger("tileOrder:modification", that);
			},

			addNewTile: function (reference, referenceType) {
				var that = this,
					alreadyExists,
					userTileOrderCollection = App.salusConnector.getSessionUser().get("tileOrderCollection");

				if (!this.isLoaded()) {
					return userTileOrderCollection.load().then(function () {
						alreadyExists = !!that.findWhere({
							referenceId: reference.id,
							referenceType: referenceType
						});

						if (!alreadyExists) {
							that.add(that.tileModelGen(reference, referenceType));
							that.trigger("tileOrder:modification", that);
						}
					});
				} else {
					alreadyExists = !!that.findWhere({
						referenceId: reference.id,
						referenceType: referenceType
					});

					if (!alreadyExists) {
						that.add(that.tileModelGen(reference, referenceType));
						that.trigger("tileOrder:modification", that);
					}
				}
			},

			tileModelGen: function (reference, refType) {
				return new Models.TileModel({
					isLargeTile: true,
					referenceType: refType,
					referenceId: reference.id
				});
			},

			setOrder: function (order) {
				if (order.length <= 0) {
					var obj = {};
					if (Error && _.isFunction(Error.captureStackTrace)) {
						Error.captureStackTrace(obj);
					}
					App.warn("SETTING TILE ORDER TO AN EMPTY (" + this.gatewayDsn + ")\n" + obj.stack || "");
				}

				this.reset(this._buildChildViewTiles(order));
			},

			_buildChildViewTiles: function (startingTileOrder) {
				var collection = [];

				if (App.salusConnector.getSessionUser().dashboardFirstLoad) {
					collection.push(new Models.TileModel({
						isLargeTile: true,
						deviceType: "welcome",
						referenceType: "welcome",
						referenceId: null
					}));
				}

				if (startingTileOrder && startingTileOrder.length > 0) {
					var shouldSave = false;

					_.each(startingTileOrder, function (tileOrderModel) {
						var shouldAdd = true;

						// check if the tileOrder deviceModel was  was removed from our device collection
						if (!App.salusConnector.getDevice(tileOrderModel.referenceId) && tileOrderModel.referenceType === "device") {
							shouldAdd = false;
							shouldSave = true;
						}

						if (shouldAdd) {
							collection.push(new Models.TileModel(tileOrderModel));
						}
					});

					if (shouldSave) {
						this.trigger("tileOrder:modification", this);
					}
				}

				return collection;
			},
			/**,
			 * Gets the inflated device collection that occurred in
			 * DashboardLayout.js, loops over each device model
			 * and builds out a tile model for a tile view
			 */
			buildStartingTilesOnCreate: function (devices) {
				var that = this,
					tempArray;

				this.firstLoad = true;

				tempArray = devices.map(function (device) {
					return that.tileModelGen(device, "device");
				});

				// add welcome tile to beginning of array
				tempArray.unshift(new Models.TileModel({
					isLargeTile: true,
					referenceType: "welcome",
					referenceId: null
				}));

				this.set(tempArray);
			}
		});
	});

	return App.Consumer.Dashboard.Models.TileOrderCollection;
});
