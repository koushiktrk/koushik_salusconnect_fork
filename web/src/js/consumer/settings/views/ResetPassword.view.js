"use strict";

define([
	"app",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins",
	"consumer/models/SalusButtonPrimary",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
], function (App, config, consumerTemplates,
			 SalusPage, SalusViewMixin, ConsumerMixins) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.ResetPasswordPage = Mn.LayoutView.extend({
			template: consumerTemplates["settings/profile/resetPassword"],
			id: "change-password",
			regions: {
				"emailRegion": ".bb-email",
				"newPasswordRegion": ".bb-new-password",
				"passwordConfirmRegion":".bb-confirm-password",
				"submitButtonRegion": ".bb-submit-button",
				"cancelButtonRegion": ".bb-cancel-button"
			},
			events: {
				"mouseover .bb-submit-button": "validateForm"
			},
			initialize: function() {
				_.bindAll(this,
						"_validatePasswordConfirm",
						"handleSubmitClicked",
						"validateForm",
						"handleCancelClicked");

				this.emailField = new App.Consumer.Views.FormTextInput({
					labelText: App.translate("login.login.emailLabel"),
                    iconPath: App.rootPath("/images/icons/icon_mail_blue.svg"),
					inputType: "email",
					required: true,
					model: this.model
				});

				this.newPasswordField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.changePassword.formFields.newPassword",
					type: "password",
					required: true,
					model: this.model
				});

				this.passwordConfirmField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.changePassword.formFields.confirmPassword",
					type: "password",
					validationMethod: this._validatePasswordConfirm,
					required: true,
					model: this.model,
					showFeedback: true,
					preventTrigger: true // textbox that is 2nd pass confirmation should have this
				});

				this.submitButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "settings.profile.formFields.submitButton",
					classes: "width100 disabled",
					clickedDelegate: this.handleSubmitClicked
				});

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "col-xs-6",
					className: "width100 btn btn-default",
					clickedDelegate: this.handleCancelClicked
				});

				this.validationNeeded.push(this.emailField, this.passwordConfirmField, this.newPasswordField);

				this.attachValidationListener(); // to validationNeeded items
			},

			onRender: function() {
				this.emailRegion.show(this.emailField);
				this.newPasswordRegion.show(this.newPasswordField);
				this.passwordConfirmRegion.show(this.passwordConfirmField);
				this.submitButtonRegion.show(this.submitButton);
				this.cancelButtonRegion.show(this.cancelButton);
			},
			handleCancelClicked: function () {
				App.navigate("settings/profile");
			},
			_validatePasswordConfirm: function () {
				var pass1 = this.newPasswordRegion.currentView.getValue(),
						pass2 = this.passwordConfirmRegion.currentView.getValue();

				return this.model.validatePassword(pass1, pass2);
			},
			validateForm: function () {
				var valid = this.isValid();

				if (!valid) {
					this.submitButtonRegion.currentView.disable();
				} else {
					this.submitButtonRegion.currentView.enable();
				}

				return valid;
			},
			resetPassword: function() {
				//var oldPass = this.oldPasswordRegion.currentView.getValue(),
						//newPass = this.newPasswordRegion.currentView.getValue();
                var email = this.emailRegion.currentView.getValue();
				return this.model.resetPassword(email);
			},
			handleSubmitClicked: function () {
				var that = this;
				// make sure the form validates before we submit
				if (!this.validateForm()) {
					return;
				}

				this.submitButton.showSpinner();

				return this.resetPassword().then(function () {
					that.submitButton.hideSpinner();
					App.navigate("/settings/profile");
				}).catch(function () {
					that.submitButton.hideSpinner();
				});
			}
		}).mixin([SalusPage, ConsumerMixins.FormWithValidation], {
			analyticsSection: "login",
			analyticsPage: "resetPassword" //TODO: This should be a function so the equipment Id is recorded.
		});
	});

	return App.Consumer.Settings.Views.ChangePasswordPage;
});
