"use strict";

define([
	"app",
	"common/commonTemplates",
	"consumer/views/mixins/mixin.salusTextBox"
], function (App, commonTemplates, SalusTextBox) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {

		Views.FormEmailInput = Mn.ItemView.extend({
			template: commonTemplates["salusForms/formTextInput"]
		}).mixin([SalusTextBox]);
	});

	return App.Consumer.Views.FormEmailInput;
});