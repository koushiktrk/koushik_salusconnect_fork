"use strict";

define([
	"app",
	"common/constants",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins",
	"common/util/VendorfyCssForJs"
], function (App, constants, config, consumerTemplates, SalusViewMixin, ConsumerMixin, VendorfyCss) {

	App.module("Consumer.Schedules.Views.Presets", function (Views, App, B, Mn) {
		Views.SchedulePresetsView = Mn.LayoutView.extend({
			id: "schedule-presets-page",
			template: consumerTemplates["schedules/presets/schedulePresets"],
			className: "container",
			regions: {
 				//
			},
			ui: {
				workImg: ".bb-work-week",
				mostlyHomeImg: ".bb-mostly-home",
				mostlyAwayImg: ".bb-mostly-away",
				createNewImg: ".bb-create-new"
			},
			onRender: function () {
				VendorfyCss.formatBgGradientWithImage(
						this.ui.workImg,
						"center / 48%",
						"no-repeat",
						App.rootPath(constants.schedulesIconPaths.working),
						"145deg",
						"#00C1DD",
						"0%",
						"#00BBE0",
						"100%"
				);

				VendorfyCss.formatBgGradientWithImage(
						this.ui.mostlyAwayImg,
						"center / 48%",
						"no-repeat",
						App.rootPath(constants.schedulesIconPaths.away),
						"145deg",
						"#7CC110",
						"0%",
						"#5FAC09",
						"100%"
				);

				VendorfyCss.formatBgGradientWithImage(
						this.ui.mostlyHomeImg,
						"center / 48%",
						"no-repeat",
						App.rootPath(constants.schedulesIconPaths.home),
						"145deg",
						"#FF8F00",
						"0%",
						"#FF7300",
						"100%"
				);

				VendorfyCss.formatBgGradientWithImage(
						this.ui.createNewImg,
						"center / 48%",
						"no-repeat",
						App.rootPath(constants.schedulesIconPaths.create_new),
						"145deg",
						"#027DD9",
						"0%",
						"#0160CB",
						"100%"
				);
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Schedules.Views.Presets.SchedulePresetsView;
});