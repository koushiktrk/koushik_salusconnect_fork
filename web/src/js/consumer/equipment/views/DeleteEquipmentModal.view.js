"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view"
], function (App, constants, consumerTemplates, SalusViewMixin, SalusPrimaryButtonView, ModalCloseButton) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.DeleteEquipmentModalView = Mn.LayoutView.extend({
			className: "modal-body delete-equip-modal-content",
			ui: {
				primaryLabel: "#bb-primary-label",
				gatewayWarning: "#bb-gateway-warning"
			},
			regions: {
				leftBtnRegion: "#bb-left-btn-area",
				rightBtnRegion: "#bb-right-btn-area"
			},
			template: consumerTemplates["equipment/removeEquipmentModal"],
			initialize: function () {
				_.bindAll(this, "handleDeleteClick", "handleCancelClick", "initiateRefreshPoll", "refreshPoll", "refreshPollTimeout", "clearRefreshPollTimeout");

				this.deleteBtn = new SalusPrimaryButtonView({
					id: "delete-equip-btn",
					classes: "btn-danger width100 pull-right",
					buttonTextKey: "common.labels.delete",
					clickedDelegate: this.handleDeleteClick
				});

				this.cancelBtn = new ModalCloseButton({
					id: "delete-equip-btn",
					classes: "width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.handleCancelClick
				});
			},
            //Bug_CASBW-216 start
            unregister: function(){
                var isCurrentGateway = this.model === App.getCurrentGateway(),
					that = this;
            
                this.model.unregister().then(function () {
                    that.deleteBtn.hideSpinner();
                    App.hideModal();

                    if (isCurrentGateway) {
                        App.salusConnector.getFullDeviceCollection().remove(that.model);
                        App.salusConnector.getFullDeviceCollection().load();
                        if(App.salusConnector.getGatewayCollection().first() !== undefined){
                            App.salusConnector.changeCurrentGateway(App.salusConnector.getGatewayCollection().first());
                        }
                        App.navigate("");
                    } 
                    else {
                        App.navigate("equipment");
                        //window.history.back();
                    }
				}).catch(function (error) {
					that.deleteBtn.hideSpinner();
					App.hideModal();
                    
					throw error;
				});
            },
            //Bug_CASBW-216 end
            
			handleDeleteClick: function () {
                //Bug_CASBW-216 start
                /*var isCurrentGateway = this.model === App.getCurrentGateway(),
					that = this;

				this.deleteBtn.showSpinner();

				this.model.unregister().then(function () {
					that.deleteBtn.hideSpinner();
					App.hideModal();

					if (isCurrentGateway) {
						App.salusConnector.getFullDeviceCollection().remove(that.model);
						App.salusConnector.getFullDeviceCollection().load();
                        if(App.salusConnector.getGatewayCollection().first() !== undefined){
                            App.salusConnector.changeCurrentGateway(App.salusConnector.getGatewayCollection().first());
                        }
						App.navigate("");
					} 
                    else {
                        App.navigate("equipment");
						//window.history.back();
					}
				}).catch(function (error) {
					that.deleteBtn.hideSpinner();
					App.hideModal();

					throw error;
				});*/
                
				var isCurrentGateway = this.model === App.getCurrentGateway(),
					that = this;
                
                // Dev_SCS-3284 Start
                if(App.getIsDemo()) {
                    App.hideModal();
                    return;
                }
                // Dev_SCS-3284 End
                
				this.deleteBtn.showSpinner();
                
                if(isCurrentGateway || this.getLeaveWorkValue(this.model)){
                   that.unregister();
                }
                else{
                    this.model.setLeaveNetwork().then(function(){ 
                        that.initiateRefreshPoll(); 
                    });
                }
                //Bug_CASBW-216 end
			},
            
           
			handleCancelClick: function () {
				App.hideModal();
                
			},
			onRender: function () {
				this.ui.primaryLabel.text(App.translate("equipment.delete.warning", {
					product_name: this.model.getDisplayName()
				}));

				// show this extra label when gateway
				if (this.model.isGateway()) {
					this.ui.gatewayWarning.removeClass("hidden");
				}

				this.leftBtnRegion.show(this.cancelBtn);
				this.rightBtnRegion.show(this.deleteBtn);
			},
            getLeaveWorkValue: function(model){
                var leaveNetworkObj = model.get("LeaveNetwork");
                if(leaveNetworkObj){
                     if(leaveNetworkObj.get("getterProperty") !== undefined){
                        leaveNetworkObj = leaveNetworkObj.get("getterProperty"); 
                    }else{
                        leaveNetworkObj = leaveNetworkObj.models[0].get("getterProperty");
                    }
                    return leaveNetworkObj.get("value") === 1 ? true:false;
                }
                return false;
            },
            //Bug_CASBW-216 start
            initiateRefreshPoll: function(){
                this.leaveNetworkInterval = setInterval(this.refreshPoll, 1000);
                this.leaveNetworkTimeout = setTimeout(this.refreshPollTimeout, 1000 * 60);
            },
            clearRefreshPollTimeout: function () {
				clearInterval(this.leaveNetworkInterval);
                clearTimeout(this.leaveNetworkTimeout);
			},
            refreshPoll: function(){
                var that = this;
                if(this.getLeaveWorkValue(this.model)){
                    that.clearRefreshPollTimeout();
                    that.unregister();
                }
                
                
                
            },
            refreshPollTimeout: function(){
                this.hideModal();
                this.clearRefreshPollTimeout();
                this.errorModal = new App.Consumer.Views.SalusAlertModalView({
                   staticBackdrop: true,
                   model: new App.Consumer.Models.AlertModalViewModel({
                       iconClass: "icon-warning",
                       primaryLabelText: App.translate("login.login.error.otherInvalidError1"),
                       rightButton: new App.Consumer.Views.ModalCloseButton({
                           classes: "btn-danger width100",
                           buttonTextKey: "common.labels.ok",
                           clickedDelegate: this.handleCancelClick
                       }),
                       leftButton: new App.Consumer.Views.ModalCloseButton({
                            classes: "width100",
                            buttonTextKey: "common.labels.cancel",
                            clickedDelegate: this.handleCancelClick
                        })
                   })
                });
                App.modalRegion.show(this.errorModal);
                App.showModal();
                
            },
            hideModal: function(){
                App.hideModal();
                $(".modal-backdrop").remove();
            },
             onBeforeDestroy: function () {
                this.clearRefreshPollTimeout();
            }
            //Bug_CASBW-216 end
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Equipment.Views.DeleteEquipmentModalView;
});