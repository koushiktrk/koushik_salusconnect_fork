"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusTabContent",
	"consumer/equipment/views/messageCenter/AlertTableRow.view",
	"consumer/equipment/models/AlertTable.model"
], function (App, templates, SalusTabView, AlertTableRowView, AlertTableModel) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {
		Views.AlertTableView = Mn.CompositeView.extend({
			id: "bb-alerts-tab",
			template: templates["equipment/messageCenter/alertTable"],
			childView: AlertTableRowView,
			childViewContainer: "#bb-alert-table",

			events: {
				'click .bb-header': "headerClick"
			},

			initialize: function () {
				this.collection = App.salusConnector.getAlertCollection();
				this.model = new AlertTableModel();

				this.listenTo(this.model, 'change', this.resortView);
			},

			onRender: function () {
				this.$(".sort-arrow-icon").removeClass("show-arrow point-arrow-up");
				this.$('div[data-sort-enum-val="' + this.model.get("sortBy") + '"] .sort-arrow-icon').addClass("show-arrow");
			},

			headerClick: function (event) {
				var $el = B.$(event.currentTarget),
					clickedType = $el.data("sort-enum-val"),
					currentType = this.model.get("sortBy");

				if (clickedType === currentType) {
					var newSortDescVal = !this.model.get("sortDesc");

					this.model.set("sortDesc", newSortDescVal);
					// this can be done with the the double
					this._flipSortArrow(!newSortDescVal);
				} else {
					this.model.set("sortBy", clickedType);
					this.model.set("sortDesc", true);
				}
			},

			viewComparator: function (equipmentA, equipmentB) {
				var sortDesc = this.model.get("sortDesc"),
					sortType = this.model.get("sortBy"),
					sortValueA = this._getSortTableValue(equipmentA, this.model.sortByEnum[sortType]),
					sortValueB = this._getSortTableValue(equipmentB, this.model.sortByEnum[sortType]),
					returnVal = 0;

				if (sortValueA > sortValueB) {
					returnVal = 1;
				} else if (sortValueA < sortValueB) {
					returnVal = -1;
				}

				return sortDesc ? returnVal : -1 * returnVal;
			},

			_flipSortArrow: function (shouldPointUp) {
				this.$(".sort-arrow-icon.show-arrow").toggleClass("point-arrow-up", shouldPointUp);
			},

			_getSortTableValue: function (equipment, sortByOptions) {
				if (sortByOptions.isDeviceProp) {
					return App.salusConnector.getDevice(equipment.get("deviceIdRef")).get(sortByOptions.modelKey);
				}

				return equipment.get(sortByOptions.modelKey);
			}
		}).mixin([SalusTabView]);
	});

	return App.Consumer.Equipment.Views.AlertTableView;
});