'use strict';

define([
	"app",
	"common/constants"
], function (App, constants) {
	var ruleMap = {};

	ruleMap[constants.modelTypes.SMARTPLUG] = [
		{  // button press -> turn on -> (1hour) turn off
			name: "equipment.oneTouch.recommended.smartPlug.oneHour.name",
			description: "equipment.oneTouch.recommended.smartPlug.oneHour.description",
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null, // device euid
					propName: "OnOff",
					newVal: 1
				}
			],
			delayedActions: [
				{
					type: "timer",
					subType: "propChange",
					totalTime: 60,
					data: {
						type: "propChange",
						EUID: null,
						propName: "OnOff",
						newVal: 0
					}
				}
			],
			dsn: null, // gateway dsn
			unparsable: false,
			active: true
		},
		{  // button press -> turn off
			name: "equipment.oneTouch.recommended.smartPlug.turnOff.name",
			description: "equipment.oneTouch.recommended.smartPlug.turnOff.description",
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "OnOff",
					newVal: 0
				}
			],
			delayedActions: [],
			dsn: null,
			unparsable: false,
			active: true
		},
		{ // When 12:00 am -> turn off
			name: "equipment.oneTouch.recommended.smartPlug.12am.name",
			description: "equipment.oneTouch.recommended.smartPlug.12am.description",
			conditions: [
				{
					type: "tod",
					startTime: "00:00:00",
					endTime: "00:01:00"
				}
			],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "OnOff",
					newVal: 0
				}
			],
			delayedActions: [],
			dsn: null,
			unparsable: false,
			active: true
		}
	];

	ruleMap[constants.modelTypes.MINISMARTPLUG] = [
		{ // button press -> turn on -> (1hour) turn off
			name: "equipment.oneTouch.recommended.smartPlug.oneHour.name",
			description: "equipment.oneTouch.recommended.smartPlug.oneHour.description",
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "OnOff",
					newVal: 1
				}
			],
			delayedActions: [
				{
					type: "timer",
					subType: "propChange",
					totalTime: 60,
					data: {
						type: "propChange",
						EUID: null,
						propName: "OnOff",
						newVal: 0
					}
				}
			],
			dsn: null,
			unparsable: false,
			active: true

		},
		{  // button press -> turn off
			name: "equipment.oneTouch.recommended.smartPlug.turnOff.name",
			description: "equipment.oneTouch.recommended.smartPlug.turnOff.description",
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "OnOff",
					newVal: 0
				}
			],
			delayedActions: [],
			dsn: null,
			unparsable: false,
			active: true
		},
		{ // When 12:00 am -> turn off
			name: "equipment.oneTouch.recommended.smartPlug.12am.name",
			description: "equipment.oneTouch.recommended.smartPlug.12am.description",
			conditions: [
				{
					type: "tod",
					startTime: "00:00:00", // local
					endTime: "00:01:00"
				}
			],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "OnOff",
					newVal: 0
				}
			],
			delayedActions: [],
			dsn: null,
			unparsable: false,
			active: true
		}
	];

	ruleMap[constants.modelTypes.THERMOSTAT] = [
		{  // when press -> Turn Thermo Off
			name: "equipment.oneTouch.recommended.thermostat.turnOff.name",
			description: "equipment.oneTouch.recommended.thermostat.turnOff.description",
			dsn: null,
			unparsable: false,
			active: true,
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "SystemMode",
					newVal: constants.thermostatModeTypes.OFF
				}
			],
			delayedActions: []
		}
	];
    
    ruleMap[constants.modelTypes.IT600THERMOSTAT] = [
		{   //Party Mode
			name: "equipment.oneTouch.recommended.iT600Thermostat.partyMode.name",
			description: "equipment.oneTouch.recommended.iT600Thermostat.partyMode.description",
			dsn: null,
			unparsable: false,
			active: true,
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: ["HeatingSetpoint_x100","CoolingSetpoint_x100"],
					newVal: 2100
				}
			],
			delayedActions: [
                {
					type: "timer",
					subType: "propChange",
					totalTime: 120,
					data: {
						type: "propChange",
						EUID: null,
						propName: "HoldType",
						newVal: constants.it600HoldTypes.FOLLOW
					}
				}
            ]
		},
        {   //Run Comfort Temperature
			name: "equipment.oneTouch.recommended.iT600Thermostat.runComfortTemperature.name",
			description: "equipment.oneTouch.recommended.iT600Thermostat.runComfortTemperature.description",
			dsn: null,
			unparsable: false,
			active: true,
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: ["HeatingSetpoint_x100","CoolingSetpoint_x100"],
					newVal: 2100
				}
			],
			delayedActions: []
		},
        {   //Run Frost Mode
			name: "equipment.oneTouch.recommended.iT600Thermostat.runFrostMode.name",
			description: "equipment.oneTouch.recommended.iT600Thermostat.runFrostMode.description",
			dsn: null,
			unparsable: false,
			active: true,
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "HoldType",
					newVal: constants.it600HoldTypes.OFF
				}
			],
			delayedActions: []
		}
	];
    

	ruleMap[constants.modelTypes.WATERHEATER] = [
//		{  // when press -> Boost mode on -> (2 hour) turn off boost mode off
//			name: "equipment.oneTouch.recommended.waterHeater.boostFor2.name",
//			description: "equipment.oneTouch.recommended.waterHeater.boostFor2.description",
//			dsn: null,
//			unparsable: false,
//			active: true,
//			conditions: [],
//			actions: [
//				{
//					type: "propChange",
//					EUID: null,
//					propName: "HoldType",
//					newVal: constants.waterHeaterHoldTypes.BOOST
//				}
//			],
//			delayedActions: [
//				{
//					type: "timer",
//					subType: "propChange",
//					totalTime: 120,
//					data: {
//						type: "propChange",
//						EUID: null,
//						propName: "HoldType",
//						newVal: constants.waterHeaterHoldTypes.SCHEDULE
//					}
//				}
//			]
//		},
        {   //Extra 2 hour Boost
			name: "equipment.oneTouch.recommended.waterHeater.extra2HourBoost.name",
			description: "equipment.oneTouch.recommended.waterHeater.extra2HourBoost.description",
			dsn: null,
			unparsable: false,
			active: true,
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "HoldType",
					newVal: constants.waterHeaterHoldTypes.BOOST
				}
			],
			delayedActions: [
				{
					type: "timer",
					subType: "propChange",
					totalTime: 120,
					data: {
						type: "propChange",
						EUID: null,
						propName: "HoldType",
						newVal: constants.waterHeaterHoldTypes.SCHEDULE
					}
				}
			]
		},
        {   //One time 2 hour Boost
			name: "equipment.oneTouch.recommended.waterHeater.oneTime2HourBoost.name",
			description: "equipment.oneTouch.recommended.waterHeater.oneTime2HourBoost.description",
			dsn: null,
			unparsable: false,
			active: true,
			conditions: [],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "HoldType",
					newVal: constants.waterHeaterHoldTypes.BOOST
				}
			],
			delayedActions: [
				{
					type: "timer",
					subType: "propChange",
					totalTime: 120,
					data: {
						type: "propChange",
						EUID: null,
						propName: "HoldType",
						newVal: constants.waterHeaterHoldTypes.OFF
					}
				}
			]
		}
	];
    
    
    //Dev_SBO-606 start
    //window sensor 不会出现在default onetouch界面
    ruleMap[constants.modelTypes.WINDOWMONITOR] = [
		{   //Window Open
			name: "",
			description: "",
			dsn: null,
			unparsable: false,
			active: true,
            windowProtection: "OR",
			conditions: [
                {
                    type: "propVal",
                    propName: "ErrorIASZSAlarmed1",
                    eq: 1,
                    EUID: null
                }
            ],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "HoldType",
					newVal: constants.waterHeaterHoldTypes.OFF
				},
                {
					type: "propChange",
					EUID: null,
					propName: "LockKey",
					newVal: constants.lockKey.LOCK
				}
			],
			delayedActions: []
		},
        {   //Window Closed
			name: "",
			description: "",
			dsn: null,
			unparsable: false,
			active: true,
            windowProtection: "AND",
			conditions: [
                {
                    type: "propVal",
                    propName: "ErrorIASZSAlarmed1",
                    eq: 0,
                    EUID: null
                }
            ],
			actions: [
				{
					type: "propChange",
					EUID: null,
					propName: "HoldType",
					newVal: constants.waterHeaterHoldTypes.SCHEDULE
				},
                {
					type: "propChange",
					EUID: null,
					propName: "LockKey",
					newVal: constants.lockKey.UNLOCK
				}
			],
			delayedActions: []
		}
	];
    //Dev_SBO-606 end

	return ruleMap;
});