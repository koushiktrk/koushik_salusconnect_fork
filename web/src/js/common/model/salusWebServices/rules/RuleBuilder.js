"use strict";

define([
	"app",
	"common/util/utilities",
	"common/constants",
	"momentWrapper"
], function (App, utils, constants) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.RuleBuilder = B.Model.extend({
			//main function;
			buildRule: function (rule) {
				this._clearOutput();

				if (!rule.get("unparsable")) {
					this._buildConditionsPayload(rule);
					this._buildActionsPayload(rule);
					this._buildDelayedActionsPayload(rule);

					rule.updateTriggers(this.output.triggersToUpdate);
				} else {
					this.output.conditions = rule.get('conditions');
					this.output.actions = rule.get("actions");
				}

				if (!!rule.uxhint) {
					this.output.uxhint = rule.uxhint;
				}

				return this.output;
			},

			_clearOutput: function () {
				this.output = {
					conditions: null,
					actions: [],
					delayedActionRule : null,
					triggersToUpdate: []
				};
			},

			_buildMyStatusPayload: function (rule) {
				var parameters,
						node = App.getCurrentGateway().getGatewayNode(),
						statusKeys = rule.getStatusKeys();

				if (!statusKeys || statusKeys.length === 0) {
					//short circuit
					return false;
				}

				parameters = _.map(statusKeys, function (key) {
					return {
						op: "EQ",
						parameters: [
							{
								op: "GetValue",
								parameters: [
									{
										string: constants.ruleGatewayNodePseudoEUID
									},
									{
										string: node && node.get("MyStatus") ? node.getPropertyName("MyStatus") : "ep_0:sRule:MyStatus"
									}
								]
							},
							{
								string: key
							}
						]
					};
				});

				return {
					op: "OR",
					parameters: parameters
				};
			},

			_buildDelayedActionsPayload: function (rule) {
				var isNew, downStreamRule, timerTriggerKey, delayedAction = rule.get("delayedActions");
				if (delayedAction && delayedAction.length > 0 && delayedAction instanceof B.Collection) {
					delayedAction = delayedAction.first();

					timerTriggerKey = delayedAction.get("timerTriggerKey");
					if (!timerTriggerKey) {
						isNew = true;
						timerTriggerKey = timerTriggerKey || "timerTriggerKey_" + utils.md5HashString(new Date().toString() + JSON.stringify(rule));
					}

					// start the timer action
					this.output.actions.push({
						"op": "StartTimer",
						"parameters": [
							{ "string": timerTriggerKey },
							{ "number": delayedAction.get("totalTime")*60 }, // 60 seconds in a minute
							{ "string": timerTriggerKey }
						]
					});

					downStreamRule = App.salusConnector.getRuleByTimerKey(timerTriggerKey);

					if (!downStreamRule && isNew) {
						this.output.ruleToSave = this._createTimerRule(delayedAction, timerTriggerKey, rule);
					} else if (!isNew) {
						this.output.ruleToSave = downStreamRule;
						downStreamRule.resetActions(delayedAction.get("data"));
					}
				} else if (rule.get("timerTriggerKey")) {
					// timerAction has been removed, so we need to delete the down stream rule
					this.output.ruleToDelete = App.salusConnector.getRuleByTimerKey(rule.get("timerTriggerKey"));
					rule.set("timerTriggerKey", null);
				}
			},

			_buildActionsPayload: function (rule) {
				var that = this;

				rule.get("actions").each(function (action) {
					var returnObj;

					switch (action.get("type")) {
						case "sms":
						case "email":
							returnObj = that._buildEmailAndSMS(action, rule);
							break;
						case "propChange":
							returnObj = that._buildPropChange(action);
							break;
						default:
							throw new Error("Unknown Rule Action Type");
					}

					if (_.isArray(returnObj)) {
						that.output.actions = that.output.actions.concat(returnObj);
					} else {
						that.output.actions.push(returnObj);
					}
				});
			},

			_buildEmailAndSMS: function (action, rule) {
				if (!action.get("triggerKey")) {
					action.set("triggerKey", "trigger-" + action.get("type") + "-" + utils.md5HashString(JSON.stringify(this.output.conditions) + new Date().toString()));
				}

				this.output.triggersToUpdate.push({
					triggerKey: action.get("triggerKey"),
					recipients: action.get("recipients"),
					message: action.get("message"),
					type: action.get("type"),
					ruleName: rule.get("name")
				});

				return {
					op: "SetValue",
					parameters: [
						{
							string: constants.ruleGatewayNodePseudoEUID
						},
						{
							"string": App.getCurrentGateway().getGatewayNode().getSetterPropNameIfPossible("AlertMessage")
						},
						{
							"string": action.get("triggerKey")
						}
					]
				};
			},

			_buildPropChange: function (action) {
				var newValObj = {},newValObjArr;

				if(_.isArray(action.get("newVal"))){
                    newValObjArr = action.get("newVal");
                } else if (_.isNumber(action.get("newVal"))) {
					newValObj.number = action.get("newVal");
				} else {
					newValObj.string = action.get("newVal");
				}

				// if for some reason newVal doesn't match the correct format or is falsy
				// don't return anything so this condition wont be broken and instead wont be saved
				if (newValObj && (_.isString(newValObj.string) || _.isNumber(newValObj.number) || newValObjArr)) {
					if (_.isArray(action.get("propName"))) {
                        var i=0,value;
						return _.map(action.get("propName"), function (propName) {
                            if(newValObjArr){
                                value={number:newValObjArr[i++]};
                            } else {
                                value=newValObj;
                            }
                            
							return {
								op: "SetValue",
								parameters: [
									{
										string: action.get("EUID")
									},
									{
										"string": propName
									},
									value
								]
							};
						});
					}

					return {
						op: "SetValue",
						parameters: [
							{
								string: action.get("EUID")
							},
							{
								"string": action.get("propName")
							},
							newValObj
						]
					};
				}
			},

			_buildConditionsPayload: function (rule) {
				var that = this, andParams = [];

				rule.get("conditions").each(function (condition) {
					var returnObj;

					switch (condition.get("type")) {
						case "dow":
							returnObj = that._buildDayOfWeek(condition);
							break;
						case "tod":
							returnObj = that._buildTimeOfDay(condition);
							break;
						case "propVal":
							returnObj = that._buildPropVal(condition);
							break;
                        case "buttonPress":
                            returnObj = that._buildButtonPress(condition);
                            break;
						default:
							throw new Error("Unknown Rule Condition Type");
					}

					if (returnObj) {
						andParams = andParams.concat(_.isArray(returnObj) ? returnObj : [returnObj]);
					}
				});

				var statusPayload = this._buildMyStatusPayload(rule);

				if (statusPayload) {
					andParams = andParams.concat([statusPayload]);
				}

			
                //Dev_SBO-606 start
                var andPayloadOp = "AND";
                if(rule.get("name").indexOf(constants.hideOneTouchKey)!==-1 && rule.get("name").indexOf("-OR")!==-1){
                    andPayloadOp="OR";
                }
                //Dev_SBO-606 end
                var andPayload = {
					op: andPayloadOp,
					parameters: andParams
				};

				var buttonParam = this._buildButtonParam(this._getRuleTriggerKey(rule));

				this.output.conditions = {
					op: "OR",
					parameters: [
						buttonParam
					]
				};

				if (andPayload.parameters.length > 0) {
					this.output.conditions.parameters.push(andPayload);
				}
			},
            
            _buildButtonPress: function(condition) {
				return [{
					"op": "EQ",
					"parameters": [
						{
							"op": "GetValue",
							"parameters": [
								{
									"string": constants.ruleGatewayNodePseudoEUID
								},
								{
									"string": "ep_0:sRule:Btn_press"
								}
							]
						},
						{
							"number": condition.get("value")
						}
					]
				}];
            },

			_buildButtonParam: function (key) {
				if (key.indexOf("timerTriggerKey") >= 0) {
					return  {
						"op": "EQ",
						"parameters": [
							{
								"op": "GetValue",
								"parameters": [
									{
										"string": constants.ruleGatewayNodePseudoEUID
									},
									{
										"string": "ep_0:sRule:Timestamp_i"
									}
								]
							},
							{
								"op": "GetValue",
								"parameters": [
									{
										"string": constants.ruleGatewayNodePseudoEUID
									},
									{
										"string": "ep_0:sRule:TimerActived_" + key
									}
								]
							}
						]
					};
				} else{
					return {
						"op": "EQ",
						"parameters": [
							{
								"op": "GetValue",
								"parameters": [
									{
										"string": constants.ruleGatewayNodePseudoEUID
									},
									{
										"string": "ep_0:sRule:TriggerRule"
									}
								]
							},
							{
								"string": key
							}
						]
					};
				}

			},

			_removeAllIndicesFromArray: function (array, indices) {
				array = $.grep(array, function (val, i) {
					return $.inArray(i, indices) === -1;
				});

				return array;
			},

			_buildDayOfWeek: function (condition) {
                var str = "";
                _.each(condition.get("dayPattern"), function(day, i) {
                    if(i === (condition.get("dayPattern").length - 1)) {
                        str = str + day;
                    } else {
                        str = str + day + " ";
                    }
                });
                
				return {
					op: "NOT",
					parameters: [{
                        "op": "NEQ",
						"parameters": [
							{
								"op": "GetValue",
								"parameters": [
									{
										string: constants.ruleGatewayNodePseudoEUID
									},
									{
										string: "ep_0:sRule:Weekday_i"
									}
								]
							},
							{
                                string: str
							}
						]
                    }]
                };
			},

			_buildTimeOfDay: function (condition) {
				var startTime = condition.get("startTime"),
						endTime = condition.get("endTime");

				if (startTime === endTime) {
					endTime = endTime + 100;
				}
				return [{
					"op": "GTE",
					"parameters": [
						{
							"op": "GetValue",
							"parameters": [
								{
									"string": constants.ruleGatewayNodePseudoEUID
								},
								{
									"string": "ep_0:sRule:Time_i"
								}
							]
						},
						{
							"number": startTime
						}
					]
				},
					{
						"op": "LT",
						"parameters": [
							{
								"op": "GetValue",
								"parameters": [
									{
										"string": constants.ruleGatewayNodePseudoEUID
									},
									{
										"string": "ep_0:sRule:Time_i"
									}
								]
							},
							{
								"number": endTime
							}
						]
					}];
			},

			_buildPropVal: function (condition) {
				var returnArray = [],
						hasString = _.isString(condition.get("gt")) || _.isString(condition.get("eq")) || _.isString(condition.get("lt")),
						numberOrStringParam;

				// need to check the propVal can be === 0 ( WHEN: smart plug off )
				if (condition.get("gt") || condition.get("gt") === 0) {
					numberOrStringParam = hasString ? {"string": condition.get("gt")} : {"number": condition.get("gt")};

					returnArray.push({
						"op": "GT",
						"parameters": [
							{
								"op": "GetValue",
								"parameters": [
									{
										"string": condition.get("EUID")
									},
									{
										"string": condition.get("propName")
									}
								]
							},
							numberOrStringParam
						]
					});
				}

				if (condition.get("lt") || condition.get("lt") === 0) {
					numberOrStringParam = hasString ? {"string": condition.get("lt")} : {"number": condition.get("lt")};

					returnArray.push({
						"op": "LT",
						"parameters": [
							{
								"op": "GetValue",
								"parameters": [
									{
										"string": condition.get("EUID")
									},
									{
										"string": condition.get("propName")
									}
								]
							},
							numberOrStringParam
						]
					});
				}

				if (condition.get("eq") || condition.get("eq") === 0) {
                    // Bug_SCS-3288 Rule error for Window sensor  
					if (_.isString(condition.get("propName")) || (_.isArray(condition.get("propName")) && condition.get("propName").length===1)) { // single property
                        
                        if(_.isArray(condition.get("propName"))){
                            condition.set("propName",condition.get("propName")[0]);
                        }
                        // Bug_SCS-3288 end
                        
						numberOrStringParam = hasString ? {"string": condition.get("eq")} : {"number": condition.get("eq")};

						returnArray.push({
							"op": "EQ",
							"parameters": [
								{
									"op": "GetValue",
									"parameters": [
										{
											"string": condition.get("EUID")
										},
										{
											"string": condition.get("propName")
										}
									]
								},
								numberOrStringParam
							]
						});
					} else if (_.isArray(condition.get("propName"))) {
						var multiPropObj = {
							"op": "OR",
							"parameters": []
						};

						multiPropObj.parameters = _.map(condition.get("propName"), function (propName) {
							numberOrStringParam = hasString ? {"string": condition.get("eq")} : {"number": condition.get("eq")};

							return {
								"op": "EQ",
								"parameters": [
									{
										"op": "GetValue",
										"parameters": [
											{
												"string": condition.get("EUID")
											},
											{
												"string": propName
											}
										]
									},
									numberOrStringParam
								]
							};
						});

						returnArray.push(multiPropObj);
					}
				}

				return returnArray;
			},

			_getRuleTriggerKey: function (rule) {
				var timeStamp = new Date().toString(),
						ruleTriggerKeyString = "ruleTriggerKey-",
						ruleTriggerKey = rule.get("ruleTriggerKey");

				if (!ruleTriggerKey) {
					ruleTriggerKey = ruleTriggerKeyString +  utils.md5HashString(timeStamp + JSON.stringify(rule));
					rule.set("ruleTriggerKey", ruleTriggerKey);
				}

				return ruleTriggerKey;
			},

			_createTimerRule: function (delayedAction, triggerKey, upStreamRule) {
				var rule = new App.Models.RuleModel(null, { dsn: upStreamRule.get("dsn") });

				rule.set("ruleTriggerKey", triggerKey);
				rule.set("name", upStreamRule.get("name"));

				rule.addAction(delayedAction.get("data"));

				return rule;
			}
		});
	});

	return App.Models.RuleBuilder;
});