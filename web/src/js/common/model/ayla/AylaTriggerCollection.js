"use strict";
define([
	"app",
	"bluebird",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/model/ayla/AylaTrigger.model"
], function (App, P, constants, AylaConfig, AylaBackedMixin) {
	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.AylaTriggerCollecton = B.Collection.extend({
			model: App.Models.AylaTriggerModel,

			initialize: function (data, options) {
				this.gatewayDsn = options.gatewayDsn;
			},

			setDSN: function (dsn) {
				this.gatewayDsn = dsn;
			},

			load: function (isLowPriority) {
				var url, that = this,
						node = App.salusConnector.getDevice(this.gatewayDsn).getGatewayNode();

				if (node) {
					return node.getDevicePropertiesPromise(isLowPriority).then(function () {
						if (node && node.get("AlertMessage")) {

							var urlModel= {
								property_key: that._getAlertMessagePropKey()
							};
							url = _.template(AylaConfig.endpoints.aylaTrigger.fetch)(urlModel);
							return App.salusConnector.makeAjaxGet(url, null, "json", null, {isLowPriority: isLowPriority}).then(function (data) {
								that.set(that.unwrapData(data), { parse: true });
							});
						}

						return P.resolve([]);
					});
				} else {
					return P.resolve([]);
				}

			},

			_getAlertMessagePropKey: function () {
				var key,
						node = App.salusConnector.getDevice(this.gatewayDsn).getGatewayNode(),
						prop = node.get("AlertMessage");

				if (prop.isLinkedDeviceProperty) {
					key = prop.get("getterProperty").get("key");
				} else {
					key = prop.get("key");
				}

				return key;
			},

			updateValues: function (triggers) {
				var that = this;
				_.each(triggers, function (trigger) {
					var node = App.salusConnector.getDevice(that.gatewayDsn).getGatewayNode(),
							triggerModel = that.findWhere({
								value: trigger.triggerKey
							});

					if (!triggerModel && node && node.get("AlertMessage")) {
						triggerModel = new App.Models.AylaTriggerModel({
							value: trigger.triggerKey,
							property_key: that._getAlertMessagePropKey()
						});

						that.add(triggerModel);
					}

					triggerModel.setTriggerValues(trigger);
				});
			},

			getTriggersForRule: function (rule, actionData) {
				var keys = rule.getAlertTriggerKeys(actionData);

				return new Models.AylaTriggerCollecton(this.filter(function (trigger) {
					return _.contains(keys, trigger.get("value"));
				}), { gatewayDsn: this.gatewayDsn });
			},

			unwrapData: function (data) {
				return _.map(data, function (trigger) {
					return trigger.trigger;
				});
			}
		}).mixin(AylaBackedMixin);
	});


	return App.Models.AylaTriggerCollecton;
});