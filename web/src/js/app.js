"use strict";

//This will be the root of the consumer world
define([
	"underscore",
	"jquery",
	"i18next",
	"packery.pkgd",
	"backbone.advice",
	"backbone",
	"backbone.marionette",
	"backbone.stickit",
	"bluebird",
	"common/util/CoreValidator",
	"common/constants",
	"moment"
], function (_, $, i18n, Packery, advice, B, Mn, stickit, P, CoreValidator, constants, moment) {
	
	var cordovaExec = function (logType, args) {
		window.cordova.exec(
			// Register the callback handler
			null,
			// Register the errorHandler
			null,
			// Define what class to route messages to
			"ConnectLoggerPlugin",
			// Execute this method on the above class
			"writeToLog",
			// args = info going into the log
			[logType, args]
		);
	};

	// override logging to create Android/iOS logs
	// TODO - maybe create a json string for the log data and other associated data
	if (window.console && console.log && window.cordova) {
		var oldlog = console.log;
		console.log = function() {
			cordovaExec("log", arguments);
			Array.prototype.unshift.call(arguments, 'Report LOG: ');
			oldlog.apply(this, arguments);
		};
	}

	if (window.console && console.info && window.cordova) {
		var oldinfo = console.info;
		console.info = function() {
			cordovaExec("info", arguments);
			Array.prototype.unshift.call(arguments, 'Report INFO: ');
			oldinfo.apply(this, arguments);
		};
	}

	if (window.console && console.warn && window.cordova) {
		var oldwarn = console.warn;
		console.warn = function() {
			cordovaExec("warn", arguments);
			Array.prototype.unshift.call(arguments, 'Report WARN: ');
			oldwarn.apply(this, arguments);
		};
	}

	if (window.console && console.error && window.cordova) {
		var olderror = console.error;
		console.error = function() {
			cordovaExec("error", arguments);
			Array.prototype.unshift.call(arguments, 'Report ERROR: ');
			olderror.apply(this, arguments);
		};
	}

	// kick off the world

	/**
	 * The following Packery.prototype overwrite methods allow us to center the whole Packery container on tile shuffle.
	 * Sources from:
	 *   https://github.com/metafizzy/packery/issues/8#issuecomment-16780631
	 *   http://codepen.io/desandro/pen/chisC
	 */
	var __resetLayout = Packery.prototype._resetLayout;
	Packery.prototype._resetLayout = function() {
		__resetLayout.call( this );
		// reset packer
		var parentSize = this.size; //this.element.parentNode;
		var colW = this.columnWidth + this.gutter;
		this.fitWidth = Math.floor( ( parentSize.innerWidth + this.gutter ) / colW ) * colW;
		this.packer.width = this.fitWidth;
		this.packer.height = Number.POSITIVE_INFINITY;
		this.packer.reset();
	};

	Packery.prototype._getContainerSize = function() {
		// remove empty space from fit width
		var emptyWidth = 0;
		for ( var i=0, len = this.packer.spaces.length; i < len; i++ ) {
			var space = this.packer.spaces[i];
			if ( space.y === 0 && space.height === Number.POSITIVE_INFINITY ) {
				emptyWidth += space.width;
			}
		}

		return {
			width: this.fitWidth - this.gutter,
			height: this.maxY - this.gutter
		};
	};

	// always resize
	Packery.prototype.needsResizeLayout = function() {
		return true;
	};

	// bridget allow us to place packery and draggabilly on $ as a plugin
	require(["jquery-bridget/jquery.bridget"], function () {
		$.bridget('packery', Packery);
	});

	P.config({
		cancellation: true,
		warnings: false
	});

	var app = new Mn.Application();

	app.addRegions({
		mainRegion: "#bb-app-connected-solution",
		modalRegion: "#bb-modal-region"
	});

	app.vent.on("onLogin", function () {
		if (app.mainRegion && app.mainRegion.currentView) {
			app.mainRegion.currentView.trigger("onLogin");
		}
	});

	app.vent.on("onLoginFailure", function () {
		if (app.mainRegion && app.mainRegion.currentView) {
			app.mainRegion.currentView.trigger("onLoginFailure");
		}
	});
	
	app.showGroupId = null;
	app.showDeviceId = null;

	app.validate = CoreValidator;

	app.onMobile = function () {
		return !!window.cordova;
	};

	app.isDevOrInt = function () {
		var env = this.getEnvironment();
		return env === "local" || env === "int";
	};

	app.isDev = function () {
		var env = this.getEnvironment();
		return env === "local";
	};

	// TODO Add environment addresses for qa, staging and prod
	app.getEnvironment = function () {
		var environment;

		// TODO update this to a non int environment when we wire up the appDelegate.m load with token ios path "salus://secure_token=[[token]]"
		if (app.onMobile()) {
			environment = "prod";
		} else {
			if (window.environment) {
				environment = window.environment;
			} else {
				environment = "prod";
			}
		}

		return environment;
	};

	app.isCordovaApiReady = function (method) {
		if (!this.onMobile()) {
			return false;
		}

		var platform = window.cordova.platformId;
		if (platform === "ios") {
			switch (method) {
				case "logout":
				case "refreshSession":
				case "loginWithUsername":
				case "getDevices":
				case "createDatapoint":
				case "getDatapointsByActivity":
				case "havePinSet":
				case "setPin":
				case "checkPin":
				case "removePin":
					return false; //disabling cordova for ios;
			}
		}

		if (platform === "android") {
			switch (method) {
				case "logout":
				case "refreshSession":
				case "loginWithUsername":
				case "getProperties":
				case "getDevices":
				case "createDatapoint":
				case "getDatapointsByActivity":
					return false; //disabling cordova for android;
			}
		}

		return false;
	};

	app.getIsEU = function () {
		return this.isEU;
	};

	app.setDataCollectionOff = function (dataCollectionOff) {
		this._dataCollectionOff = dataCollectionOff;
	};

	app.getDataCollectionOff = function () {
		return this._dataCollectionOff;
	};

	app.getCurrentGateway = function () {
		return app.salusConnector.get("currentGateway");
	};

	app.getCurrentGatewayDSN = function () {
		var gateway = app.salusConnector.get("currentGateway");

		return gateway ? gateway.get("dsn") : constants.noGateway;
	};

	app.hasCurrentGateway = function() {
		return !!app.getCurrentGateway();
	};

	app.hideHeader = function () {
		$(".bb-region-header").css("z-index", -1);
	};

	app.showHeader = function () {
		$(".bb-region-header").css("z-index", 1000);
	};

	app.showModal = function () {
		if (this.modalRegion && this.modalRegion.currentView) {
			this.modalRegion.currentView.showModal();
		}
	};

	app.isBreakpoint = function (breakpoint) {
		return $(".bb-device-" + breakpoint).is(":visible");
	};

	app.currentBreakpoint = function () {
		// find the visible div and grab its breakpoint key
		var classes = $(".bb-breakpoint-detector:visible").attr("class").split(" ");

		// find the class that contains the breakpoint key
		var devClass = _.find(classes, function (classString) {
			return classString.indexOf("bb-device-") >= 0;
		});

		// class is of format "bb-device-{{my_breakpoint}}
		return devClass.split("-")[2];
	};

	app.isMSBrowser = function () {
		var userAgent = window.navigator.userAgent;
		return !!userAgent.match(/Edge\//) || !!userAgent.match(/Trident\//);
	};

	app.isIE11 = function () {
		return app._isIE11;
	};

	app._setupBreakpointListeners = function () {
		var that = this;

		this._currentWindow = that.currentBreakpoint();

		$(window).bind("resize", function () {
			var newWindow = that.currentBreakpoint();
			// if the breakpoints have changed trigger and app wide event
			if (newWindow !== that._currentWindow) {
				app.vent.trigger("changed:viewport", that._currentWindow, newWindow);
				that._currentWindow = newWindow;
			}
		});
	};

	app.log = function (message) {
		console.log(message);
	};

	var _sendLogMessage = function (message /*, messageType*/) {
		console.log(message);
		//try {
		//	message = message || "";
		//
		//	var promise,
		//		eventObj = {
		//			"type": messageType,
		//			"message": message ? message.toString() : message || ""
		//		};
		//
		//
		//	if (message.stack) {
		//		eventObj.stack = message.stack;
		//	}
		//
		//	promise = app.salusConnector.log(eventObj).catch(function () {
		//		//Eat network errors. They have all ready been logged to console.
		//		//This will pevent an infiite loop as we also logged unhandled Rejections.
		//	});
		//} catch (e) {
		//	//Use console not app.error to prevent infinite loop.
		//	console.error(e);
		//}
	};

	app.warn = function (message) {
		if (!app.isDev()) {
			_sendLogMessage(message, "warn");
		}

		console.warn(message);
	};

	app.error = function (message) {
		if (!app.isDev()) {
			_sendLogMessage(message, "error");
		}

		console.error(message);
	};

	app.hideModal = function () {
		var that = this;
		if (this.modalRegion && this.modalRegion.currentView) {
			this.modalRegion.currentView.$el.on("hidden.bs.modal", function () {
				that.modalRegion.currentView.$el.off();
				that.modalRegion.empty();
			});
			this.modalRegion.currentView.hideModal();
		}
	};
	
	app.pathname = function () {
		if (!app.onMobile()) {
			return window.location.pathname;
		} else if (app.onMobile()) {
			return window.location.hash;
		}
	};

	app.navigate = function (route, options) {
		if (app.pathname() === route) {
			return false;
		}
		options = options || {};
		options = _.extend(options, {trigger: true});
		var matched = B.history.navigate(route, options);

		if (!matched && _.isBoolean(matched)) {
			//http is for external links.  internal links will be / or dashboard
			if (route.indexOf("mailto:") !== 0 && route.indexOf("http") !== 0 && route.indexOf("#noOp") !== 0) {
				app.Consumer.Controller.showErrorPage(constants.errorTypes.error404);
			}
		}
	};

	// changes the route without triggering the navigate
	app.changeRoute = function (route) {
		B.history.navigate(route, { replace: true });
	};

	app.refreshPage = function () {
		B.history.loadUrl(B.history.fragment);
	};

	app.i18n = i18n;

	app.translate = function (key, options) {
		return app.i18n.t(key, options);
	};

	app.rootPath = function (path) {
		if (!path) {
			return path;
		}

		if (path.charAt(0) !== "/") {
			return path;
		}

		if (window.cordova) {
			return "." + path;
		}

		return path;
	};

	app.logout = function () {
		app.salusConnector.logout().then(function () {
            app.changeRoute("");
			location.reload();
		});
	};

	var lng = navigator.language || navigator.browserLanguage;
	var translationPath = app.rootPath('/translations/__lng__/__ns__.json'),
		languageWhiteList = [
			//This is the complete list of languagaes that the app is going to support.
			//We only comment in the language that are complete and currently support.
			//NOTE: If you enable a new language you must also upload email templates for them.
			"en", //Use en for now as this is our development strings, save en-us for approved copy.
			//"en-us", //"English (US)"
			"en-uk", //"English (UK)"
			"nl", //"Dutch"
			//"hu", //"Hungarian"
			"ro", //"Romanian"
			//"es", //"Spanish"
			"ru", //"Russian"
			//"cs", //"Czech"
			"fr", //"French"
			//"it", //"Italian"
			//"sk", //"Slovak"
			"sv", //"Swedish"
			"da", //"Danish"
			"de", //"German"
			"pl", //"Polish"
			//"sl", //"Slovenian"
			"no" //"Norwegian"
		];


	app.addInitializer(function () {
		if (app.onMobile()) {
			var storageItem =  window.localStorage.getItem("isEU");

			if (!storageItem) {
				window.localStorage.setItem("isEU", true);
				this.isEU = true;
			} else {
				this.isEU = storageItem === "true";
			}
		} else {
			var host = window.location.origin || (window.location.protocol + "//" + window.location.host);
			if (host.indexOf("eu.salusconnect.io") >= 0) {
				this.isEU = true;
			} else if (host === "http://localhost:8080" || host === "http://210.75.16.197:8080") {
				this.isEU = true;
			} else {
				this.isEU = false;
			}
		}

		if (app.backgroundImageUrl) {
			app.Consumer.Login.backgroundImageUrl = app.backgroundImageUrl;
		}

		if (app.isDevOrInt()) {
			window.debugApp = app;
		}

		this._isIE11 = !!$("html").hasClass("preserve3d");

		app._setupBreakpointListeners();
		// in order to prevent circular dependencies, we wait until the app has started to add the salus connector
		require(["common/model/api/SalusConnector"], function (SalusConnector) {
			// the salus connector is needed by the app to determine if the user is logged in, so we want to attach
			// it before we start navigating the app
			app.salusConnector = new SalusConnector();
			app.salusConnector.startSessionManager();

			app.startHistory = function () {
				var matched;
				if (app.onMobile()) {
					matched = B.history.start({});
				} else {
					matched = B.history.start({ pushState: true, root: "/"});
				}

				if (!matched && _.isBoolean(matched)) {
					// no matched route

					if (app.salusConnector.isLoggingIn()) {
						// wait for logging in as this we will reach this long before even attempting to log in.
						app.listenToOnce(app.vent, "onLogin", function () {
							app.Consumer.Controller.showErrorPage(constants.errorTypes.error404);
						});
						app.listenToOnce(app.vent, "onLoginFailure", function () {
							app.Consumer.Controller.showErrorPage(constants.errorTypes.error404);
						});
					} else {
						app.Consumer.Controller.showErrorPage(constants.errorTypes.error404, { isUnAuthenticated: true});
					}

				}
			};

			var i18nOptions = {
				lng: lng,
				ns: 'consumer',
				lowerCaseLng: true,
				resGetPath: translationPath,
				lngWhitelist: languageWhiteList
			};

			if (!app.isDev()) {
				//falls back to en in production mode.
				i18nOptions.fallbackLng = 'en';
			}

			app.i18n.init(i18nOptions, function (/*er, t*/) {
				$(app.mainRegion.$el).i18n();
				app.startHistory();
				
				var currentLang;

				try {
					currentLang = app.i18n.lng();
					moment.locale(currentLang);
				} catch (e) {
					app.warn("Can't change moment languge to: " + currentLang || ""  + "   " + e);
				}
			});
		});
	});

	/**
	 * Stop the loading of large images, new windows, and other objects whose loading is deferred.
	 */
	app.stopWindow = function () {
		if (window.stop) {
			window.stop();
		} else if (window.document.execCommand) {
			//ie fix
			window.document.execCommand('Stop');
		}
	};

	/**
	 * Updates the apps language for displayed text.
	 * @param language language code
	 * @returns {Promise} success when language file has been loaded.
	 */
	app.changeLanguage = function (language) {
		if (app.i18n) {
			return new P(function (fulfill, reject) {
				app.i18n.setLng(language, function (err, t) {
					if (err) {
						reject(err);
					} else {
						fulfill(t);
					}
				});
			}).then(function () {
				if (moment && moment.locale) {
					moment.locale(app.i18n.lng());
				}
			});
		} else {
			return P.reject("i18n not ready");
		}
	};

	/**
	 * Creates a simple hash from the giving string. Not to be used for security.
	 * @param string value to hash
	 * @returns {number}
	 */
	app.hashString = function (string) {
		var hash = 0, i, chr, len;
		if (string.length === 0) {
			return hash;
		}

		for (i = 0, len = string.length; i < len; i++) {
			chr   = string.charCodeAt(i);
			/* jshint ignore:start */
			//We want to use bitwise operators here.
			hash  = ((hash << 5) - hash) + chr;
			hash |= 0; // Convert to 32bit integer
			/* jshint ignore:end */
		}
		return hash;
	};

    // Dev_SCS-3284 Start
    app.getIsDemo = function() {
        return window.sessionStorage.getItem("isDemo");
    },
    // Dev_SCS-3284 End

//	app.getIP = function() {
//		$.getJSON('//jsonip.com/?callback=?', function(data) {
//			app.ip = data.ip;
//		});
//	};

	// listener for uncaught errors [ex. throw new Error('Hello')]
	window.onerror = function (fullErr, path, line) {

		app.error(fullErr + "\n\t\t" + path + ":" + line);
		return true;
	};

	window.addEventListener("unhandledrejection", function(e) {
		// NOTE: e.preventDefault() must be manually called to prevent the default
		// action which is currently to log the stack trace to console.warn
		e.preventDefault();

		var reason = e.detail.reason;

		app.warn(reason);
	});

	B.history.on("all", function (route, router) {
		try {
			// Close the mobile menu drop-down if open, on every route change.
			app.modalRegion.empty({preventDestroy: true});
			app.showHeader();
			
			if($(".modal-backdrop").length){
				$(".modal-backdrop").remove();
			}
			// re-enable scroll
			$("body").removeClass("modal-open");
		}catch(e) {
			console.error("error occurred while close mobile menu");
		}
		
	});

	return app;
});
