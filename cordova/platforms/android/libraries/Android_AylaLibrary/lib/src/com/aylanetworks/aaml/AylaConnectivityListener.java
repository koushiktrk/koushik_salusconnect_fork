//
//  AylaConnectivityListner.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 12/2/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//

package com.aylanetworks.aaml;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

/**
 * This is a broadcast receiver that is listening broadcasts from android system, when 
 * detecting connection status changes, service/LAN, and initialize connection status 
 * scan process. 
 * 
 * Need to register statically in app`s manifest file. 
 * */
public class AylaConnectivityListener extends BroadcastReceiver {
	
	private static Context context;
	private static AylaConnectivityListener receiver;
	
	private static final String tag = "AylaConnectivityListener";
	static Intent intent = null;

	static void disableStaticReceiver(final Context c)
	{
		if (c == null) {
			return;
		}
		ComponentName receiver = new ComponentName(c, AylaConnectivityListener.class);
		PackageManager pm = c.getPackageManager();

		int newState = PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
		try {
			pm.setComponentEnabledSetting(receiver, newState, PackageManager.DONT_KILL_APP);
		} catch (Exception e) {
			AylaSystemUtils.saveToLog("%s, %s, error message:%s.", "D", tag, e.getMessage());
		}
	}
	
	private AylaConnectivityListener(final Context c) { 
		//param does nothing for now, just make the private constructor different from the public one.
		super();
	}
	
	/**
	 * For backward compatibility, android system would call this if broadcast receiver is 
	 * claimed in manifest file Developers should not call this themselves. 
	 * */
	public AylaConnectivityListener() {
		super();
	}
	
	/** 
	 * This would be called when it gets registered, and every time a broadcast message comes in.
	 * 
	 * Reference <a href="https://developer.android.com/reference/android/content/BroadcastReceiv	\n
	 * er.html">Android Broadcast Receiver</a> for details.
	 * */  
	@Override
	public void onReceive(Context c, Intent thisIntent) {
		if (context==null && c != null) {
			context = c;
		}
		if (c == null) {
			context = AylaNetworks.appContext;
		}
		if (c == null) {
			AylaSystemUtils.saveToLog("%s, %s, %s.", "E", tag + " AylaNetworks.appContext is null", "onReceive");
			return;
		}
		
		if ( context != null ) {
			AylaSystemUtils.saveToLog("%s, %s, %s.", "I", tag + " onReceive() called",  "onReceive");
			AylaSystemUtils.serviceReachableTimeout = AylaNetworks.AML_SERVICE_REACHABLE_TIMEOUT; // always test if connectivity changes
			//AylaLanMode.discovery.initialize(); // force reinit to handle network change
			AylaReachability.determineReachability(false);
		}
	}
	
	static synchronized AylaConnectivityListener getInstance(final Context c) {
		if (receiver == null) {
			receiver = new AylaConnectivityListener(c);
		}
		return receiver;
	}// singleton

}// end of AylaConnectivityListener class     



