/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aylanetworks.AMLUnitTest;

import com.aylanetworks.aaml.AylaNetworks;
import com.aylanetworks.aaml.AylaSystemUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Main UI for the demo app.
 */
public class PushNotification extends AylaSystemUtils {

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    
	
	// TODO: Set this regular expression string to match your OEM product SSID
	//final String gblAmlDeviceSsidRegex1 = "^Ayla-[0-9A-Fa-f]{12}|^T-Stat-[0-9A-Fa-f]{12}";

    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    //String SENDER_ID = "103052998040";

    /**
     * Tag used on log messages.
     */
//    private static final String TAG = "GCM Demo";
    private static final String TAG = PushNotification.class.getSimpleName();

    //TextView mDisplay;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    //Context context;

    public static String registrationId; // required to set a push notification

    public void init(String senderId, String emailAddress, String appId) {
    	boolean clearOnAppVersionChange = false;
        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(appContext);
            registrationId = getRegistrationId(appContext, clearOnAppVersionChange);
            
            if (senderId.equals("unregister")) {
            	unregisterInBackground();
            	return;
            }

            if (registrationId.isEmpty()) {
                registerInBackground(senderId, emailAddress, appId);
            } else {
            	emailRegId(emailAddress, appId);
            }
        } else {
        	saveToLog("%s, %s, %s:%s, %s", "I", "amca.PushNotification", "PlayServicesAPK", "not found", "init");
        }
    }
    
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(AylaNetworks.appContext);
        if (resultCode != ConnectionResult.SUCCESS) {
            Log.i(TAG, "This device does not support Google Play Services not supported.");
            saveToLog("%s, %s, %s:%d, %s", "I", "amca.PushNotification", "resultCode", resultCode, "checkPlayServices");
            
            String toastMessage = "Push notificaitons not supported";
            Toast.makeText(AylaNetworks.appContext, toastMessage, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing registration ID.
     */
    private String getRegistrationId(Context context, boolean clearOnAppVersionChange) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
        	saveToLog("%s, %s, %s:%s, %s", "I", "amca.PushNotification", "registration", "not found", "getRegistrationId()");
            return "";
        }
        // Check if the app was updated. If so, optionally clear the registration ID
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
        	saveToLog("%s, %s, %s:%s, %s", "I", "amca.PushNotification", "App version", "changed", "getRegistrationId()");
            
            if (clearOnAppVersionChange) {
            	registrationId = "";
            }
        }
        return registrationId;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground(final String senderId, final String emailAddress, final String appId) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(appContext);
                    }
                    registrationId = gcm.register(senderId);
                    msg = "Device registered, registration ID=" + registrationId;
                    
                    //gcm.unregister();
                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    sendRegistrationIdToBackend(emailAddress, appId);

                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(appContext, registrationId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
               // mDisplay.append(msg + "\n");
            }
        }.execute(null, null, null);
    }
    
	/**
	 * Generally not required, but included for completeness
	 * Calling unregister() stops any messages from the server.
	 * You should rarely (if ever) need to call this method.
	 * Not only is it expensive in terms of resources, but it invalidates your registration ID,
	 * which you should never change unnecessarily. 
	 * A better approach is to simply have your server stop sending messages.
	 * Only use unregister if you want to change your sender ID.
	 */
    private void unregisterInBackground() {
    	new AsyncTask<Void, Void, String>() {
    		@Override
    		protected String doInBackground(Void... params) {
    			String msg = "";
    			try {
    				if (gcm == null) {
    					gcm = GoogleCloudMessaging.getInstance(appContext);
    				}
    				msg = "Device registered, registration ID=" + registrationId;

    				gcm.unregister();

    				// Persist the regID - no need to register again.
    				storeRegistrationId(appContext, "");
    			} catch (IOException ex) {
    				msg = "Error :" + ex.getMessage();
    				// If there is an error, don't just keep trying to register.
    				// Require the user to click a button again, or perform
    				// exponential back-off.
    			}
    			return msg;
    		}

    		@Override
    		protected void onPostExecute(String msg) {
    			// mDisplay.append(msg + "\n");
    		}
    	}.execute(null, null, null);
    }

    // Send an upstream message.
    void sendPushNotification() {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String msg = "";
                    try {
                        Bundle data = new Bundle();
                        data.putString("my_message", "Hello World");
                        data.putString("my_action", "com.google.android.gcm.demo.app.ECHO_NOW");
                        String id = Integer.toString(msgId.incrementAndGet());
                        gcm.send(AMLUnitTestConfig.senderId + "@gcm.googleapis.com", id, data);
                        msg = "Sent message";
                    } catch (IOException ex) {
                        msg = "Error :" + ex.getMessage();
                    }
                    return msg;
                }

                @Override
                protected void onPostExecute(String msg) {
                	saveToLog("%s, %s, %s:%s, %s", "I", "amca.PushNotification", "msg", msg, "onPostExecute");
                    //mDisplay.append(msg + "\n");
                }
            }.execute(null, null, null);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGcmPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return appContext.getSharedPreferences(PushNotification.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend(String emailAddress, String appId) {
      // Your implementation here.
    	emailRegId(emailAddress, appId);
    }
    
    final static MediaPlayer mp = new MediaPlayer();
    static boolean playSound(String audioFileName) {
    	boolean playedSound = false;
    	
    	if(mp.isPlaying())
        {  
            mp.stop();
            mp.reset();
        } 
        try {
        	File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC); // "/storage/emulated/0/Music"
            File soundFile = new File(path, audioFileName);
            mp.setDataSource(soundFile.getPath());
            //mp.setDataSource(path+"/"+audioFileName);
            mp.prepare();
            mp.start();
            playedSound = true;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    	return playedSound;
    }

    public void emailRegId(String emailAddress, String appId) {
        String emailSubject = "Registering device for " + appId;
        String emailMessage = registrationId;
        // below is the code for sending an email
        Intent emailIntent = new Intent(
                android.content.Intent.ACTION_SEND);
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, emailAddress);
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, emailSubject);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, emailMessage);
       //appContext.startActivity(emailIntent); // uncomment for development/testing
    }
}// end of PushNotification class                






