"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusTextBox.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view"
], function (App, constants, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $, _) {
		Views.SignInWell = Mn.ItemView.extend({
			id: "sign-in-well",
			template: consumerTemplates["login/signInWell"],
			events: {
				"keydown": "handleEnterButtonPress"
			},
			initialize: function () {
				_.bindAll(this, "handleButtonClicked");

				this._sendConfirmAccountWithToken();

				this.signInButton = new App.Consumer.Views.SalusButtonPrimaryView({
					id: "sign-in-btn",
					buttonTextKey: "login.login.button",
					clickedDelegate: this.handleButtonClicked
				});

				this.emailBox = new App.Consumer.Views.SalusTextBox({
					wrapperClassName: "col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 email-text-wrp",
					iconPath: App.rootPath("/images/icons/icon_mail_blue.svg"),
					iconAlt: App.translate("login.login.emailLabel"),
					inputPlaceholder: App.translate("login.login.emailLabel"),
					inputType: "email",
					isRequired: true
				});

				this.passwordBox = new App.Consumer.Views.SalusTextBox({
					wrapperClassName: "col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 password-text-wrp",
					iconPath: App.rootPath("/images/icons/icon_password_blue.svg"),
					iconAlt: App.translate("login.login.passwordLabel"),
					inputPlaceholder: App.translate("login.login.passwordLabel"),
					inputType: "password",
					isRequired: true
				});

				this.keepLoggedInCheckbox = new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						name: "keepLoggedIn",
						labelTextKey: "login.login.keepLoggedIn",
						secondaryIconClass: "",
						isChecked: false
					})
				});
			},

			onRender: function () {
				this.$(".email-text-row").replaceWith(this.emailBox.render().$el);
				this.$(".password-text-row").replaceWith(this.passwordBox.render().$el);
				this.$("#sign-in-btn-container").append(this.signInButton.render().$el);
				this.$(".bb-checkbox").append(this.keepLoggedInCheckbox.render().$el);
                if (App.onMobile()) {
					this.$(".bb-silo-switch-container").removeClass('hidden');
					this.listenTo(App.vent, "swap:silo", this._setSwitchText);
					this._setSwitchText();
                    this.$(".bb-silo-switch").click(function () {
                        App.swapSilos();
                    });
				}
			},
            
            _setSwitchText: function () {
				this.$(".bb-silo-switch").text(App.translate("login.login.siloMessage." + (App.getIsEU() ? "eu" : "us")));
			},
            
			onDestroy: function () {
				this.emailBox.destroy();
				this.passwordBox.destroy();
				this.signInButton.destroy();
				this.keepLoggedInCheckbox.destroy();
			},
			goToHomePage: function () {
				App.navigate("dashboard"); //TODO: Make this go to home page. For now it goes to index.
			},
			/**
			 * Trigger the form submit when the enter key is pressed
			 */
			handleEnterButtonPress: function (event) {
				if (event.which && event.which === 13) {
					this.handleButtonClicked();
				}
			},
			handleButtonClicked: function () {
				var that = this,
					sc = App.salusConnector,
						checked;

				var email = this.emailBox.val();
				var password = this.passwordBox.val();

				this.emailBox.clearError();

				checked = this.keepLoggedInCheckbox.getIsChecked();
				sc.setKeepSignedIn(!!checked);

				if (email && password) {
					this.signInButton.showSpinner();
					var loginPromise = sc.loginWithUsername(email, password);
					loginPromise.then(function () {
                        App.salusConnector.setAylaUserPassword(password);
						// firstTimeLogingRouteCheck routes based on our device collectin so wait until we have it to call it
						sc.getDataLoadPromise(["devices"]).then(function () {
							sc.firstTimeLoginRouteCheck();
							that.signInButton.hideSpinner();
						});
					}).catch(function (e) {
						that.passwordBox.clearVal();
						that.signInButton.hideSpinner();

						if (e.status === 401) {
							if (e.responseText.indexOf("Invalid email or password.") > -1) {
								that.emailBox.registerError(App.translate("login.login.error.invalidUsernamePassword"));
								return;
							} else if (e.responseText.indexOf("Your account is locked.") > -1) {
								that.emailBox.registerError(App.translate("login.login.error.accountLocked"));
								return;
							}
						}
						that.emailBox.registerError(App.translate("login.login.error.otherInvalidError", {statusCode: e.status}));

					});
				} else {
					that.emailBox.registerError(App.translate("login.login.error.invalidUsernamePassword"));
				}
			},
			/**
			 * This handles the case where a new user clicks the click back link in their new user e-mail and it
			 * drops them to this page with the url parameter "?confirmation_token=(token)". We then grab this token and
			 * make an ajax call to Ayla, confirming their account.
			 * @private
			 */
			_sendConfirmAccountWithToken: function () {
				var params = this.model.get("params");

				if (params && params.secureToken) {
					App.salusConnector.confirmUserWithEmailVerificationToken(params.secureToken).catch(function (e) {
						// TODO Surface some form of this error to the user
						App.log("Account confirmation with token failed with error: " + e.responseText);
					});
				}
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Login.Views.SignInWell;
});
