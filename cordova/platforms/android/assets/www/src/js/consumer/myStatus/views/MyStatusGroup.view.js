"use strict";

define([
	"app",
	"common/config",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs",
	"consumer/myStatus/views/NewMyStatusPage.view",
	"consumer/equipment/views/myEquipment/AddNewTile.view"
], function (App, config, constants, templates, SalusView, VendorfyCss) {

	App.module("Consumer.MyStatus.Views", function (Views, App, B, Mn, $, _) {
		Views.MyStatusCompleteTileView = Mn.ItemView.extend({
			className: "myStatus-complete-tile col-xs-6 col-sm-4",
			template: templates["myStatus/myStatusTile"],
			attributes: {
				role: "button"
			},
			ui: {
				myStatusTileBackgroundRegion: ".bb-myStatus-tile-background"
			},
			bindings: {
				".bb-tile-text": {
					observe: "name"
				},
				".bb-myStatus-selected": {
					observe: "selected",
					update: function ($el, selected) {
						if (App.hasCurrentGateway()) {
							$el.toggleClass("selected", selected).toggleClass("deselected", !selected);
						} else {
							$el.addClass("invalid").removeClass("selected").removeClass("deselected");
						}
					}
				}
			},
			events: {
				"click": "_handleTileClick"
			},
			initialize: function () {
				_.bindAll(this, "_handleTileClick");
			},
			onRender: function () {
				var iconPaths = constants.myStatusIconPaths,
					colors = constants.getStatusIconBackgroundColor(this.model.get("icon"));

				VendorfyCss.formatBgGradientWithImage(
					this.ui.myStatusTileBackgroundRegion,
					"center 50%/48% 48%",
					"no-repeat",
					App.rootPath(iconPaths[this.model.get("icon")]),
					"145deg",
					colors.topColor, //top color
					"0%",
					colors.botColor, //bottom color
					"100%",
					"/images/icons/myStatus/icon_question_mark.svg"
				);

				this._styleNewestStatus();
			},
			onShow: function () {
				if (this.$('.bb-new-status-tile-text').length !== 0) {
					this.$('.bb-new-status-tile-text').text(App.translate("myStatus.newStatusAdded"));
					this.$('.bb-new-status').fadeOut(3000);
				}
			},
			_styleNewestStatus: function () {
				var key = App.Consumer.MyStatus.Controller.newMyStatus;

				if (key && this.model.get("key") === key) {
					var textHtml = '<div class="bb-new-status new-status bb-new-status-tile-text new-status-tile-text text-center center-horizontal-absolute"></div>',
						iconHtml = '<div class="bb-new-status new-status new-status-tile-icon center-horizontal-absolute"></div>',
						firstChild = this.ui.myStatusTileBackgroundRegion.children().first();

					firstChild.append(textHtml);
					firstChild.append(iconHtml);

					App.Consumer.MyStatus.Controller.newMyStatus = null;
				}
			},
			_handleTileClick: function () {
				App.Consumer.MyStatus.Controller.currentPendingRuleGroup = this.model;
				App.navigate("myStatus/" + App.getCurrentGatewayDSN() + "/makeStatus");
			}
		}).mixin([SalusView]);

		Views.MyStatusGroups = Mn.CollectionView.extend({
			id: "myStatus-collection",
			className: "row",
			template: false,
			childView: Views.MyStatusCompleteTileView,
			onRender: function () {
				var textKey, destination;

				if (!this.addStatusTile) {
					if (App.hasCurrentGateway()) {
						textKey = "myStatus.addNewStatus";
						destination = "myStatus/" + App.getCurrentGatewayDSN() + "/newStatus";
					} else {
						textKey = "myStatus.addNewGateway";
						destination = "setup/gateway";
					}

					this.addStatusTile = new App.Consumer.Equipment.Views.AddNewTileView({
						i18nTextKey: textKey,
						size: "regular",
						classes: "myStatus-complete-tile",
						clickDestination: destination
					});
				}

				this.$el.prepend(this.addStatusTile.render().$el);
			},

			onDestroy: function () {
				this.addStatusTile.destroy();
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.MyStatus.Views.MyStatusGroups;
});

