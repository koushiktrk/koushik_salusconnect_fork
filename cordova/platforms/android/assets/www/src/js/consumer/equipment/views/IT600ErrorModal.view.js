"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/views/widgets/StatusWidgetContent.view"
], function (App, constants, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views.Modal", function (Views, App, B, Mn) {
		Views.IT600ErrorItemView = Mn.ItemView.extend({
			className: "error-item col-xs-12",
			template: consumerTemplates["equipment/it600ErrorItem"],
			bindings: {
				".bb-error-message": {
					observe: "error",
					onGet: function (errorNumber) {
						return App.translate("equipment.it600Warnings." + errorNumber);
					}
				}
			}
		}).mixin([SalusView]);

		Views.IT600ErrorCollectionView = Mn.CollectionView.extend({
			className: "error-collection row",
			childView: Views.IT600ErrorItemView
		}).mixin([SalusView]);

		Views.IT600ErrorModalView = Mn.LayoutView.extend({
			id: "it600-error-modal",
			className: "modal-body",
			template: consumerTemplates["equipment/it600ErrorModal"],
			regions: {
				errorCollectionRegion: ".bb-error-collection-region"
			},
			initialize: function (options) {
				this.errorCollectionView = new Views.IT600ErrorCollectionView({collection: new B.Collection(options.errors)});
			},

			onRender: function () {
				this.errorCollectionRegion.show(this.errorCollectionView);
			}
		}).mixin([SalusView]);
	});
});
