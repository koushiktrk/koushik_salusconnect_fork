"use strict";

define([
    "app",
    "bluebird",
    "common/constants",
    "common/config",
    "consumer/models/HeaderLinkViewModel",
    "consumer/views/base/MainLayout.view",
    "consumer/views/base/Header.view",
    "consumer/views/base/Footer.view",
    "consumer/views/base/HeaderLinkDesktop.view",
    "consumer/views/base/HeaderLinkMobile.view",
    "consumer/views/SalusErrorPage.view",
    "consumer/views/Error404Page.view",
    "consumer/views/SalusAlertModal.view",
    "consumer/models/SalusAlertModalModel",
    "consumer/views/modal/ModalCloseButton.view"
], function(App, P, constants) {

    App.module("Consumer", function(Consumer, App, B, Mn, $, _) {
        Consumer.Controller = {
            isErrorPage: false,
            _getHeaderLinkCollection: function(isAuthenticatedRoute, userFirstName) {
                var returnModel, hrefSetupEquipment;
                if (App.getCurrentGateway()) {
                    hrefSetupEquipment = "/setup/pairing";
                } else {
                    hrefSetupEquipment = "/setup/gateway";
                }

                if (isAuthenticatedRoute) {
                    returnModel = new App.Consumer.Models.LinkViewModelCollection([{
                        type: "link",
                        displayTextKey: "common.header.home",
						href: "/dashboard",
                        classes: "margin-r-5 top-level-link"
                    }, {
                        type: "dropdown",
                        menuId: "equipment",
                        displayTextKey: "common.header.equipment",
                        classes: "margin-r-5 top-level-link",
                        subLinks: [{
                            displayTextKey: "common.header.allEquipment",
                            href: "/equipment"
                        }, {
                            displayTextKey: "common.header.oneTouch",
                            href: "/oneTouch"
                        }, {
                            displayTextKey: "common.header.gateways",
                            href: "/gateways"
                        }]
                    }, {
                        type: "dropdown",
                        menuId: "settings",
                        displayTextKey: "common.header.settings",
                        classes: "margin-r-5 top-level-link",
                        subLinks: [{
                            displayTextKey: "common.header.profile",
                            href: "/settings/profile"
                        }, {
                            displayTextKey: "common.header.setupEquipment",
                            href: hrefSetupEquipment
//							clickHandler: function () {
//								if (App.getCurrentGateway()) {
//									App.navigate("setup/pairing");
//								} else {
//									App.navigate("setup/gateway");
//								}
//
//								return true;
//							}
                        }, {
                            displayTextKey: "common.header.dataCollection",
                            href: "/settings/dataCollection"
                        }]
                    }, {
                        type: "dropdown",
                        menuId: "help",
                        displayTextKey: "common.header.help",
                        classes: "margin-r-5 top-level-link",
                        subLinks: [{
                            displayTextKey: "common.header.faq",
                            href: "http://salusinchelp.zendesk.com/",
                            target: "_blank"
                        }, {
                            displayTextKey: "common.header.contactSupport",
                            href: "mailto:support@salusconnect.io"
                        }, {
                            displayTextKey: "common.header.about",
                            href: "/settings/about"
                        }]
                    }, {
                        type: "myStatus",
                        menuId: "myStatus",
                        displayTextKey: "common.header.myStatus",
                        classes: "top-level-link",
                        href: "#"
                    }, {
                        collection: [],
                        displayTextNonKey: userFirstName,
                        classes: "top-level-link",
                        isUserName: true,
                        href: "/settings/profile"
                    }, {
                        type: "dropdown",
                        menuId: "legal",
                        displayTextKey: "common.header.legal",
                        classes: "margin-r-5 top-level-link",
                        subLinks: [{
                            displayTextKey: "setupWizard.newUserForm.formFields.privacyPolicyLinkLabel",
                            href: "/legal/privacy"
                        }, {
                            displayTextKey: "setupWizard.newUserForm.formFields.termsAndConditionsLinkLabel",
                            href: "/legal/terms"
                        }]
                    }, {
                        type: "link",
                        displayTextKey: "common.header.logout",
                        clickHandler: App.logout,
                        classes: "logout top-level-link"
                    }]);

                    if (App.isDevOrInt()) {
                        var links = returnModel.findWhere({
                            displayTextKey: "common.header.help"
                        }).get("subLinks");
                        var obj = {
                            displayTextNonKey: "Nav",
                            href: "/temp-nav"
                        };
                        links.push(obj);
                    }
                } else {
                    returnModel = new App.Consumer.Models.LinkViewModelCollection([{
                            type: "link",
                            displayTextKey: "common.header.home",
                            href: "/",
                            classes: "top-level-link"
                        }, {
                            type: "dropdown",
                            menuId: "help",
                            displayTextKey: "common.header.help",
                            classes: "margin-r-5 top-level-link",
                            subLinks: [{
                                displayTextKey: "common.header.faq",
                                href: "http://salusinchelp.zendesk.com/",
                                target: "_blank"
                            }, {
                                displayTextKey: "common.header.contactSupport",
                                href: "mailto:support@salusconnect.io"
                            }, {
                                displayTextKey: "common.header.about",
                                href: "/settings/about"
                            }]
                        }
//						{
//							type: "link",
//							displayTextKey: "common.header.help",
//							classes: "top-level-link"
//						}
                    ]);
                }

                return returnModel;
            },
			showLayout: function (layoutView, isAuthenticatedRoute, pagePromiseDependencies) {
				var headerView, footerView,
					that = this, layout = new App.Consumer.Views.MainLayout();

					layout.listenTo(layout, "show", function () {
                        $('body').removeClass("dashboard-background-color");

                        headerView = new App.Consumer.Views.HeaderView({
                            isError: that.isErrorPage && that._errorType === constants.errorTypes.REQUEST
                        });
                        footerView = new App.Consumer.Views.FooterView();

                        headerView.listenToOnce(headerView, "show", function() {
                            if (App.salusConnector.isLoggedIn() || App.salusConnector.isLoggingIn()) {
                                App.salusConnector.getDoOnLoginPromise().then(function() {
                                    var user = App.salusConnector.getSessionUser();

                                    if (!that.isErrorPage || that._errorType === constants.errorTypes.error404) {
										//headerLinkCollection will be cleaned up with the destructor of HeaderMobileMenuView
                                        var headerLinkCollection = that._getHeaderLinkCollection(isAuthenticatedRoute, user.get("firstname")),
                                            headerLinkDesktopCollectionView = new App.Consumer.Views.HeaderLinkDesktopCollectionView({
                                                collection: headerLinkCollection
                                            });

                                        headerView.mobileMenu = new App.Consumer.Views.HeaderMobileMenuView({
                                            collection: headerLinkCollection
                                        });

                                        if (headerView.desktopLinks) {
                                            headerView.desktopLinks.show(headerLinkDesktopCollectionView);
                                        }

                                        if (!App.hasCurrentGateway()) {
                                            App.listenTo(App.salusConnector, "set:currentGateway", function() {
                                                // this will trigger onLogin, but we may still not have a gateway
                                                if (App.hasCurrentGateway() && headerView.desktopLinks) {
                                                    // refresh the header to have myStatus show up
                                                    headerView.desktopLinks.empty();

                                                    headerView.desktopLinks.show(new App.Consumer.Views.HeaderLinkDesktopCollectionView({
                                                        collection: headerLinkCollection
                                                    }));
                                                }
                                            });
                                        }
                                    }
                                });
                            } else if (!isAuthenticatedRoute) {
                                var headerLinkCollection = that._getHeaderLinkCollection(isAuthenticatedRoute),
                                    headerLinkDesktopCollectionView = new App.Consumer.Views.HeaderLinkDesktopCollectionView({
                                        collection: headerLinkCollection
                                    });
                                headerView.mobileMenu = new App.Consumer.Views.HeaderMobileMenuView({
                                    collection: headerLinkCollection
                                });

                                if (!that.isErrorPage || that._errorType === constants.errorTypes.error404) {
                                    headerView.desktopLinks.show(headerLinkDesktopCollectionView);
                                }
                            }
                        });

                        layout.header.show(headerView);
                        var promise;
                        if (!pagePromiseDependencies || pagePromiseDependencies.length === 0) {
                            promise = P.resolve();
                        } else {
                            promise = App.salusConnector.getDataLoadPromise(pagePromiseDependencies);
                        }

                        promise.then(function() {
                            if (!layout.isDestroyed) {
                                if (that.isErrorPage) {
                                    that.currentPage = that._getErrorPage();
                                    layout.body.show(that.currentPage);
                                    if (that._errorType === constants.errorTypes.error404) {
                                        that._cleanErrorData();
                                    }
                                } else {
                                    that.currentPage = layoutView;
                                    layout.body.show(layoutView);
                                }
                            }
                        });
                        layout.footer.show(footerView);
                    });
                    App.mainRegion.show(layout);
            },
            showErrorPage: function(type, errData) {
                this._errorType = type;
                this._errorData = errData || {};

                if (!this.isErrorPage) {
                    this.isErrorPage = true;
                }
                this.showLayout(null, !this._errorData.isUnAuthenticated, []);
            },
            /**
             * on propertiesFailedSync,
             * @param device
             */
            showDeviceSyncError: function(device) {
                if (!App.modalRegion.currentView) {
                    App.modalRegion.show(new App.Consumer.Views.SalusAlertModalView({
                        model: new App.Consumer.Models.AlertModalViewModel({
                            iconClass: "icon-warning",
                            primaryLabelText: App.translate("equipment.error.syncError", {
                                device: device.get("name")
                            }),
                            secondaryLabelText: (function() {
                                var text, online = device.get("OnlineStatus_i");

                                if (online && !online.getProperty()) {
                                    text = App.translate("equipment.error.deviceOffline");
                                } else {
                                    text = App.translate("equipment.error.tryAgain");
                                }

                                return text;
                            }()),
                            leftButton: new App.Consumer.Views.ModalCloseButton({
                                id: "ok-close-btn",
                                classes: "width100 preserve-case",
                                buttonTextKey: "common.labels.ok"
                            })
                        })
                    }));

                    //App.showModal(); //Don't show the syncError Modal
                }
            },
            getPageAnalytics: function() {
                if (this.currentPage) {
                    if (this.currentPage.getAnalytics) {
                        return this.currentPage.getAnalytics();
                    } else {
                        App.warn("Page is missing analytics: " + window.location.toString());
                    }
                    this.currentPage = null;
                } else if (App && App.mainRegion && App.mainRegion.currentView && App.mainRegion.currentView.getAnalytics) {
                    return App.mainRegion.currentView.getAnalytics();
                } else {
                    return null;
                }
            },
            changeGatewayIfNeeded: function(dsn, defaultNavigatePath) {
                if (dsn && _.isString(dsn) && App.getCurrentGatewayDSN() !== dsn) {
                    var gateway = App.salusConnector.getDevice(dsn);

                    if (gateway) {
                        App.salusConnector.changeCurrentGateway(gateway);
                        return true;
                    }
                } else if (App.getCurrentGatewayDSN() === dsn) {
                    return true;
                }

                App.navigate(defaultNavigatePath, {
                    replace: true
                });
                return false;
            },
            _getErrorPage: function() {
                if (this._errorType === constants.errorTypes.REQUEST) {
                    return new App.Consumer.Views.SalusErrorPage({
                        model: new B.Model(this._errorData)
                    });
                } else {
                    return new App.Consumer.Views.Error404Page({
                        model: new B.Model(this._errorData)
                    });
                }
            },
            _cleanErrorData: function() {
                this.isErrorPage = false;
                this._errorData = null;
                this._errorType = null;
            }
        };
    });

    return App.Consumer.Controller;
});
