"use strict";

define([
	"app",
	"momentWrapper",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusRadio.view",
	"consumer/models/SalusRadioViewModel"
], function (App, moment, constants, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.Schedules.Views", function (Views, App, B, Mn) {

		Views.IntervalTypeRadioButtonsView = Mn.CompositeView.extend({
			className: "on-off-radio-buttons",
			template: consumerTemplates["schedules/intervalRadioCollection"],
			childView: App.Consumer.Views.RadioView,
			childViewContainer: ".bb-radio-container",
			_typeMap: {
				"onOff": [
					{
						labelTextKey: "schedules.schedule.newInterval.mode.on",
						value: "ON",
						name: "deviceState",
						isChecked: true,
						classes: "device-state-radio margin-l-10"
					},
					{
						labelTextKey: "schedules.schedule.newInterval.mode.off",
						value: "OFF",
						name: "deviceState",
						isChecked: false,
						classes: "device-state-radio margin-l-25"
					}
				],
				"thermostat": [
					{
						labelTextKey: "schedules.schedule.newInterval.mode.heat",
						value: "HEAT",
						name: "deviceState",
						isChecked: true,
						classes: "device-state-radio margin-l-10"
					},
					{
						labelTextKey: "schedules.schedule.newInterval.mode.cool",
						value: "COOL",
						name: "deviceState",
						isChecked: false,
						classes: "device-state-radio margin-l-25"
					},
					{
						labelTextKey: "schedules.schedule.newInterval.mode.off",
						value: "OFF",
						name: "deviceState",
						isChecked: false,
						classes: "device-state-radio margin-l-25"
					}
				]
			},

			initialize: function(options) {
				this.type = options.type;

				this.collection = new App.Consumer.Models.RadioCollection(this._typeMap[this.type]);
			},

			getSelectedValue: function () {
				return this.collection.findWhere({ isChecked: true }).get("value");
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Schedules.Views.IntervalTypeRadioButtonsView;
});

