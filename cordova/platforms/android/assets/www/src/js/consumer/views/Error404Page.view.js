"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage"
], function (App, consumerTemplates, SalusPage) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.Error404Page = Mn.LayoutView.extend({
			id: "error-404",
			className: "container margin-b-45",
			template: consumerTemplates["base/error404"]
		}).mixin([SalusPage], {
			analyticsSection: "settings",
			analyticsPage: "404page"
		});
	});

	return App.Consumer.Views.Error404Page;
});
