"use strict";

define([
	"app",
	"common/util/equipmentUtilities"
], function (App, equipUtils) {
	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $, _) {
		Mixins.BatteryLevel = function () {
			this.after("render", function () {

				this.addBinding(null, ".bb-battery-text", {
					observe: "BatteryVoltage_x10",
					onGet: function (voltage) {
						return voltage ? voltage.getProperty() / 10 : voltage;
					},
					update: function ($el, voltage) {
						var batteryObj = equipUtils.sensorBatteryMapperForVoltage(voltage, this.model.modelType);

						// toggle hidden if batteryObj couldn't map
						$el.toggleClass("invisible", !_.has(batteryObj, "percent"));

						$el.text(App.translate("equipment.status.battery.charged", {
							val: batteryObj.percent
						}));
					}
				});

				this.addBinding(null, ".bb-battery-icon", {
					observe: "BatteryVoltage_x10",
					onGet: function (voltage) {
						return voltage ? voltage.getProperty() / 10 : voltage;
					},
					update: function ($el, voltage) {
						var batteryObj = equipUtils.sensorBatteryMapperForVoltage(voltage, this.model.modelType);

						// toggle hidden if batteryObj couldn't map
						$el.toggleClass("invisible", !_.has(batteryObj, "status"));

						$el.addClass(batteryObj.status);
					}
				});
			});
		};
	});

	return App.Consumer.Views.Mixins.BatteryLevel;
});