"use strict";

define([
	"app",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/deviceTypes/mixin.BaseDevice"
], function (App, constants, AylaConfig, BaseAylaDevice) {

	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.DoorMonitor = B.Model.extend({
			tileType: "doorMonitor",
			modelType: constants.modelTypes.DOORMONITOR,
			defaults: {
				// get properties
				FirmwareVersion: null, // string, Firmware Version
				MACAddress: null, // string, IEEE MAC Address
				SubDeviceName_c: null, // string, Sub device name _c naming is as defined on device wiki
				DeviceType: null, // integer 1026: IAS Zone
				ManufactureName: null, // string, Manufacture Name
				ModelIdentifier: null, // string, Model ID
				PowerSource: null, // 1 AC 3 Battery
				MainsVoltage_x10: null, // not used
				BatteryVoltage_x10: null, // integer, Battery voltage, voltage = reading / 10
				ErrorPowerSLowBattery: null, // boolean error low battery
				MeasuredValue_x100: null, // not used
				ErrorIASZSAlarmed1: null, // alarm1 or opened
				ErrorIASZSAlarmed2: null, // alarm2 or opened
				ErrorIASZSTampered: null, // tampered
				ErrorIASZSLowBattery: null, // low battery
				ErrorIASZSTrouble: null, // trouble or failure
				ErrorIASZSACFault: null, // ac/mains fault

				// set properties
				SetIndicator: null // set device indicator with duration
			},

			initialize: function (/*data, options*/) {
				//TODO Setup
				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: 1026,
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_door.svg"),
					device_category_type: constants.categoryTypes.DOORMONITORS,
					device_category_name_key: "equipment.myEquipment.categories.types.doorMonitors"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_door_grey.svg")
				});
			}
		}).mixin([BaseAylaDevice]);
	});

	return App.Models.DoorMonitor;
});