Pod::Spec.new do |s|
  s.name         = "iOS_AylaLibrary"
  s.version      = "4.0.2"
  s.summary      = "iOS_AylaLibrary."
  s.homepage              = "https://github.com/AylaNetworks/iOS_AylaLibrary.git"
  s.license               = { :type => 'Commercial', :file => 'LICENSE' }
  s.author                = { "Company" => "Ayla Networks" }
  s.source                = { :git => "https://github.com/AylaNetworks/iOS_AylaLibrary.git", :tag => s.version.to_s  }
  s.platform              = :ios
  s.ios.deployment_target = '6.0'
  s.library		  = 'z'
  s.frameworks            = 'SystemConfiguration', 'MobileCoreServices', 'Security', 'UIKit'	
  s.source_files          = 'iOS_AylaLibrary/*.{h,m}', 'iOS_AylaLibrary/Ext/*'
  s.prefix_header_contents = "#import <SystemConfiguration/SystemConfiguration.h>\n#import <MobileCoreServices/MobileCoreServices.h>"
  s.requires_arc          = true
  s.dependency		 'AFNetworking', '~> 2.5'
  s.dependency		 'CocoaHTTPServer', '~> 2.3'
end