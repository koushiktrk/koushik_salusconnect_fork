//
//  AMLPropertyTestViewController.m
//  AML IOS
//
//  Created by Yipei Wang on 4/17/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import "AMLPropertyTestViewController.h"
#import "AylaNetworks.h"
#import "AylaTest.h"
@interface AMLPropertyTestViewController (){
    AylaDevice *curDevice;
    NSThread *testThread;
    dispatch_semaphore_t semaphore;
    
    dispatch_semaphore_t mainThread_semaphore;
    int totalLanModeLoops;
    int currentLoopNum ;
    BOOL threadWait;
    int totalFailureTimes;
    int totalTestTimes;
    int totalPassTimes;
    int reachabilityFailureTimes;
    BOOL newStart ;
    
    AylaProperty *property1;

    AylaProperty *boolProperty;
    AylaProperty *intProperty;
    AylaProperty *decimalProperty;
    AylaProperty *floatProperty;
    AylaProperty *strProperty;
    AylaProperty *streamProperty;
}

@end

@implementation AMLPropertyTestViewController
@synthesize boolInputTextField = _boolInputTextField;
@synthesize boolOutputTextField = _boolOutputTextField;
@synthesize boolHttpTextField = _boolHttpTextField;
@synthesize intInputTextField = _intInputTextField;
@synthesize intOutputTextField = _intOutputTextField;
@synthesize intHttpTextField = _intHttpTextField;
@synthesize decimalInputTextField = _decimalInputTextField;
@synthesize decimalOutputTextField = _decimalOutputTextField;
@synthesize decimalHttpTextField = _decimalHttpTextField;
@synthesize strInputTextField = _strInputTextField;
@synthesize strOutputTextField = _strOutputTextField;
@synthesize strHttpTextField = _strHttpTextField;
@synthesize streamInputTextField = _streamInputTextField;
@synthesize streamOutputTextField = _streamOutputTextField;
@synthesize streamHttpTextField = _streamHttpTextField;
@synthesize enableLanModeSwitch = _enableLanModeSwitch;
@synthesize statusField = _statusField;
@synthesize textView = _textView;
@synthesize boolButton = _boolButton;
@synthesize intButton = _intButton;
@synthesize decimalButton = _decimalButton;
@synthesize strButton = _strButton;
@synthesize streamButton = _streamButton;
@synthesize boolLabel = _boolLabel;
@synthesize intLabel = _intLabel;
@synthesize decimalLabel = _decimalLabel;
@synthesize strLabel = _strLabel;
@synthesize streamLabel = _streamLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    curDevice = nil;
    currentLoopNum = 0;
    totalTestTimes = 0;
    totalFailureTimes = 0;
    totalPassTimes = 0;
    newStart = true;
    reachabilityFailureTimes = 0;
    [AylaSystemUtils loadSavedSettings];
    [AylaSystemUtils lanModeState: ENABLED];
    [AylaLanMode enable];
    [AylaCache clearAll];
    
    
    [self start];

    _boolLabel.text = gblBooleanTypePropertyName1!=nil? gblBooleanTypePropertyName1:@"null";
    _intLabel.text = gblIntegerTypePropertyName1!=nil? gblIntegerTypePropertyName1:@"null";
    _decimalLabel.text = gblDecimalTypePropertyName1!=nil? gblDecimalTypePropertyName1:@"null";
    _strLabel.text = gblStringTypePropertyName1!=nil?gblStringTypePropertyName1:@"null";
    _streamLabel.text = gblStreamTypePropertyName1!=nil? gblStreamTypePropertyName1:@"null";

}

-(void) clean{

    boolProperty = nil;
    intProperty = nil;
    decimalProperty = nil;
    floatProperty = nil;
    strProperty = nil;
    streamProperty = nil;

}

-(void) start{
    
    [self clean];
    [self pushToScreen:@">>Start\n"];
    
    /**
     * user login
     */
    [AylaUser login:AML_TEST_USER_NAME password:AML_TEST_PASSWORD appId:AML_TEST_APP_ID appSecret:AML_TEST_APP_SECRET success:^(AylaResponse *resp, AylaUser *user){
        
        saveToLog(@"%@, %@, %@:%@, %@", @"P", @"LanMode", @"userLogin", @"success", @"start");
        [self pushToScreen:[NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"P", @"propertyTest", @"userLogin", @"success", @"start"]];
        
        //get devices first;
        totalPassTimes++;
        [AylaReachability determineServiceReachabilityWithBlock:^(int reachable) {
            
            saveToLog(@"%@, %@, %@:%@, %@", @"P", @"LanMode", @"userLogin", @"success", @"start");
            [self pushToScreen:[NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"I", @"propertyTest", @"serviceReachability", reachable == AML_REACHABILITY_REACHABLE?@"reachable":@"unreachable", @"start"]];
            
            /**
             * Get user registered devices
             */
            [AylaDevice getDevices:nil success:^(AylaResponse *resp, NSArray *devices)  {
                totalPassTimes++;
                curDevice = nil;
                
                saveToLog(@"%@, %@, %@:%d, %@", @"P", @"LanMode", @"devCount", [devices count], @"getDevices");
                [self pushToScreen:[NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"P", @"LanMode", @"devCount", [devices count], @"getDevices"]];
                
                /**
                 * Note this test only uses one device which matches @gblAmlProductName
                 */
                if([devices count] >= 1){
                    
                    for(AylaDevice *tmp in devices){
                        if([tmp.dsn isEqualToString:gblAmlProductName]){
                            curDevice = tmp;
                            break;
                        }
                    }
                    
                    if(curDevice == nil){
                        saveToLog(@"%@, %@, %@:%@, %@", @"E", @"LanMode", @"device", @"Couldn't find, Please check if gblAmlProductName is correct", @"getDevices - no device assigned");
                        
                        [self pushToScreen:[NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"E", @"LanMode", @"device", @"Couldn't find, Please check if gblAmlProductName is correct", @"getDevices - no device assigned"]];
                        [self abortMsg];
                        return;
                    }
                }
                else {
                    saveToLog(@"%@, %@, %@:%@, %@", @"E", @"LanMode", @"deviceCount", @"Please have at least 1 device to run test", @"Abort");
                    [self pushToScreen:[NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"E", @"LanMode", @"deviceCount", @"Please have at least 1 device to run test", @"Abort"]];
                    [self abortMsg];
                    return;
                }
                
                saveToLog(@"startLanModeEnableForDevice %@", curDevice.dsn);
                /**
                 * Lan Mode Support method. register your Rechabillity handler to library.
                 * msg format: {  "device" : 1,
                                  "connectivity" : 1
                                }
                 *      Possbile values: AML_REACHABLE             0
                                         AML_UNREACHABLE          -1
                                         AML_LAN_MODE_DISABLED    -2
                                         AML_REACHABILITY_UNKNOWN -3
                 */
                [AylaLanMode registerReachabilityHandle:^(NSDictionary *msg) {
                    NSNumber *deviceReachability = [msg valueForKey:@"device"];
                    NSNumber *serviceReachability = [msg valueForKey:@"connectivity"];
                    saveToLog(@"%@, %@, %@:%@, %@:%@, %@", @"I", @"LanMode", @"devReachability", deviceReachability, @"serviceReachability", serviceReachability, @"ReachabilityHandle");
                }];
                
                
                /**
                 * Lan Mode Support method. register your Notify handler to library.
                 * msg format: {  "statusCode" : 200,
                                }
                 * Possbile values: HTTP status code, 200 ~ 299 means Lan Mode works correctly with current LME device.
                                                                When any property value is updated, this handler would also be called. So please call method "getProperties"
                                                                to get latest property values when statusCode is returned as 200 ~ 299
                                                      400 ~ 499 error happened during Lan Mode communication, A polling thread is required if app still wants to keep tra
                 * Note: For testing, here a sperate testing thread would be called.
                 */
                [AylaLanMode registerNotifyHandle:^(NSDictionary *msg) {
                    int status =[[msg objectForKey:@"statusCode"] intValue];
                    
                    if(status >= 200 && status < 300  ){
                        // [self getDeviceProperties];
                        //dispatch_semaphore_signal(semaphore);
                        
                        if(newStart){
                            //run a thread for testing
                            testThread = [[NSThread alloc] initWithTarget:self selector:@selector(threadStart:) object:nil];
                            [testThread start];
                            newStart = false;
                        }
                        else{
                            /*
                             NSArray *props = [[NSArray alloc] initWithObjects:@"Blue_LED", @"Green_LED", @"Blue_button", nil];
                             NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:props, @"names", nil];
                             [curDevice getProperties:params
                                 success:^(NSArray *properties) {
                                     saveToLog(@"%@, %@, %@:%d, %@", @"I", @"LanMode", @"count", [properties count],  @"getProperties");
                                     
                                 } failure:^(aylaError *err) {
                                    saveToLog(@"%@, %@, %@:%d, %@", @"E", @"LanMode", @"errHttpStatusCode", err->httpStatusCode,  @"getProperties");
                                 
                                 }];
                             */
                        }
                    }
                    else{
                        [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"E", @"LanMode", @"Lan Mode Connection", @"failed",  @"notifyHandler"]];
                        
                        saveToLog(@"%@, %@, %@:%d, %@", @"E", @"LanMode", @"statusCode", status , @"notifer");
                    }
                    saveToLog(@"%@, %@, %@:%@, %@:%d, %@", @"I", @"LanMode", @"Notification", @"called", @"status", status, @"notifer");
                }];
                

                /**
                 * Enable Lan Mode communication to curDevice
                 */
                [curDevice lanModeEnable];
                
            } failure:^(AylaError *err) {
                totalFailureTimes++;
                saveToLog(@"%@, %@, %@:%d, %@:%d, %@", @"E", @"LanMode", @"errcode", err.errorCode , @"httpCode", err.httpStatusCode,  @"getDevices");
                [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@\n", @"E", @"LanMode", @"errcode", err.errorCode , @"httpCode", err.httpStatusCode,  @"getDevices"]];
            }];
            
            
        }];
        
    } failure:^(AylaError *err) {
        
        totalFailureTimes++;
        saveToLog(@"%@, %@, %@:%d, %@:%d, %@", @"E", @"LanMode", @"errcode", err.errorCode , @"httpCode", err.httpStatusCode,  @"userLogin");
        [self pushToScreen:[NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@\n", @"E", @"LanMode", @"errcode", err.errorCode , @"httpCode", err.httpStatusCode,  @"userLogin"]];
        [self abortMsg];
    }];
    
    
    
}

- (IBAction)boolTest:(id)sender {
    
    UITextField *tOutField = _boolOutputTextField;
    UITextField *tInField = _boolInputTextField;
    UITextField *tHttpField = _boolHttpTextField;
    NSNumber *value = [NSNumber numberWithBool:[tInField.text intValue]];
    AylaProperty *tProperty = boolProperty;
    
    tOutField.text = @"";
    tHttpField.text = @"";
    NSString *description = @"createDatapoint.boolean";
    if(tProperty!=nil){
        AylaDatapoint *dataPoint = [AylaDatapoint new];
        dataPoint.nValue = value;
        
        [AylaReachability determineDeviceReachabilityWithBlock:^(int reachable) {
            if(reachable !=AML_REACHABILITY_REACHABLE){
                NSLog(@"ERROR reachability %d", reachable);
            }
            
            [tProperty createDatapoint:dataPoint
                               success:^(AylaResponse *resp, AylaDatapoint *datapointCreated) {
                                   
                                   tOutField.text = [datapointCreated.nValue stringValue];
                                   tHttpField.text = @"200";
                                   
                                   saveToLog(@"%@, %@, %@:%@, %@", @"P", @"PropertyTest", @"datapoint", @"created",  @"createDatapoint_int");
                                   [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"P", @"LanMode", @"datapoint", @"created",  description]];
                               } failure:^(AylaError *err) {
                                   
                                   tOutField.text = @"null";
                                   tHttpField.text = [NSString stringWithFormat:@"%d/%d",err.httpStatusCode, err.errorCode];
                                   saveToLog(@"%@, %@, %@:%d, %@", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description);
                                   [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description]];
                                   
                               }];
            
        }];
    }
    else{
        tOutField.text = @"null";
        tHttpField.text = @"null";
        
        saveToLog(@"%@, %@, %@:%@, %@", @"F", @"PropertyTest", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned");
        [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"F", @"LanMode", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned"]];
    }

}

- (IBAction)intTest:(id)sender {
    
    UITextField *tOutField = _intOutputTextField;
    UITextField *tInField = _intInputTextField;
    UITextField *tHttpField = _intHttpTextField;
    NSNumber *value = [NSNumber numberWithInt:[tInField.text intValue]];
    AylaProperty *tProperty = intProperty;
    
    tOutField.text = @"";
    tHttpField.text = @"";
    NSString *description = @"createDatapoint.int";
    if(tProperty!=nil){
        AylaDatapoint *dataPoint = [AylaDatapoint new];
        dataPoint.nValue = value;
        
        [AylaReachability determineDeviceReachabilityWithBlock:^(int reachable) {
            if(reachable != AML_REACHABILITY_REACHABLE){
                NSLog(@"ERROR reachability %d", reachable);
            }
            
            [tProperty createDatapoint:dataPoint
                                  success:^(AylaResponse *resp, AylaDatapoint *datapointCreated) {
                                      
                                      tOutField.text = [datapointCreated.nValue stringValue];
                                      tHttpField.text = @"200";
                                      
                                      saveToLog(@"%@, %@, %@:%@, %@", @"P", @"PropertyTest", @"datapoint", @"created",  @"createDatapoint_int");
                                      [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"P", @"LanMode", @"datapoint", @"created",  description]];
                                  } failure:^(AylaError *err) {
                                      
                                      tOutField.text = @"null";
                                      tHttpField.text = [NSString stringWithFormat:@"%d/%d",err.httpStatusCode, err.errorCode];
                                      saveToLog(@"%@, %@, %@:%d, %@", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description);
                                      [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description]];
                                      
                                  }];
            
        }];
    }
    else{
        tOutField.text = @"null";
        tHttpField.text = @"null";
        
        saveToLog(@"%@, %@, %@:%@, %@", @"F", @"PropertyTest", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned");
        [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"F", @"LanMode", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned"]];
    }

    
    
}

- (IBAction)decimalTest:(id)sender {
    
    UITextField *tOutField = _decimalOutputTextField;
    UITextField *tInField = _decimalInputTextField;
    UITextField *tHttpField = _decimalHttpTextField;
    NSNumber *value = [NSNumber numberWithFloat:[tInField.text floatValue]];
    AylaProperty *tProperty = decimalProperty;
    NSString *description = @"createDatapoint.decimal";

    tOutField.text = @"";
    tHttpField.text = @"";
    if(tProperty!=nil){
        AylaDatapoint *dataPoint = [AylaDatapoint new];
        dataPoint.nValue = value;
        
        [AylaReachability determineDeviceReachabilityWithBlock:^(int reachable) {
            if(reachable !=AML_REACHABILITY_REACHABLE){
                NSLog(@"ERROR reachability %d", reachable);
            }
            
            [tProperty createDatapoint:dataPoint
                               success:^(AylaResponse *resp, AylaDatapoint *datapointCreated) {
                                   
                                   tOutField.text = [datapointCreated.nValue stringValue];
                                   tHttpField.text = @"200";
                                   
                                   saveToLog(@"%@, %@, %@:%@, %@", @"P", @"PropertyTest", @"datapoint", @"created",  @"createDatapoint_int");
                                   [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"P", @"LanMode", @"datapoint", @"created",  description]];
                               } failure:^(AylaError *err) {
                                   
                                   tOutField.text = @"null";
                                   tHttpField.text = [NSString stringWithFormat:@"%d/%d",err.httpStatusCode, err.errorCode];
                                   
                                   
                                   saveToLog(@"%@, %@, %@:%d, %@", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description);
                                   [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description]];
                                   
                               }];
            
        }];
    }
    else{
        tOutField.text = @"null";
        tHttpField.text = @"null";
        
        saveToLog(@"%@, %@, %@:%@, %@", @"F", @"PropertyTest", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned");
        [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"F", @"LanMode", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned"]];
    }

}

- (IBAction)stringTest:(id)sender {
    
    UITextField *tOutField = _strOutputTextField;
    UITextField *tInField = _strInputTextField;
    UITextField *tHttpField = _strHttpTextField;
    NSString *value = tInField.text;
    AylaProperty *tProperty = strProperty;
    NSString *description = @"createDatapoint.string";
    
    tOutField.text = @"";
    tHttpField.text = @"";
    if(tProperty!=nil){
        AylaDatapoint *dataPoint = [AylaDatapoint new];
        dataPoint.sValue = value;
        
        [AylaReachability determineDeviceReachabilityWithBlock:^(int reachable) {
            if(reachable !=AML_REACHABILITY_REACHABLE){
                NSLog(@"ERROR reachability %d", reachable);
            }
            
            [tProperty createDatapoint:dataPoint
                               success:^(AylaResponse *resp, AylaDatapoint *datapointCreated) {
                                   
                                   tOutField.text = datapointCreated.sValue;
                                   tHttpField.text = @"200";
                                   
                                   saveToLog(@"%@, %@, %@:%@, %@", @"P", @"PropertyTest", @"datapoint", @"created",  @"createDatapoint_int");
                                   [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"P", @"LanMode", @"datapoint", @"created",  description]];
                               } failure:^(AylaError *err) {
                                   
                                   tOutField.text = @"null";
                                   tHttpField.text = [NSString stringWithFormat:@"%d/%d",err.httpStatusCode, err.errorCode];
                                   saveToLog(@"%@, %@, %@:%d, %@", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description);
                                   [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description]];
                                   
                               }];
            
        }];
    }
    else{
        tOutField.text = @"null";
        tHttpField.text = @"null";
        
        saveToLog(@"%@, %@, %@:%@, %@", @"F", @"PropertyTest", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned");
        [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"F", @"LanMode", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned"]];
    }
}


- (IBAction)streamTest:(id)sender {
    
    UITextField *tOutField = _streamOutputTextField;
    UITextField *tInField = _streamInputTextField;
    UITextField *tHttpField = _streamHttpTextField;
    NSString *value = tInField.text;
    AylaProperty *tProperty = streamProperty;
    NSString *description = @"createDatapoint.stream";
    
    tOutField.text = @"";
    tHttpField.text = @"";
    if(tProperty!=nil){
        AylaDatapoint *dataPoint = [AylaDatapoint new];
        dataPoint.sValue = value;
        
        [AylaReachability determineDeviceReachabilityWithBlock:^(int reachable) {
            if(reachable !=AML_REACHABILITY_REACHABLE){
                NSLog(@"ERROR reachability %d", reachable);
            }
            
            [tProperty createDatapoint:dataPoint
                               success:^(AylaResponse *resp, AylaDatapoint *datapointCreated) {
                                   
                                   tOutField.text = datapointCreated.sValue ;
                                   tHttpField.text = @"200";
                                   
                                   saveToLog(@"%@, %@, %@:%@, %@", @"P", @"PropertyTest", @"datapoint", @"created",  @"createDatapoint_int");
                                   [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"P", @"LanMode", @"datapoint", @"created",  description]];
                               } failure:^(AylaError *err) {
                                   
                                   tOutField.text = @"null";
                                   tHttpField.text = [NSString stringWithFormat:@"%d/%d",err.httpStatusCode, err.errorCode];
                                   saveToLog(@"%@, %@, %@:%d, %@", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description);
                                   [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"F", @"PropertyTest", @"errHttpStatusCode", err.httpStatusCode,  description]];
                                   
                               }];
            
        }];
    }
    else{
        tOutField.text = @"null";
        tHttpField.text = @"null";
        
        saveToLog(@"%@, %@, %@:%@, %@", @"F", @"PropertyTest", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned");
        [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"F", @"LanMode", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned"]];
    }

    
    
}


-(void) threadStart:(id) params{
    static BOOL isReady = true;
    static int times = 0;
    __block AylaProperty *prop = nil;
    int threadTestLoops = 1;
    times  = 0;

    isReady = true;
    while(times< threadTestLoops){
        // if([[NSThread currentThread] isCancelled])
        //     break; //kick out the thread
        static int passNum = 0;
        sleep(2);
        
        if(isReady && times< threadTestLoops){
            isReady = false;
            NSArray *props = [[NSArray alloc] initWithObjects:@"Blue_LED", @"Green_LED", @"Blue_button", @"input", @"decimal_in", nil];
            // NSArray *props = [[NSArray alloc] initWithObjects:@"1", @"s", @"c", @"d", @"r", nil];
            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:props, @"names", nil];
            
            [curDevice getProperties:params
                             success:^(AylaResponse *resp, NSArray *properties) {
                                 totalPassTimes++;
                                 saveToLog(@"%@, %@, %@:%d, %@\n", @"P", @"LanMode", @"count", [properties count],  @"getProperties");
                                 [self pushToScreen:[NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"P", @"LanMode", @"count", [properties count],  @"getProperties"]];
                                 passNum++;
                                 prop = nil;
                                 times++;
                                 for(AylaProperty *p in properties){
                                     if([p.name isEqualToString:gblBooleanTypePropertyName1]){
                                         prop = p;
                                         boolProperty = p;
                                     }
                                     else if([p.name isEqualToString:gblStringTypePropertyName1]){
                                         strProperty = p;
                                     }
                                     else if([p.name isEqualToString:gblIntegerTypePropertyName1]){
                                         intProperty = p;
                                     }
                                     else if([p.name isEqualToString:gblDecimalTypePropertyName1]){
                                         decimalProperty = p;
                                     }
                                     else if([p.name isEqualToString:gblStreamTypePropertyName1]){
                                         streamProperty = p;
                                     }
                                     else if([p.name isEqualToString:gblFloatTypePropertyName1]){
                                         floatProperty = p;
                                     }
                                     
                                 }
                                 /*
                                 if(boolProperty!=nil){
                                     AylaDatapoint *dataPoint = [AylaDatapoint new];
                                     static int v1alue = 0;
                                     value = 1-value;
                                     dataPoint.nValue = [NSNumber numberWithBool:value];
                                     
                                     [AylaReachability determineDeviceReachabilityWithBlock:^(int reachable) {
                                         if(reachable !=AML_REACHABLE){
                                             NSLog(@"ERROR reachability %d", reachable);
                                         }
                                         
                                         [boolProperty createDatapoint:dataPoint success:^(AylaDatapoint *datapointCreated) {

                                             isReady = true;
                                             saveToLog(@"%@, %@, %@:%@, %@", @"P", @"LanMode", @"datapoint", @"created",  @"createDatapoint_boolean");
                                             [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"P", @"LanMode", @"datapoint", @"created",  @"createDatapoint.boolean"]];
                                         } failure:^(aylaError *err) {
                  
                                             isReady = true;
                                             saveToLog(@"%@, %@, %@:%d, %@", @"F", @"LanMode", @"errHttpStatusCode", err->httpStatusCode,  @"createDatapoint_boolean");
                                             [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"F", @"LanMode", @"errHttpStatusCode", err->httpStatusCode,  @"createDatapoint_boolean"]];
                                             
                                         }];
                                         
                                     }];
                                 }
                                 else{
                                     saveToLog(@"%@, %@, %@:%@, %@", @"F", @"LanMode", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned");
                                     [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"F", @"LanMode", gblBooleanTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned"]];
                                 }
                                 
                                 
                                 if(strProperty!=nil){
                                     
                                     AylaDatapoint *dataPoint2 = [AylaDatapoint new];
                                     static NSString *strValue = @"hihi";
                                     strValue = [NSString stringWithFormat:@"%@%@", strValue, @"X" ];
                                     dataPoint2.sValue = strValue;
                                     [strProperty createDatapoint:dataPoint2 success:^(AylaDatapoint *datapointCreated) {
                                         times++;
                                         saveToLog(@"%@, %@, %@:%@, %@", @"P", @"LanMode", @"datapoint", @"created",  @"createDatapoint.string");
                                         [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"P", @"LanMode", @"datapoint", @"created",  @"createDatapoint.string"]];
                                         
                                     } failure:^(aylaError *err) {
                                         if(err->errorCode == AML_USER_INVALID_PARAMETERS){
                                             NSDictionary *resp = (__bridge NSDictionary *)err->errorInfo;
                                             NSLog(@"err-> %@", resp);
                                         }
                                         times++;
                                         saveToLog(@"%@, %@, %@:%d, %@", @"F", @"LanMode", @"errHttpStatusCode", err->httpStatusCode,  @"createDatapoint");
                                         [self pushToScreen:[NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"F", @"LanMode", @"errHttpStatusCode", err->httpStatusCode,  @"createDatapoint.prop4"]];
                                     }];
                                     
                                 }
                                 else{
                                     saveToLog(@"%@, %@, %@:%@, %@", @"F", @"LanMode", gblStringTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned");
                                     [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"F", @"LanMode", gblStringTypePropertyName1, @"cannot find from property list",  @"getProperties - not assigned"]];
                                 }
                                 */
                                 
                               
                                 
                                 
                                 
                             } failure:^(AylaError *err) {
                                 saveToLog(@"%@, %@, %@:%d, %@", @"E", @"LanMode", @"errHttpStatusCode", err.httpStatusCode,  @"getProperties");
                                 times++;
                                 isReady = true;
                                 
                             }];
        }
    }
    
}


- (void) viewDidDisappear:(BOOL)animated{
    if(curDevice!=nil)
        [curDevice lanModeDisable];
    [AylaLanMode disable];
}



- (void) pushToLogAndScreen:(NSString *) msg{
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,msg]];
    saveToLog(@"%@", msg);
}


- (void) pushToScreen:(NSString *) msg{
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,msg]];
}

- (void) complete{
    
    gblTestsPassed += totalPassTimes;
    gblTestsFailed += totalFailureTimes;
    NSString *aMsg = [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@:%d\n", @"Completed", @"amlLanModeDeviceTest", @"TotalTestsRunned&Running", gblNumberOfTests, @"Passed", gblTestsPassed, @"Failed", gblTestsFailed, nil];
    saveToLog(@"%@, %@, %@:%d, %@:%d, %@:%d", @"Completed", @"amlLanModeDeviceTest", @"TotalTestsRunnedOrRunning", gblNumberOfTests, @"Passed", gblTestsPassed, @"Failed", gblTestsFailed);
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,aMsg]];
    _statusField.text = @"success";
    
    if(runAllInALoop){
        outputMsgs = [NSString stringWithFormat:@"%@\n%@", outputMsgs, _textView.text];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (void) abortMsg{
    
    gblTestsPassed += totalPassTimes;
    gblTestsFailed += totalFailureTimes;
    NSString *aMsg = [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@:%d, %@\n", @"F", @"amlLanModeDeviceTest", @"TotalTestsRunned&Running", gblNumberOfTests, @"Failed", gblTestsFailed, @"Passed", gblTestsPassed, @"Abort", nil];
    saveToLog(@"%@, %@, %@:%d, %@:%d, %@:%d, %@\n", @"F", @"amlLanModeDeviceTest", @"TotalTestsRunnedOrRunning", gblNumberOfTests, @"Failed", gblTestsFailed, @"Passed", gblTestsPassed, @"Abort");
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,aMsg]];
    _statusField.text= @"fail";
    if(runAllInALoop){
        outputMsgs = [NSString stringWithFormat:@"%@\n%@", outputMsgs, _textView.text];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setBoolInputTextField:nil];
    [self setBoolOutputTextField:nil];
    [self setBoolHttpTextField:nil];
    [self setIntInputTextField:nil];
    [self setIntOutputTextField:nil];
    [self setIntHttpTextField:nil];
    [self setDecimalInputTextField:nil];
    [self setDecimalOutputTextField:nil];
    [self setDecimalHttpTextField:nil];
    [self setStrInputTextField:nil];
    [self setStrOutputTextField:nil];
    [self setStrHttpTextField:nil];
    [self setStreamInputTextField:nil];
    [self setStreamOutputTextField:nil];
    [self setStreamHttpTextField:nil];
    [self setEnableLanModeSwitch:nil];
    [self setTextView:nil];
    [self setStatusField:nil];
    [self setBoolButton:nil];
    [self setIntButton:nil];
    [self setDecimalButton:nil];
    [self setStrButton:nil];
    [self setStreamButton:nil];
    [self setBoolLabel:nil];
    [self setIntLabel:nil];
    [self setDecimalLabel:nil];
    [self setStrLabel:nil];
    [self setStreamLabel:nil];
    [super viewDidUnload];
}
@end
