//
//  AMLDeviceTestViewController.h
//  AML IOS
//
//  Created by Yipei Wang on 1/31/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMLDeviceTestViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *statusField;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end
