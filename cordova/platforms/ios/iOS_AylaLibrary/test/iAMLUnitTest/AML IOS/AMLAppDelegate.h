//
//  AMLAppDelegate.h
//  AML IOS
//
//  Created by Daniel Myers on 7/15/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
