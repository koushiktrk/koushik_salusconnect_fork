//
//  NSObject+AylaNetworks.h
//  iOS_AylaLibrary
//
//  Created by Yipei Wang on 7/24/14.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

@interface NSObject (AylaNetworks)

- (id) nilIfNull;
- (BOOL) isNumber;

@end
