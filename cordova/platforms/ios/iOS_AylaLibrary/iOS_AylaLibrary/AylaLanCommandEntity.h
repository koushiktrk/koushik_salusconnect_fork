//
//  AylaLanCommandEntity.h
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 3/20/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AylaLanCommandEntity : NSObject
@property (nonatomic, copy) NSString *jsonString;
@property (nonatomic)       int cmdId;
@property (nonatomic)       enum {AYLA_LAN_COMMAND, AYLA_LAN_PROPERTY} baseType;

- (id)initWithParams: (int)cmdId jsonString:(NSString*)jsonString type:(int) baseType;

@end