//
//  AylaUser.m
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 6/28/12.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaNetworks.h"
#import "AylaApiClient.h"
#import "AylaSystemUtilsSupport.h"
#import "AylaOAuth.h"
#import "AylaErrorSupport.h"
#import "AylaReachabilitySupport.h"
@interface AylaUser ()
@property (nonatomic,readwrite) NSString *accessToken;
@property (nonatomic,readwrite) NSString *refreshToken;
@property (nonatomic,readwrite) NSUInteger expiresIn;
@property (nonatomic,readwrite) NSDate *updatedAt;
@end

NSString * const aylaOAuthAccountTypeGoogle = @"google_provider";
NSString * const aylaOAuthAccountTypeFacebook = @"facebook_provider";

NSString * const kAylaUserLogoutClearCache = @"clear_cache";

@implementation AylaUser

static AylaUser *_user = nil;

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self){
        self.firstName = [dict objectForKey:@"firstname"];
        self.lastName = [dict objectForKey:@"lastname"];
        self.email = [dict objectForKey:@"email"];
        self.country = [dict objectForKey:@"country"];
        self.termsAccepted = [dict objectForKey:@"terms_accepted"]
        &&[[dict objectForKey:@"terms_accepted"] boolValue]==YES? YES:NO;
    }
    return self;
}

+ (NSOperation *)login:(NSString *)userName password:(NSString *)password appId:(NSString *)appId appSecret:(NSString *)appSecret
      success:(void (^)(AylaResponse *response, AylaUser *user))successBlock
      failure:(void (^)(AylaError *err))failureBlock
{
  // params = {‘user’:{‘email’:‘user@aylanetworks.com’,‘password’:‘password’,‘application’:{‘app_id’:‘debwebserver_id’,‘app_secret’:‘debwebserver_secret’}}}
  NSDictionary *app_params =[NSDictionary dictionaryWithObjectsAndKeys:
                             appId, @"app_id", appSecret, @"app_secret", nil];
  NSDictionary *user_params =[NSDictionary dictionaryWithObjectsAndKeys:
                             app_params, @"application",
                             userName, @"email", password, @"password",
                             nil];
  NSDictionary *params =[NSDictionary dictionaryWithObjectsAndKeys:
                             user_params, @"user", nil];
  
  // Clean auth token
  gblAuthToken = nil;
    
  saveToLog(@"%@, %@, %@, %@", @"I", @"User", appId, @"userLogin.postPath attempt");
  return [[AylaApiClient sharedUserServiceInstance] postPath: @"users/sign_in.json" parameters:params
    success:^(AFHTTPRequestOperation *operation, id loginResponse) {
      saveToLog(@"%@, %@, %@, %@", @"I", @"User", @"none", @"userLogin.postPath success");
      
      //set reachability to be REACHABLE, after success reponse from cloud
      [AylaReachability setConnectivity:AML_REACHABILITY_REACHABLE];
      //auth_token = ([loginResponse valueForKeyPath:@"user.auth_token"] != [NSNull null]) ? [loginResponse valueForKeyPath:@"user.auth_token"] : @"";
      NSString *accessToken = [loginResponse valueForKeyPath:@"access_token"] ? [loginResponse valueForKeyPath:@"access_token"] : @"";
      if ([accessToken isEqualToString:@""]) {
        saveToLog(@"%@, %@, %@:%@, %@", @"E", @"User", @"auth_token", @"null", @"userLogin No authorization token from device service.");
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_NO_AUTH_TOKEN; err.nativeErrorInfo = nil; err.errorInfo = nil;
        failureBlock(err);
      } else {
        gblAuthToken = accessToken;
        AylaUser *user = [AylaUser new];
        user.accessToken = gblAuthToken;
        user.refreshToken = [loginResponse valueForKeyPath:@"refresh_token"] ? [loginResponse valueForKeyPath:@"refresh_token"] : nil;
        user.expiresIn = [loginResponse valueForKeyPath:@"expires_in"] ? [(NSNumber *)[loginResponse valueForKeyPath:@"expires_in"] unsignedIntegerValue] : 0;
        user.updatedAt = [NSDate date];
        _user = user;
        saveToLog(@"%@, %@, %@:%@, %@", @"I", @"User", @"gblAuthToken", @"retrieved", @"userLogin authorization header set");
        AylaResponse *response = [AylaResponse new];
        response.httpStatusCode = operation.response.statusCode;
        successBlock(response, user);
      }
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"User", @"NSError.code", error.code, @"userLogin", [AylaSystemUtils shortErrorFromError:error]);
      
        AylaError *err = [AylaError new];
      NSMutableDictionary *description;
      if(operation.responseString != NULL){
          err.errorCode = AML_USER_INVALID_PARAMETERS;
          
          NSError *jerr = nil;
          id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
          NSDictionary *resp = responseJSON;
        
          description = [[NSMutableDictionary alloc] initWithObjectsAndKeys: [resp valueForKeyPath:@"error"] , @"error", nil];
          err.errorInfo = description;
      }
      else{
          err.errorCode = 1;
          err.errorInfo = nil;
      }
      err.httpStatusCode = operation.response.statusCode; err.nativeErrorInfo = error;
      failureBlock(err);
    }
  ];  
}

+ (NSUInteger)accessTokenSecondsToExpiry
{
    if(gblAuthToken == nil || _user == nil)
        return 0;
    NSTimeInterval interval = [[_user.updatedAt dateByAddingTimeInterval:_user.expiresIn] timeIntervalSinceDate:[NSDate date]];
    return interval>0?interval:0;
}

+ (NSOperation *)refreshAccessTokenWithRefreshToken:(NSString *)refreshToken
        success:(void (^)(AylaResponse *response, AylaUser *refreshed))successBlock
        failure:(void (^)(AylaError *err))failureBlock
{
    if(refreshToken == nil){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS;
        err.nativeErrorInfo = nil; err.errorInfo = nil; err.httpStatusCode = 0;
        failureBlock(err);
        return nil;
    }
    
    NSDictionary *refresh = [NSDictionary dictionaryWithObjectsAndKeys:refreshToken, @"refresh_token", nil];
    return [[AylaApiClient sharedUserServiceInstance] postPath: @"users/refresh_token.json" parameters: [NSDictionary dictionaryWithObjectsAndKeys:refresh, @"user", nil]
            success:^(AFHTTPRequestOperation *operation, id  refreshResponse) {
                saveToLog(@"%@, %@, %@, %@", @"I", @"User", @"none", @"refreshToken.postPath success");
                
                //set reachability to be REACHABLE, after success reponse from cloud
                [AylaReachability setConnectivity:AML_REACHABILITY_REACHABLE];
                NSString *accessToken = [refreshResponse valueForKeyPath:@"access_token"] ? [refreshResponse valueForKeyPath:@"access_token"] : @"";
                if ([accessToken isEqualToString:@""]) {
                    saveToLog(@"%@, %@, %@:%@, %@", @"E", @"User", @"auth_token", @"null", @"refreshAccessWithRefreshToken: No authorization token from device service.");
                    AylaError *err = [AylaError new]; err.errorCode = AML_USER_NO_AUTH_TOKEN; err.nativeErrorInfo = nil; err.errorInfo = nil;
                    failureBlock(err);
                } else {
                    gblAuthToken = accessToken;
                    AylaUser *user = [AylaUser new];
                    user.accessToken = gblAuthToken;
                    user.refreshToken = ([refreshResponse valueForKeyPath:@"refresh_token"] != [NSNull null]) ? [refreshResponse valueForKeyPath:@"refresh_token"] : nil;
                    user.expiresIn = ([refreshResponse valueForKeyPath:@"expires_in"] != [NSNull null]) ? [(NSNumber *)[refreshResponse valueForKeyPath:@"expires_in"] unsignedIntegerValue] : 0;
                    user.updatedAt = [NSDate date];
                    _user = user;
                    saveToLog(@"%@, %@, %@:%@, %@", @"I", @"User", @"gblAuthToken", @"retrieved", @"userLogin authorization header set");
                    AylaResponse *response = [AylaResponse new];
                    response.httpStatusCode = operation.response.statusCode;
                    successBlock(response, user);
                }
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"E", @"User",
                          @"statusCode", operation.response.statusCode, @"responce", operation.responseString, @"refreshToken.postPath");
                
                AylaError *err = [AylaError new]; err.httpStatusCode = operation.response.statusCode;
                err.nativeErrorInfo = error;
                NSMutableDictionary *errList;
                if(operation.responseString != NULL){
                    err.errorCode = AML_USER_INVALID_PARAMETERS;
                    NSError *jerr = nil;
                    id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                    err.errorInfo = responseJSON;
                    err.nativeErrorInfo = nil;
                    err.errorInfo = errList;
                }
                else{
                    err.errorCode = 1;
                    err.errorInfo = nil;
                }
                failureBlock(err);
            }
     ];

}



+ (NSOperation *)signUp: (NSDictionary *)callParams appId:(NSString *)appId appSecret:(NSString *)appSecret
           success:(void (^)(AylaResponse *response))successBlock
           failure:(void (^)(AylaError *err))failureBlock
{
    NSDictionary *app_params =[NSDictionary dictionaryWithObjectsAndKeys:
                               appId, @"app_id", appSecret, @"app_secret", nil];
    
    NSMutableDictionary *nparams = [callParams mutableCopy];
    [nparams setObject:app_params forKey:@"application"];
    
    NSMutableDictionary *errors = [[NSMutableDictionary alloc] init];
    if([nparams objectForKey:@"email"] == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"email"];
    }
    else{
        NSString *mail = [nparams objectForKey:@"email"];
        NSError *error = nil;
        static NSString *mailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:mailRegex
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:mail
                                                            options:0
                                                            range:NSMakeRange(0, [mail length])];
        if(numberOfMatches == 0){
            [errors setObject:@"is invalid" forKey:@"email"];
        }
    }
    
    if([nparams objectForKey:@"password"] == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"password"];
    }
    else{
        if([[nparams objectForKey:@"password"] length] < 2)
            [errors setObject:@"is invalid" forKey:@"password"];
    }

    if([nparams objectForKey:@"firstname"] == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"firstname"];
    }
    else{
        if([[nparams objectForKey:@"firstname"] length] < 2)
            [errors setObject:@"is invalid" forKey:@"firstname"];
    }

    if([nparams objectForKey:@"lastname"] == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"lastname"];
    }
    else{
        if([[nparams objectForKey:@"lastname"] length] < 2)
            [errors setObject:@"is invalid" forKey:@"lastname"];
    }
/*
    if([nparams objectForKey:@"company"] == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"company"];
    }
    else{
        if([[nparams objectForKey:@"company"] length] < 2)
            [errors setObject:@"is invalid" forKey:@"company"];
    }
*/
    if([nparams objectForKey:@"country"] == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"country"];
    }
    else{
        if([[nparams objectForKey:@"country"] length] < 2)
            [errors setObject:@"is invalid" forKey:@"country"];
    }
    
    
    NSDictionary *possibleParams = [[NSDictionary alloc] initWithObjectsAndKeys:@"1",@"email", @"1", @"password",
                                                                                @"1", @"firstname",@"1", @"lastname", @"1", @"country",
                                                                                @"1", @"phone",@"1", @"company",@"1", @"city", @"1", @"state",
                                                                                @"1", @"street",@"1", @"zip",@"1", @"ayla_dev_kit_num",
                                                                                @"1", @"phone_country_code",
                                                                                @"1", AML_EMAIL_TEMPLATE_ID,
                                                                                @"1", AML_EMAIL_SUBJECT,
                                                                                @"1", AML_EMAIL_BODY_HTML,
                                                                                nil];
    for(NSString *key in [callParams allKeys]){
        if(  [possibleParams valueForKey:key ] ==nil ){ // not supported
            [errors setObject:@"is not supported" forKey:key];
        }
    }
    
    if([errors count]>0){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS;
        err.errorInfo = errors;
        err.nativeErrorInfo = nil; err.httpStatusCode = 422;
        failureBlock(err);
        return nil;
    }
    
    NSString *path = @"users.json";
    if([nparams objectForKey:AML_EMAIL_TEMPLATE_ID]) {
        NSString *emailTemplateIdString = [@"?email_template_id=" stringByAppendingString:[nparams objectForKey:AML_EMAIL_TEMPLATE_ID]];
        path = [path stringByAppendingString:emailTemplateIdString];
        if([nparams objectForKey:AML_EMAIL_SUBJECT]) {
            NSString *utf8Encoding = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                               (__bridge CFStringRef)[nparams objectForKey:AML_EMAIL_SUBJECT],
                                                                                               NULL,
                                                                                               CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                               kCFStringEncodingUTF8));
            path = [path stringByAppendingFormat:@"&email_subject=%@",utf8Encoding];
        }
        if([nparams objectForKey:AML_EMAIL_BODY_HTML]) {
            NSString *utf8Encoding = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                               (__bridge CFStringRef)[nparams objectForKey:AML_EMAIL_BODY_HTML],
                                                                                               NULL,
                                                                                               CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                               kCFStringEncodingUTF8));
            path = [path stringByAppendingFormat:@"&email_body_html=%@",utf8Encoding];
        }

    }
    
    NSDictionary *userParams =[NSDictionary dictionaryWithObjectsAndKeys:nparams, @"user", nil];
    NSString *uname = [callParams objectForKey:@"email"];
    saveToLog(@"%@, %@, %@:%@, %@:%@, %@", @"I", @"User",@"username", uname, @"appId", appId, @"userSignUp.postPath attempt");
    
    return [[AylaApiClient sharedUserServiceInstance] postPath:path parameters:userParams
                 success:^(AFHTTPRequestOperation *operation, id signUpResponse) {
                     saveToLog(@"%@, %@, %@, %@", @"I", @"User", @"none", @"userSignUp.postPath success");
                     
                     NSString *email = ([signUpResponse valueForKeyPath:@"email"] != [NSNull null]) ? [signUpResponse valueForKeyPath:@"email"] : @"";
                     if ([email isEqualToString:@""]) {
                         saveToLog(@"%@, %@, %@:%@, %@", @"E", @"User", @"SignUp email", @"null", @"userSignUp");
                         NSString *errStr = @"Cannot get right responce from server";
                         AylaError *err = [AylaError new]; err.errorCode = 1; err.httpStatusCode = operation.response.statusCode; err.nativeErrorInfo = errStr;
                         failureBlock(err);
                     } else {
                         saveToLog(@"%@, %@, %@:%@", @"I", @"User", @"mail", email, @"userSignUp successfully");
                         AylaResponse *response = [AylaResponse new];
                         response.httpStatusCode = operation.response.statusCode;
                         successBlock(response);
                     }
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     
                     saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"E", @"User",
                               @"statusCode", operation.response.statusCode, @"responce", operation.responseString, @"userSignUp");
                     
                     AylaError *err = [AylaError new]; err.httpStatusCode = operation.response.statusCode;
                     err.nativeErrorInfo = error;
                     NSMutableDictionary *errList;
                     if(operation.responseString != NULL){
                         err.errorCode = AML_USER_INVALID_PARAMETERS;
                         NSError *jerr = nil;
                         id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                         NSDictionary *resp = responseJSON;
                         NSDictionary *errors = [resp objectForKey:@"errors"];
                         NSArray *arr;
                         errList = [[NSMutableDictionary alloc] init];
                     
                         for(NSString* key in errors){
                             arr = [errors objectForKey:key];
                             [errList setObject:[arr objectAtIndex:0] forKey:key];
                         }
                         err.nativeErrorInfo = nil;
                         err.errorInfo = errList;
                     }
                     else{
                         err.errorCode = 1;
                         err.errorInfo = nil;
                     }
                     failureBlock(err);
                 }
     ];
}

+ (NSOperation *)signUpConfirmationWithToken:(NSString *)token
                            success:(void (^)(AylaResponse *response, AylaUser *user))successBlock
                            failure:(void (^)(AylaError *err))failureBlock
{
    if(!token || [token isEqualToString:@""]){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS;
        err.errorInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"is invalid", @"token", nil];
        err.nativeErrorInfo = nil; err.httpStatusCode = 422;
        failureBlock(err);
        return nil;
    }
    
    NSString *path = [NSString stringWithFormat:@"users/confirmation.json?confirmation_token=%@", token];
    return [[AylaApiClient sharedUserServiceInstance] putPath: path parameters:nil
                                                success:^(AFHTTPRequestOperation *operation, id response) {
                                                    saveToLog(@"%@, %@, %@:%d, %@:%@, %@", @"I", @"User",
                                                              @"statusCode", operation.response.statusCode, @"success",@"null", @"signUpConfirmationWithToken");
                                                    AylaUser *user = response?[[AylaUser alloc] initWithDictionary:response]:nil;
                                                    AylaResponse *resp = [AylaResponse new];
                                                    resp.httpStatusCode = operation.response.statusCode;
                                                    successBlock(resp, user);
                                                }
                                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                    saveToLog(@"%@, %@, %@:%d, %@:%@, %@", @"E", @"User",
                                                              @"httpStatusCode", operation.response.statusCode, @"responce", operation.responseString, @"signUpConfirmationWithToken");
                                                    AylaError *err = [AylaError new]; err.httpStatusCode = operation.response.statusCode;
                                                    err.nativeErrorInfo = error;
                                                    if(operation.responseString != NULL){
                                                        err.errorCode = AML_USER_INVALID_PARAMETERS;
                                                        NSError *jerr = nil;
                                                        id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                                                        NSDictionary *resp = [responseJSON objectForKey:@"errors"];
                                                        err.errorInfo = resp;
                                                    }
                                                    else{
                                                        err.errorCode = 1;
                                                        err.errorInfo = nil;
                                                    }
                                                    failureBlock(err);
                                                }
     ];

}

+ (NSOperation *)getInfo:(NSDictionary *)callParams
            success:(void (^)(AylaResponse *response, NSDictionary *dict))successBlock
            failure:(void (^)(AylaError *err))failureBlock
{
    NSString *token = gblAuthToken;
    if(token == nil){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_NO_AUTH_TOKEN;
        err.nativeErrorInfo = nil; err.errorInfo = nil; err.httpStatusCode = 0;
        failureBlock(err);
        return nil;
    }
    return [[AylaApiClient sharedUserServiceInstance] getPath: [NSString stringWithFormat:@"%@%@",@"users/get_user_profile.json?access_token=",gblAuthToken ] parameters:nil
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   saveToLog(@"%@, %@, %@, %@", @"I", @"User", @"none", @"usergetInfo.getPath");
                   NSDictionary *resp = responseObject;
                   AylaResponse *aResp = [AylaResponse new];
                   aResp.httpStatusCode = operation.response.statusCode;
                   successBlock(aResp, resp);
               }
               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"User", @"NSError.code", error.code, @"usergetInfo.getPath", [AylaSystemUtils shortErrorFromError:error]);
                   AylaError *err = [AylaError new];
                   err.httpStatusCode = operation.response.statusCode;
                   err.errorCode = AML_ERROR_FAIL;
                   err.nativeErrorInfo = error;
                   err.errorInfo = nil;
                   
                   failureBlock(err);
               }
     ];
}

+ (NSOperation *)changePassword:(NSString *)currentPassword newPassword:(NSString *)newPassword
                    success:(void (^)(AylaResponse *response))successBlock
                    failure:(void (^)(AylaError *err))failureBlock
{
    NSString *token = gblAuthToken;
    if(token == nil){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_NO_AUTH_TOKEN;
        err.nativeErrorInfo = nil; err.errorInfo = nil; err.httpStatusCode = 0;
        failureBlock(err);
        return nil;
    }
    
    NSDictionary *pwdInfo = [[NSDictionary alloc] initWithObjectsAndKeys:currentPassword, @"current_password", newPassword, @"password", nil];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:pwdInfo, @"user", nil];
    return [[AylaApiClient sharedUserServiceInstance] putPath: @"users.json" parameters:params
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                saveToLog(@"%@, %@, %@, %@", @"I", @"User", @"none", @"userPasswordEdit.putPath");
                AylaResponse *response = [AylaResponse new];
                response.httpStatusCode = operation.response.statusCode;
                successBlock(response);
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"User", @"NSError.code", error.code, @"userPasswordEdit.putPath", [AylaSystemUtils shortErrorFromError:error]);
                AylaError *err = [AylaError new];
                err.httpStatusCode = operation.response.statusCode;
                NSMutableDictionary *errList;
                if(operation.responseString != NULL){
                    err.errorCode = AML_USER_INVALID_PARAMETERS;
                    NSError *jerr = nil;
                    id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                    NSDictionary *resp = responseJSON;
                    NSDictionary *errors = [resp objectForKey:@"errors"];
                    NSArray *arr;
                    errList = [[NSMutableDictionary alloc] init];
                    
                    for(NSString* key in errors){
                        arr = [errors objectForKey:key];
                        [errList setObject:[arr objectAtIndex:0] forKey:key];
                    }
                    err.nativeErrorInfo = nil;
                    err.errorInfo = errList;
                }
                else{
                    err.errorCode = 1;
                    err.nativeErrorInfo = error;
                    err.errorInfo = nil;
                }
                failureBlock(err);
            }
     ];
}


+ (NSOperation *)updateUserInfo:(NSDictionary *)callParams
                  success:(void (^)(AylaResponse *response))successBlock
                  failure:(void (^)(AylaError *err))failureBlock
{
    NSString *token = gblAuthToken;
    if(token == nil){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_NO_AUTH_TOKEN;
        err.nativeErrorInfo = nil; err.errorInfo = nil; err.httpStatusCode = 0;
        failureBlock(err);
        return nil;
    }
    
    NSDictionary *possibleParams = [[NSDictionary alloc] initWithObjectsAndKeys:@"1", @"firstname",@"1", @"lastname", @"1", @"country",
                                                                                @"1", @"phone",@"1", @"company", @"1", @"city", @"1", @"state",
                                                                                @"1", @"street",@"1", @"zip",@"1", @"ayla_dev_kit_num",
                                                                                @"1", @"phone_country_code", nil];
    NSMutableDictionary *errors = [NSMutableDictionary new];
    for(NSString *key in [callParams allKeys]){
        if(  [possibleParams valueForKey:key ] ==nil ){ // not supported
            [errors setObject:@"is not supported" forKey:key];
        }
    }
    if([errors count]>0){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS;
        err.errorInfo = errors;
        err.nativeErrorInfo = nil; err.httpStatusCode = 422;
        failureBlock(err);
        return nil;
    }
    
    NSDictionary *userParams = [[NSDictionary alloc] initWithObjectsAndKeys:callParams, @"user", nil];
    return [[AylaApiClient sharedUserServiceInstance] putPath: @"users.json" parameters:userParams
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               saveToLog(@"%@, %@, %@, %@", @"I", @"User", @"none", @"userUpdateUserInfo");
               AylaResponse *response = [AylaResponse new];
               response.httpStatusCode = operation.response.statusCode;
               successBlock(response);
           }
           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"User", @"NSError.code", error.code, @"userAttributesUpdate", [AylaSystemUtils shortErrorFromError:error]);
               AylaError *err = [AylaError new];
               err.httpStatusCode = operation.response.statusCode;
               NSMutableDictionary *errList;
               if(operation.responseString != NULL){
                   err.errorCode = AML_USER_INVALID_PARAMETERS;
                   NSError *jerr = nil;
                   id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                   NSDictionary *resp = responseJSON;
                   NSDictionary *errors = [resp objectForKey:@"errors"];                   
                   NSArray *arr;
                   errList = [[NSMutableDictionary alloc] init];
                   for(NSString* key in errors){
                       arr = [errors objectForKey:key];
                       [errList setObject:[arr objectAtIndex:0] forKey:key];
                   }
                   err.nativeErrorInfo = nil;
                   err.errorInfo = errList;
               }
               else {
                   err.errorCode = 1;
                   err.nativeErrorInfo = error;
                   err.errorInfo = nil;
               }
               failureBlock(err);
           }
     ];
}

+ (NSOperation *)logout:(__unused NSString *)accessToken
                success:(void (^)(AylaResponse *response))successBlock
                failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaUser logoutWithParams:@{kAylaUserLogoutClearCache: @(YES)} success:successBlock failure:failureBlock];
}

+ (NSOperation *)logoutWithParams:(NSDictionary *)callParams
                  success:(void (^)(AylaResponse *response))successBlock
                  failure:(void (^)(AylaError *err))failureBlock
{
    NSString *token = gblAuthToken;
    
	// '{"user": {"access_token": "e0b062246574a6de2980687857f28240"}}'
    NSDictionary *user_params =[NSDictionary dictionaryWithObjectsAndKeys:
                             token, @"access_token", nil];
    NSDictionary *params =[NSDictionary dictionaryWithObjectsAndKeys:
                         user_params, @"user", nil];
  
    NSNumber *clearCache = callParams[kAylaUserLogoutClearCache]?: @(NO);
    NSMutableURLRequest *request = [[AylaApiClient sharedUserServiceInstance] requestWithMethod:@"POST" path:@"users/sign_out.json" parameters:params];
    [request setTimeoutInterval:20];
    AFHTTPRequestOperation *operation = [[AylaApiClient sharedUserServiceInstance] HTTPRequestOperationWithRequest:request
                                                 success:^(AFHTTPRequestOperation *operation, id responseObject){
                                                     saveToLog(@"%@, %@, %@, %@", @"I", @"User", @"none", @"userLogout.getPath");
                                                     gblAuthToken = nil;
                                                     _user = nil;
                                                     
                                                     if(clearCache.boolValue) [AylaCache clearAll];
                                                     
                                                     AylaResponse *response = [AylaResponse new];
                                                     response.httpStatusCode = operation.response.statusCode;
                                                     successBlock(response);
                                                 }
                                                 failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                                     gblAuthToken = nil;
                                                     _user = nil;
                                                     saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"User", @"NSError.code", error.code, @"userLogout.getPath", [AylaSystemUtils shortErrorFromError:error]);
                                                     AylaError *err = [AylaError new]; err.errorCode = 1; err.nativeErrorInfo = error;
                                                     failureBlock(err);
                                                 }
                                         ];
    [[AylaApiClient sharedUserServiceInstance] enqueueHTTPRequestOperation:operation];
    return operation;
}

+ (NSOperation *)resetPassword:(NSString *)mailAddress
                    withParams:(NSDictionary *)params
                       success:(void (^)(AylaResponse *response))successBlock
                       failure:(void (^)(AylaError *err))failureBlock
{
   return [AylaUser resetPassword:mailAddress appId:nil appSecret:nil andParams:params success:successBlock failure:failureBlock];
}

+ (NSOperation *)resetPassword:(NSString *)mailAddress
                         appId:(NSString *)appId
                     appSecret:(NSString *)appSecret
                     andParams:(NSDictionary *)params
                  success:(void (^)(AylaResponse *response))successBlock
                  failure:(void (^)(AylaError *err))failureBlock
{
    NSMutableDictionary *errors = [[NSMutableDictionary alloc] init];
    if(mailAddress == nil)
    {
        [errors setObject:@"Email can't be blank." forKey:@"base"];
    }
    else{
        NSError *error = nil;
        static NSString *mailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:mailRegex
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:mailAddress
                                                            options:0
                                                              range:NSMakeRange(0, [mailAddress length])];
        if(numberOfMatches == 0){
            [errors setObject:@"Email is invalid." forKey:@"base"];
        }
    }
    
    NSDictionary *applicationDictionary = nil;
    if(appId && appSecret)
    {
        applicationDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   appId, @"app_id",
                                                   appSecret, @"app_secret", nil];
    }
    else {
        saveToLog(@"%@, %@, %@:%@, %@", @"W", @"User",
              @"app_id||app_secret", @"can't be blank", @"userResetPassword");
    }

    if([errors count] != 0){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS;
        err.errorInfo = errors;
        err.nativeErrorInfo = nil; err.httpStatusCode = 0;
        failureBlock(err);
        return nil;
    }
    
    NSMutableDictionary *infoDictionary =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:mailAddress, @"email", nil];
    
    if(applicationDictionary) {
        [infoDictionary setObject:applicationDictionary forKey:@"application"];
    }
    
    NSString *path = @"users/password.json";
    if(params) {
        if([params objectForKey:AML_EMAIL_TEMPLATE_ID]) {
            path = [path stringByAppendingFormat:@"?email_template_id=%@",
                    [params objectForKey:AML_EMAIL_TEMPLATE_ID]];
            if([params objectForKey:AML_EMAIL_SUBJECT]) {
                NSString *utf8Encoding = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                                   (__bridge CFStringRef)[params objectForKey:AML_EMAIL_SUBJECT],
                                                                                                   NULL,
                                                                                                   CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                                   kCFStringEncodingUTF8));
                path = [path stringByAppendingFormat:@"&email_subject=%@",utf8Encoding];
            }
            if([params objectForKey:AML_EMAIL_BODY_HTML]) {
                NSString *utf8Encoding = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                                   (__bridge CFStringRef)[params objectForKey:AML_EMAIL_BODY_HTML],
                                                                                                   NULL,
                                                                                                   CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                                   kCFStringEncodingUTF8));
                path = [path stringByAppendingFormat:@"&email_body_html=%@",utf8Encoding];
            }
        }
    }
    NSDictionary *userParams = [[NSDictionary alloc] initWithObjectsAndKeys:infoDictionary, @"user", nil];
    return [[AylaApiClient sharedUserServiceInstance] postPath:path parameters:userParams
                success:^(AFHTTPRequestOperation *operation, id response) {
                    saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"I", @"User",
                              @"statusCode", operation.response.statusCode, @"success",@"null", @"userResetPassword");
                    AylaResponse *resp = [AylaResponse new];
                    resp.httpStatusCode = operation.response.statusCode;
                    successBlock(resp);
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"E", @"User",
                              @"httpStatusCode", operation.response.statusCode, @"responce", operation.responseString, @"userResetPassword");
                    AylaError *err = [AylaError new]; err.httpStatusCode = operation.response.statusCode;
                    err.nativeErrorInfo = error;
                    NSMutableDictionary *errList;
                    if(operation.responseString != NULL){
                        err.errorCode = AML_USER_INVALID_PARAMETERS;
                        NSError *jerr = nil;
                        id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                        NSDictionary *resp = responseJSON;
                        NSDictionary *errors = [resp objectForKey:@"errors"];
                        NSArray *arr;
                        errList = [[NSMutableDictionary alloc] init];
                        
                        for(NSString* key in errors){
                            arr = [errors objectForKey:key];
                            [errList setObject:[arr objectAtIndex:0] forKey:key];
                        }
                        err.nativeErrorInfo = nil;
                        err.errorInfo = errList;
                    }
                    else{
                        err.errorCode = 1;
                        err.errorInfo = nil;
                    }
                    failureBlock(err);
                }
     ];
}

+ (NSOperation *)resetPasswordWithNewPassword:(NSString *)password
                                     andToken:(NSString *)token
                                      success:(void (^)(AylaResponse *response))successBlock
                                      failure:(void (^)(AylaError *err))failureBlock
{
    NSMutableDictionary *errors = [[NSMutableDictionary alloc] init];
    if(!password) {
        [errors setObject:@"can't be blank." forKey:@"password"];
    }
    if(!token) {
        [errors setObject:@"can't be blank." forKey:@"token"];
    }
    if([errors count] != 0){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS;
        err.errorInfo = errors;
        err.nativeErrorInfo = nil; err.httpStatusCode = 0;
        failureBlock(err);
        return nil;
    }
    NSDictionary *pwdInfo = [[NSDictionary alloc] initWithObjectsAndKeys: password, @"password",
                                                                          password, @"password_confirmation",
                                                                          token, @"reset_password_token",
                                                                          nil];
    NSDictionary *userParams = [[NSDictionary alloc] initWithObjectsAndKeys: pwdInfo, @"user", nil];
    return [[AylaApiClient sharedUserServiceInstance] putPath:@"users/password.json" parameters:userParams
               success:^(AFHTTPRequestOperation *operation, id response) {
                   saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"I", @"User",
                             @"statusCode", operation.response.statusCode, @"success",@"null", @"userResetPasswordWithToken");
                   AylaResponse *resp = [AylaResponse new];
                   resp.httpStatusCode = operation.response.statusCode;
                   successBlock(resp);
               }
               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   
                   saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"E", @"User",
                             @"httpStatusCode", operation.response.statusCode, @"responce", operation.responseString, @"userResetPasswordWithToken");
                   AylaError *err = [AylaError new]; err.httpStatusCode = operation.response.statusCode;
                   err.nativeErrorInfo = error;
                   NSMutableDictionary *errList;
                   if(operation.responseString != NULL){
                       err.errorCode = AML_USER_INVALID_PARAMETERS;
                       NSError *jerr = nil;
                       id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                       NSDictionary *resp = responseJSON;
                       NSDictionary *errors = [resp objectForKey:@"errors"];
                       NSArray *arr;
                       errList = [[NSMutableDictionary alloc] init];
                       
                       for(NSString* key in errors){
                           arr = [errors objectForKey:key];
                           [errList setObject:[arr objectAtIndex:0] forKey:key];
                       }
                       err.errorInfo = errList;
                   }
                   else{
                       err.errorCode = 1;
                       err.errorInfo = nil;
                   }
                   failureBlock(err);
               }
        ];
}

+ (NSOperation *)resendConfirmation:(NSString *)mailAddress
                          withParams:(NSDictionary *)params
                            success:(void (^)(AylaResponse *response))successBlock
                            failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaUser resendConfirmation:mailAddress appId:nil appSecret:nil andParams:params success:successBlock failure:failureBlock];
}

+ (NSOperation *)resendConfirmation:(NSString *)mailAddress
                              appId:(NSString *)appId
                          appSecret:(NSString *)appSecret
                          andParams:(NSDictionary *)params
                  success:(void (^)(AylaResponse *response))successBlock
                  failure:(void (^)(AylaError *err))failureBlock
{
    NSMutableDictionary *errors = [[NSMutableDictionary alloc] init];
    if(mailAddress == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"email"];
    }
    else{
        NSError *error = nil;
        static NSString *mailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:mailRegex
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:mailAddress
                                                            options:0
                                                              range:NSMakeRange(0, [mailAddress length])];
        if(numberOfMatches == 0){
            [errors setObject:@"is invalid" forKey:@"email"];
        }
    }
    
    NSDictionary *applicationDictionary = nil;
    if(appId && appSecret)
    {
        applicationDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                 appId, @"app_id",
                                 appSecret, @"app_secret", nil];
    }
    else {
        saveToLog(@"%@, %@, %@:%@, %@", @"W", @"User",
                  @"app_id||app_secret", @"can't be blank", @"userResendConfirmationToken");
    }

    if([errors count] != 0){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS;
        err.errorInfo = errors;
        err.nativeErrorInfo = nil; err.httpStatusCode = 0;
        failureBlock(err);
        return nil;
    }
    
    NSMutableDictionary *infoDictionary =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:mailAddress, @"email", nil];
    
    if(applicationDictionary) {
        [infoDictionary setObject:applicationDictionary forKey:@"application"];
    }
    
    NSString *path = @"users/confirmation.json";
    if(params) {
        if([params objectForKey:AML_EMAIL_TEMPLATE_ID]) {
            path = [path stringByAppendingFormat:@"?email_template_id=%@",
                        [params objectForKey:AML_EMAIL_TEMPLATE_ID]];
            if([params objectForKey:AML_EMAIL_SUBJECT]) {
                NSString *utf8Encoding = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                                   (__bridge CFStringRef)[params objectForKey:AML_EMAIL_SUBJECT],
                                                                                                   NULL,
                                                                                                   CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                                   kCFStringEncodingUTF8));
                path = [path stringByAppendingFormat:@"&email_subject=%@",utf8Encoding];
            }
            if([params objectForKey:AML_EMAIL_BODY_HTML]) {
                NSString *utf8Encoding = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                                   (__bridge CFStringRef)[params objectForKey:AML_EMAIL_BODY_HTML],
                                                                                                   NULL,
                                                                                                   CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                                   kCFStringEncodingUTF8));
                path = [path stringByAppendingFormat:@"&email_body_html=%@",utf8Encoding];
            }
        }
    }
    
    NSDictionary *userParams = [[NSDictionary alloc] initWithObjectsAndKeys:infoDictionary, @"user", nil];

        return [[AylaApiClient sharedUserServiceInstance] postPath:path parameters:userParams
            success:^(AFHTTPRequestOperation *operation, id response) {
                saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"I", @"User",
                          @"statusCode", operation.response.statusCode, @"success",@"null", @"userResendConfirmationToken");
                AylaResponse *resp = [AylaResponse new];
                resp.httpStatusCode = operation.response.statusCode;
                successBlock(resp);
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"E", @"User",
                          @"httpStatusCode", operation.response.statusCode, @"response", operation.responseString, @"userResendConfirmationToken");
                AylaError *err = [AylaError new]; err.httpStatusCode = operation.response.statusCode;
                err.nativeErrorInfo = error;
                NSMutableDictionary *errList;
                if(operation.responseString != NULL){
                    err.errorCode = AML_USER_INVALID_PARAMETERS;
                    NSError *jerr = nil;
                    id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                    NSDictionary *resp = responseJSON;
                    NSDictionary *errors = [resp objectForKey:@"errors"];
                    NSArray *arr;
                    errList = [[NSMutableDictionary alloc] init];
                    
                    for(NSString* key in errors){
                        arr = [errors objectForKey:key];
                        [errList setObject:[arr objectAtIndex:0] forKey:key];
                    }
                    err.nativeErrorInfo = nil;
                    err.errorInfo = errList;
                }
                else{
                    err.errorCode = 1;
                    err.errorInfo = nil;
                }
                failureBlock(err);
            }
     ];
}


+ (NSOperation *)delete:(NSDictionary *) callParams
                   success:(void (^)(AylaResponse *response)) successBlock
                   failure:(void (^)(AylaError *err))failureBlock
{
    return [[AylaApiClient sharedUserServiceInstance] deletePath: @"users.json" parameters:nil
        success:^(AFHTTPRequestOperation *operation, id response) {
            saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"I", @"User",
                      @"statusCode", operation.response.statusCode, @"success",@"null", @"cancelAccount");
            AylaResponse *resp = [AylaResponse new];
            resp.httpStatusCode = operation.response.statusCode;
            successBlock(resp);
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"E", @"User",
                      @"httpStatusCode", operation.response.statusCode, @"response", operation.responseString, @"cancelAccount");
            AylaError *err = [AylaError new]; err.httpStatusCode = operation.response.statusCode;
            err.nativeErrorInfo = error;
            err.errorCode = AML_ERROR_FAIL;
            err.errorInfo = nil;
            failureBlock(err);
        }
     ];
}

+ (AylaUser *)currentUser {
    return _user;
}

+ (void)refreshAccessTokenOnExpiry
{
    if([AylaUser currentUser]) {
        AylaUser *user = [AylaUser currentUser];
        NSUInteger left = [AylaUser accessTokenSecondsToExpiry];
        if(left <= DEFAULT_ACCESS_TOKEN_REFRESH_THRRESHOLD && user.refreshToken) {
            [AylaUser refreshAccessTokenWithRefreshToken:user.refreshToken
                 success:^(AylaResponse *response, AylaUser *updatedUser) {
                     @synchronized(user) {
                         user.accessToken = updatedUser.accessToken;
                         user.refreshToken = updatedUser.refreshToken;
                         user.expiresIn = updatedUser.expiresIn;
                         user.updatedAt = updatedUser.updatedAt;
                     }
                     saveToLog(@"%@, %@, %@:%lu, %@", @"I", @"User",
                               @"httpStatusCode", response.httpStatusCode, @"refreshAccessTokenWithRefreshToken");
                 } failure:^(AylaError *err) {
                     saveToLog(@"%@, %@, %@:%lu, %@", @"E", @"User",
                               @"httpStatusCode", err.httpStatusCode, @"refreshAccessTokenWithRefreshToken");
                 }];
        }
    }
}

- (NSOperation *)createDatum:(AylaDatum *)datum
                     success:(void (^)(AylaResponse *response, AylaDatum *newDatum))successBlock
                     failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaDatum createWithObject:self andDatum:datum success:successBlock failure:failureBlock];
}

- (NSOperation *)getDatumWithKey:(NSString *)key
                         success:(void (^)(AylaResponse *response, AylaDatum *datum))successBlock
                         failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaDatum getWithObject:self andKey:key success:successBlock failure:failureBlock];
}

- (NSOperation *)getDatumWithParams:(NSDictionary *)params
                          success:(void (^)(AylaResponse *response, NSArray *datums))successBlock
                          failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaDatum getWithObject:self andParams:params success:successBlock failure:failureBlock];
}

- (NSOperation *)updateDatum:(AylaDatum *)datum
                     success:(void (^)(AylaResponse *response, AylaDatum *updatedDatum))successBlock
                     failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaDatum updateWithObject:self andDatum:datum success:successBlock failure:failureBlock];
}

- (NSOperation *)deleteDatum:(AylaDatum *)datum
                     success:(void (^)(AylaResponse *response))successBlock
                     failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaDatum deleteWithObject:self andDatum:datum success:successBlock failure:failureBlock];
}

- (NSOperation *)createShare:(AylaShare *)share
                     success:(void (^)(AylaResponse *resp, AylaShare *share))successBlock
                     failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaShare create:share object:self success:successBlock failure:failureBlock];
}

- (NSOperation *)getShares:(NSDictionary *)callParams
                   success:(void (^)(AylaResponse *response, NSArray *deviceShares)) successBlock
                   failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare get:self callParams:callParams success:successBlock failure:failureBlock];
}

+ (NSOperation *)getAllShares:(NSDictionary *)callParams
                      success:(void (^)(AylaResponse *response, NSArray *deviceShares)) successBlock
                      failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare get:nil callParams:callParams success:successBlock failure:failureBlock];
}

- (NSOperation *)getReceivedShares:(NSDictionary *)callParams
                           success:(void (^)(AylaResponse *response, NSArray *deviceShares)) successBlock
                           failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare getReceives:self callParams:callParams success:successBlock failure:failureBlock];
}

+ (NSOperation *)getAllReceivedShares:(NSDictionary *)callParams
                              success:(void (^)(AylaResponse *response, NSArray *deviceShares)) successBlock
                              failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare getReceives:nil callParams:callParams success:successBlock failure:failureBlock];
}

- (NSOperation *)getShareWithId:(NSString *)id
                    success:(void (^)(AylaResponse *resp, AylaShare *share))successBlock
                    failure:(void (^)(AylaError *error))failureBlock;
{
    return [AylaShare getWithId:id success:successBlock failure:failureBlock];
}

- (NSOperation *)updateShare:(AylaShare *)share
                     success:(void (^)(AylaResponse *resp, AylaShare *updatedShare))successBlock
                     failure:(void (^)(AylaError *error))failureBlock
{
    return [AylaShare update:share success:successBlock failure:failureBlock];
}

- (NSOperation *)deleteShare:(AylaShare *)share
                     success:(void (^)(AylaResponse *response)) successBlock
                     failure:(void (^)(AylaError *err))failureBlock
{
    return [AylaShare delete:share success:successBlock failure:failureBlock];
}

static AylaOAuth *oAuth = nil;
+ (void)loginThroughOAUTHWithAccountType:(AylaOAuthAccountType) accountType webView:(UIWebView *)webView AppId:(NSString *)appId andAppSecret:(NSString *)appSecret
                                 success:(void (^)(AylaResponse *response)) successBlock
                                 failure:(void (^)(AylaError *err))failureBlock
{
    // Clean auth token
    gblAuthToken = nil;
    
    [AylaUser retrieveOAuthURLWithAccountType:accountType AppId:appId andAppSecret:appSecret
          success:^(NSURL *url){
              
              NSString *redirectRemoteUrlStr = [accountType isEqualToString:aylaOAuthAccountTypeGoogle]? aylaOAuthRedirectUriLocal:aylaOAuthRedirectUriRemote ;
              
              NSURL *toWebViewURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@&redirect_uri=%@", [url absoluteString], [accountType isEqualToString:aylaOAuthAccountTypeGoogle]? aylaOAuthRedirectUriLocal:redirectRemoteUrlStr]];
              oAuth = [[AylaOAuth alloc] initWithType:accountType webView:webView];
              [oAuth authenticateOnWebViewWithURL:toWebViewURL success:^(NSString *code) {
                  saveToLog(@"%@, %@, %@:%@, %@", @"I", @"User",
                            @"success", @"success", @"authenticateOnWebViewWithURL");
                  NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:code, @"code", [accountType isEqualToString:aylaOAuthAccountTypeGoogle]? aylaOAuthRedirectUriLocal:aylaOAuthRedirectUriRemote, @"redirect_url", appId, @"app_id", accountType, @"provider", nil];
                  [AylaUser authenticateToService:params success:successBlock failure:failureBlock];
                  
              } failure:^(AylaError *err) {
                  failureBlock(err);
              }];
              
            } failure:^(AylaError *err) {
                failureBlock(err);
            }];
}

+ (void)retrieveOAuthURLWithAccountType:(AylaOAuthAccountType)type AppId:(NSString *)appId andAppSecret:(NSString *)appSecret
                                success:(void (^)(NSURL *url)) successBlock
                                failure:(void (^)(AylaError *err))failureBlock
{
    
    NSMutableDictionary *errors = [[NSMutableDictionary alloc] init];
    if(appId == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"appId"];
    }
    if(appSecret == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"appSecret"];
    }
    if(type == nil)
    {
        [errors setObject:@"can't be blank" forKey:@"accountType"];
    }
    
    if([errors count] != 0){
        AylaError *err = [AylaError new]; err.errorCode = AML_USER_INVALID_PARAMETERS;
        err.errorInfo = errors;
        err.nativeErrorInfo = nil; err.httpStatusCode = 0;
        failureBlock(err);
        return;
    }
    
    NSDictionary *applicationInfo = [[NSDictionary alloc] initWithObjectsAndKeys:appId, @"app_id", appSecret, @"app_secret", nil];
    NSDictionary *userParams = [[NSDictionary alloc] initWithObjectsAndKeys:type, @"auth_method", applicationInfo, @"application", nil];
    
    [[AylaApiClient sharedUserServiceInstance] postPath: @"users/sign_in.json"
             parameters:[[NSDictionary alloc] initWithObjectsAndKeys:userParams, @"user", nil]
                success:^(AFHTTPRequestOperation *operation, id response) {
                    saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"I", @"User",
                              @"statusCode", operation.response.statusCode, @"success",@"null", @"retrieveOAuthURLWithAccountType");
                    successBlock([NSURL URLWithString:[response objectForKey:@"url"]]);
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    saveToLog(@"%@, %@, %@:%d, %@:%@ %@", @"E", @"User",
                              @"httpStatusCode", operation.response.statusCode, @"responce", operation.responseString, @"retrieveOAuthURLWithAccountType");
                    AylaError *err = [AylaError new]; err.httpStatusCode = operation.response.statusCode;
                    err.nativeErrorInfo = error;
                    NSMutableDictionary *errList;
                    if(operation.responseString != NULL){
                        err.errorCode = AML_USER_INVALID_PARAMETERS;
                        NSError *jerr = nil;
                        id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                        NSDictionary *resp = responseJSON;
                        NSDictionary *errors = [resp objectForKey:@"errors"];
                        NSArray *arr;
                        errList = [[NSMutableDictionary alloc] init];
                        
                        for(NSString* key in errors){
                            arr = [errors objectForKey:key];
                            [errList setObject:[arr objectAtIndex:0] forKey:key];
                        }
                        err.nativeErrorInfo = nil;
                        err.errorInfo = errList;
                    }
                    else{
                        err.errorCode = 1;
                        err.errorInfo = nil;
                    }
                    failureBlock(err);
                }
     ];
}

+ (void) authenticateToService:(NSDictionary *)callParams
        success:(void (^)(AylaResponse *response)) successBlock
        failure:(void (^)(AylaError *err))failureBlock
{
    //NSString *code = [callParams objectForKey:@"code"];
    //NSDictionary *toService = [NSDictionary alloc] initWithObjectsAndKeys:callParams, @"" nil
    
    saveToLog(@"%@, %@, %@, %@", @"I", @"User", @"none", @"authenticateToService attempt");
    [[AylaApiClient sharedUserServiceInstance] postPath: @"users/provider_auth.json" parameters:callParams
            success:^(AFHTTPRequestOperation *operation, id authResp) {
                saveToLog(@"%@, %@, %@, %@", @"I", @"User", @"none", @"authenticateToService success");
                                                                    
                //auth_token = ([loginResponse valueForKeyPath:@"user.auth_token"] != [NSNull null]) ? [loginResponse valueForKeyPath:@"user.auth_token"] : @"";
                NSString *accessToken = ([authResp valueForKeyPath:@"access_token"] != [NSNull null]) ? [authResp valueForKeyPath:@"access_token"] : @"";
                if (!accessToken || [accessToken isEqualToString:@""]) {
                    saveToLog(@"%@, %@, %@:%@, %@", @"E", @"User", @"auth_token", @"null", @"authenticateToService No authorization token from device service.");
                    AylaError *err = [AylaError new]; err.errorCode = AML_USER_NO_AUTH_TOKEN; err.nativeErrorInfo = nil; err.errorInfo = nil;
                    failureBlock(err);
                } else {
                    
                    gblAuthToken = accessToken;
                    //NSString *refreshToken = ([loginResponse valueForKeyPath:@"refresh_token"] != [NSNull null]) ? [loginResponse valueForKeyPath:@"refresh_token"] : @"";
                    //NSNumber *exipryIn = ([loginResponse valueForKeyPath:@"expires_in"] != [NSNull null]) ? [loginResponse valueForKeyPath:@"expires_in"] : @"";
                    saveToLog(@"%@, %@, %@:%d", @"I", @"User", @"gblAuthToken", gblAuthToken?1:0, @"authenticateToService authorization header set");
                    AylaResponse *response = [AylaResponse new];
                    response.httpStatusCode = operation.response.statusCode;
                    successBlock(response);
                }
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"User", @"NSError.code", error.code, @"authenticateToService", [AylaSystemUtils shortErrorFromError:error]);
                
                AylaError *err = [AylaError new];
                NSMutableDictionary *description;
                if(operation.responseString != NULL){
                    err.errorCode = AML_USER_INVALID_PARAMETERS;
                    
                    NSError *jerr = nil;
                    id responseJSON = [NSJSONSerialization JSONObjectWithData: operation.responseData options:NSJSONReadingMutableContainers error:&jerr];
                    NSDictionary *resp = responseJSON;
                    
                    description = [[NSMutableDictionary alloc] initWithObjectsAndKeys: [resp valueForKeyPath:@"error"] , @"error", nil];
                    err.errorInfo = description;
                }
                else{
                    err.errorCode = 1;
                    err.errorInfo = nil;
                }
                err.httpStatusCode = operation.response.statusCode; err.nativeErrorInfo = error;
                failureBlock(err);
            }
     ];

}

@end
