//
//  AylaDeviceNode.m
//  iOS_AylaLibrary
//
//  Created by Yipei Wang on 7/11/14.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaDeviceNode.h"
#import "AylaDevice.h"
#import "AylaDeviceSupport.h"
#import "AylaSystemUtils.h"
#import "AylaNetworks.h"
#import "AylaError.h"
#import "AylaErrorSupport.h"
#import "NSObject+AylaNetworks.h"
#import "AylaApiClient.h"
#import "AylaDeviceSupport.h"

@implementation AylaDeviceNode : AylaDevice

static NSString * const kAylaDeviceType = @"device_type";
static NSString * const kAylaNodeType = @"node_type";

static NSString * const kAylaNodeTypeZigbee = @"Zigbee";

static NSString * const kAylaNodeParamGatewayDsn = @"gateway_dsn";

//override
- (instancetype)initDeviceWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self) {
        [self updateWithDictionary:dictionary];
        
        //TODO: SKIP because returned values are always null. check properties when init a new node instance, needs to be lan mode enabled
        /*
        dictionary =
        dictionary[@"device"]? dictionary[@"device"]: dictionary;
        
        NSArray *properties = [dictionary objectForKey:@"properties"];
        if(properties) {
            NSMutableDictionary *nodeProperties = [NSMutableDictionary dictionary];
            for(NSDictionary *propertyDictionary in properties) {
                AylaProperty *property = [[AylaProperty alloc] initDevicePropertiesWithDictionary:propertyDictionary];
                if(property && property.name) {
                    [nodeProperties setObject:property forKey:property.name];
                }
            }
            self.properties = nodeProperties;
        }
        */
    }
    return self;
}

- (NSOperation *)identifyWithParams:(NSDictionary *)callParams
                   success:(void (^)(AylaResponse *response, NSDictionary *responseParams))successBlock
                   failure:(void (^)(AylaError *err))failureBlock
{
    NSMutableDictionary *errors = [NSMutableDictionary new];
    NSMutableDictionary *params = [NSMutableDictionary new];
    if(!callParams) {
        [errors setObject:@"can not be blank." forKey:@"callParams"];
    }
    else {
        NSString *value = callParams[kAylaDeviceNodeParamIdentifyValue];
        if(value) {
            if([value isEqualToString:kAylaDeviceNodeParamIdentifyOn]) {
                
                NSNumber *duration = callParams[kAylaDeviceNodeParamIdentifyTime];
                if(duration &&
                   [duration isKindOfClass:[NSNumber class]]) {
                    [params setObject:duration forKey:kAylaDeviceNodeParamIdentifyTime];
                }
                else {
                    [errors setObject:@"can not be blank." forKey:kAylaDeviceNodeParamIdentifyTime];
                }
            }
            params[kAylaDeviceNodeParamIdentifyValue] = value;
        }
        else {
            [errors setObject:@"can not be blank." forKey:kAylaDeviceNodeParamIdentifyValue];
        }
    }
    if(errors.count > 0) {
        failureBlock([AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:0 nativeError:nil andErrorInfo:errors]);
        return nil;
    }

    NSString *path = [NSString stringWithFormat:@"devices/%@/identify.json", self.key];
    saveToLog(@"%@, %@, %@:%@, %@:%@, %@", @"I", @"AylaNode", @"url", path, @"userValues", callParams, @"identifyWithParams");
    return
    [[AylaApiClient sharedDeviceServiceInstance] putPath:path parameters:params
                                                        success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                            AylaResponse *resp = [AylaResponse new];
                                                            resp.httpStatusCode = operation.response.statusCode;
                                                            successBlock(resp, responseObject);
                                                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                            saveToLog(@"%@, %@, %@:%ld, %@:%ld, %@", @"E", @"AylaNode", @"NSError.code", (long)error.code, @"http", (long)operation.response.statusCode, @"identifyWithParams");
                                                            failureBlock([AylaError createWithCode:AML_AYLA_ERROR_FAIL httpCode:operation.response.statusCode nativeError:error andErrorInfo:operation.responseObject]);
                                                        }];
}

//override
- (void)updateWithDictionary:(NSDictionary *)deviceDictionary
{
    [super updateWithDictionary:deviceDictionary];
    NSDictionary *attributes = deviceDictionary[@"device"];
    self.nodeType = [[attributes objectForKey:kAylaNodeType] nilIfNull];
    self.gatewayDsn = [[attributes objectForKey:kAylaNodeParamGatewayDsn] nilIfNull];
}

+ (Class)deviceClassFromDeviceDictionary:(NSDictionary *)dictionary
{
    NSDictionary *attributes = dictionary[@"node"]?:dictionary[@"device"];
    Class deviceClass;
    if(attributes &&
       attributes[kAylaNodeType]) {
        deviceClass = [AylaDeviceNode deviceClassFromNodeType:attributes[kAylaNodeType]];
    }
    else if(attributes &&
            [attributes[kAylaDeviceType] isEqualToString:kAylaDeviceTypeNode]) {
        deviceClass = [AylaDeviceNode class];
    }
    return deviceClass?:[AylaDevice class];
}

static NSString * const kAylaDeviceNodeClassNameZigbee = @"AylaDeviceZigbeeNode";
+ (Class)deviceClassFromNodeType:(NSString *)nodeType
{
    static Class AylaDeviceNodeClassZigbee;
    static Class AylaDeviceClass;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        AylaDeviceClass = [AylaDeviceNode class]; //SALUS FIX scs-1108: Default to AylaDeviceNode to get gatewayDSN from device calls.
        //AylaDeviceClass = [AylaDevice deviceClassFromClassName:kAylaDeviceClassName];
        AylaDeviceNodeClassZigbee = [AylaDevice deviceClassFromClassName:kAylaDeviceNodeClassNameZigbee];
    });
    
    if(AylaDeviceNodeClassZigbee &&
       nodeType &&
       [nodeType isEqualToString:kAylaNodeTypeZigbee]) {
        return AylaDeviceNodeClassZigbee;
    }
    return AylaDeviceClass;
}

//override
- (void)lanModeEnable
{
    saveToLog(@"%@, %@, %@:%@, %@, %@", @"E", @"AylaDeviceNode", @"lanMode", @"add lan mode implementation on gateway level", @"lanModeEnable");
}

//override
- (void)lanModeDisable
{
    saveToLog(@"%@, %@, %@:%@, %@, %@", @"E", @"AylaDeviceNode", @"lanMode", @"add lan mode implementation on gateway level", @"lanModeEnable");
}

//--------------------caching helper methods----------------------

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [super encodeWithCoder:encoder];
    [encoder encodeObject:_nodeType forKey:@"nodeType"];
    [encoder encodeObject:_gatewayDsn forKey:@"gatewayDsn"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    AylaDeviceNode *node = [super initWithCoder:decoder];
    node.nodeType = [decoder decodeObjectForKey:@"nodeType"];
    node.gatewayDsn = [decoder decodeObjectForKey:@"gatewayDsn"];
    return node;
}

//--------------------------helpful methods----------------------------
- (id)copyWithZone:(NSZone *)zone
{
    id copy = [super copyWithZone:zone];
    if (copy) {
        AylaDeviceNode *_copy = copy;
        _copy.nodeType = [_nodeType copy];
        _copy.gatewayDsn = [_gatewayDsn copy];
    }
    return copy;
}
@end

NSString * const kAylaDeviceNodeParamIdentifyValue = @"value";
NSString * const kAylaDeviceNodeParamIdentifyTime = @"time";
NSString * const kAylaDeviceNodeParamIdentifyOn = @"On";
NSString * const kAylaDeviceNodeParamIdentifyOff = @"Off";
NSString * const kAylaDeviceNodeParamIdentifyResult = @"Result";

NSString * const kAylaDeviceNodeParamIdentifyId = @"id";
NSString * const kAylaDeviceNodeParamIdentifyStatus = @"status";