"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $, _) {
		/**
		 * This mixin is coupled to the validation mixin.
		 *
		 * The expectation is that this form contains inputs that use the validation mixin so that they have
		 * the isValid and showErrors methods
		 *
		 */
		Mixins.FormWithValidation = function () {
			this.setDefaults({
				/**
				 * Check all the views that have been designated as needing validation.  Will display error messages
				 * if needed
				 *
				 * @param showError - will try and call the showError method of the validation mixin if this is true
				 * @returns boolean whether the form is valid or not
				 */
				isValid: function (showError) {
					var valid = true;

					_.each(this.validationNeeded, function (view) {
						var viewValid = view.isValid ? view.isValid() : true;

						if (!viewValid.valid) {
							valid = false;

							if (showError && view.showErrors) {
								view.showErrors(viewValid.message);
							}
						} else {
							if (view.hideErrors) {
								view.hideErrors();
							}
						}
					});

					return valid;
				},

				//TODO: check if we still need this later
				validateForm: function () {
					//Implement this function in parent object
					//You can promote "change:value" event up the tree see ex below.
					//ex: this.trigger("change:value");
					throw Error("validateForm should be overwritten");
				},

				attachValidationListener: function () {
					var that = this;

					_.each(this.validationNeeded, function (view) {
						that.listenTo(view, "change:value", that.validateForm);
					});
				}
			});

			this.before("initialize", function () {
				// list of views to be validated
				this.validationNeeded = [];
			});
		};
	});

	return App.Consumer.Views.Mixins.FormWithValidation;
});