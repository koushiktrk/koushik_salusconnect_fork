"use strict";

define([
	"app",
	"common/commonTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, commonTemplates, SalusViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.RadioView = Mn.ItemView.extend({
			template: commonTemplates["salusForms/salusRadio"],
			className: "salus-radio",
			onRender: function () {
				if (this.model.get("classes")) {
					this.$el.addClass(this.model.get("classes"));
				}

				if (this.model.get("isChecked")) {
					this.$("input").attr("checked", "");
				}

				if (this.model.get("radioClass")) {
					this.$("input").addClass(this.model.get('radioClass'));
				}
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Views.RadioView;
});