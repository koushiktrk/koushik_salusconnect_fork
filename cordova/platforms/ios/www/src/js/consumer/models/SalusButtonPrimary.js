"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Models", function (Models, App, B) {
		Models.SalusButtonPrimaryModel = B.Model.extend({
			defaults: {
				buttonTextKey: null
			}
		});
	});

	return App.Consumer.Models.SalusButtonPrimaryModel;
});