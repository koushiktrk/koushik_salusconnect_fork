"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/dashboard/views/header/Overview.view",
	"consumer/dashboard/views/header/Scan.view",
	"consumer/dashboard/views/header/ScanResults.view"
], function (App, P, templates, SalusView) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn, $, _) {
		Views.DashboardHeaderLayout = Mn.LayoutView.extend({
			template: templates["dashboard/header/layout"],
			className: "dashboard-header",
			ui: {
				"main": ".bb-dashboard-header-main"
			},
			regions: {
				"overview": ".bb-overview",
				"scan": ".bb-scan",
				"scanResults":".bb-scan-results-region"
			},
			initialize: function () {
				var that = this;
				
				_.bindAll(this, "_showScan", "_showOverview", "_setupScanViews", "_closeOnGatewayChange", "_insertGatewayImage");
				
				this.manager = this.options.manager;
				this.gateway = this.options.gateway;

				this.listenTo(this.manager, "update:manager", this._setupScanViews);
				this.listenTo(App.salusConnector, "change:currentGateway", this._closeOnGatewayChange);
				this.listenToOnce(this.gateway, "unregistered:device", function (/*gateway*/) {
					that.destroy();
				});
			},

			onRender: function () {
				var that = this;

				this.manager.loadPromise.then(function () {
					that._insertGatewayImage();

					if (!that.gateway) {
						//If we dont have a gateway lets just show the default background
						//and remove the controls.
						if (that.overviewView) {
							that.overviewView.$el.hide();
						}
					}
				});

				this.$el.attr("data-gateway-dsn",  !!this.gateway ? this.gateway.get("dsn") : "");

				if (!!this.gateway) {
					if (this.overviewView) {
						this.overviewView.destroy();
					}
					
					this.overviewView = new Views.Overview({
						gateway: this.gateway
					});

					this.overview.show(this.overviewView);
				}

				this.scanView = new Views.ScanLayout({
					manager: this.manager
				});
				this.scan.show(this.scanView);
				
				this._setupScanViews();

				this.listenTo(this.scanView, "click:resultsControl", this._displayScanResults);
				this.listenTo(this.scanView, "click:close", this._showOverview);

				// TODO scs-748 remove listenTo in onRender
				this.listenTo(this.overviewView, "openSettings", this._openSettings);
				this.listenTo(this.scanView, "click:close", this._showOverview);
				this.listenTo(this.manager, "gatewayPhoto:loaded", this._insertGatewayImage);
			},

			_setupScanViews: function () {
				// reinit scanResultsLayout for new manager
				this.scanResultsView = new Views.ScanResultsLayout({
					manager: this.manager
				});
				this.scanResults.show(this.scanResultsView);
			},

			_insertGatewayImage: function () {
				if (!this.manager) {
					return;
				}
				
				var backgroundUrl;

				// we get it from the manager if we have no gateway
				if (!!this.gateway) {
					backgroundUrl = this.manager.getGatewayInfo(this.gateway).get("backgroundImage");
				} else {
					backgroundUrl = this.manager.get("backgroundImage");
				}

				if (backgroundUrl) {
					var backgroundString = "linear-gradient(180deg, rgba(0, 0, 0, 0) 50%, rgba(21, 21, 21, 1) 100%), url(" +
							App.rootPath(backgroundUrl) + ") center no-repeat",
						main = this.$(".bb-dashboard-header-main");

					main.css("background", backgroundString);
					main.css("background-size", "cover");
					main.css("background-position", "center");
				}
			},

			_showScan: function () {
				this.overview.$el.addClass("hidden");
				this.scan.$el.removeClass("hidden");

				this.manager.startScan();
				this._displayScanResults();
				
				$(".bb-dashboard-inner-header").slick("slickSetOption", "accessibility", false, false);
				$(".bb-dashboard-inner-header").slick("slickSetOption", "swipe", false, false);
			},

			_openSettings: function () {
				this.settingsView = new Views.SettingsPanelView();

				this.$(".bb-overview").append(this.settingsView.render().$el);

				this.listenTo(this.settingsView, "scan", this._showScan);
				this.listenTo(this.settingsView, "capturePhotoClick", this._capturePhoto);
				this.listenTo(this.settingsView, "photoLibraryClick", this._photoLibrary);
				this.listenTo(this.settingsView, "close", this.closeSettings);
			},

			closeSettings: function () {
				this.settingsView.destroy();
			},

			/**
			 * Click event handler for getting a photo from the camera.
			 * @private
			 */
			_capturePhoto: function () {
				var that = this,
					CAMERA = 1;
				this.settingsView.showSpinner();
				this.manager.takePhoto(CAMERA).then(function () {
					that.settingsView.hideSpinner();
					that.closeSettings();
				}).catch(function () {
					that.settingsView.hideSpinner();
				});
			},

			/**
			 * Click event handler for getting a photo from mobile library.
			 * @private
			 */
			_photoLibrary: function () {
				var that = this,
					PHOTOLIBRARY = 0;
				this.settingsView.showSpinner();
				this.manager.takePhoto(PHOTOLIBRARY).then(function () {
					that.settingsView.hideSpinner();
					that.closeSettings();
				}).catch(function () {
					that.settingsView.hideSpinner();
				});
			},

			_showOverview: function () {
				this.manager.stopScan();

				this.scan.$el.addClass("hidden");
				this.$(".bb-scan-results-region").addClass("hidden");
				this.overview.$el.removeClass("hidden");
				
				$(".bb-dashboard-inner-header").slick("slickSetOption", "accessibility", true, false);
				$(".bb-dashboard-inner-header").slick("slickSetOption", "swipe", true, false);
			},

			_displayScanResults: function () {
				if (this.$(".bb-scan-results-region").hasClass("hidden")) {
					this.$(".bb-scan-results-region").removeClass("hidden");
				} else {
					this.$(".bb-scan-results-region").addClass("hidden");
				}
			},

			_closeOnGatewayChange: function () {
				// do some cleanup on the scans
				this._showOverview();
				this.$(".bb-scan-results-region").addClass("hidden");
			},

			onBeforeDestroy: function () {
				this.gateway = null;

				if (this.settingsView) {
					this.settingsView.destroy();
				}
				
				if (this.scanView) {
					this.scanView.destroy();
				}

				if (this.scanResultsView) {
					this.scanResultsView.destroy();
				}

				if (this.overviewView) {
					this.overviewView.destroy();
				}
			},

			onDestroy: function () {
				this.manager = null;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.DashboardHeaderLayout;
});