"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/views/EquipmentCollection.view",
	"consumer/schedules/views/modal/ThermostatCheckboxCollection.view",
	"common/model/salusWebServices/ThermostatSingleControl.model",
	"common/model/ayla/Group.model",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusLinkButton.view",
	"consumer/views/FormTextInput.view",
	"consumer/views/mixins/mixin.validation"
], function (App, P, consumerTemplates, SalusViewMixin) {
	App.module("Consumer.Schedules.Views.Modal", function (Views, App, B, Mn, $, _) {
		Views.SingleControlModalWidget = Mn.LayoutView.extend({
			id: "bb-single-control-widget",
			template: consumerTemplates["schedules/modal/singleControlModalWidget"],
			regions: {
				singleControlNameTextbox: ".bb-sc-name-textbox",
				thermostatsRegion: ".bb-thermostats-available",
				deleteButtonRegion: ".bb-delete-sc"
			},
			initialize: function () {
				_.bindAll(this, "handleDeleteClick");

				this.isEdit = this.options.model.get("isEdit") || false;
			},
			onRender: function (options) {
				var tstatCollection = App.salusConnector.getThermostatCollection();

				this.singleControlNameTextbox.show(new App.Consumer.Views.FormTextInput({
					labelText: "schedules.edit.modal.nameLabel",
					value: options.model.get("name"),
					readonly: !this.isEdit,
					required: true
				}));

				this.deleteButtonRegion.show(new App.Consumer.Views.SalusLinkButtonView({
					buttonText: App.translate("schedules.edit.modal.deleteLabel"),
					cssClass: "margin-l-10",
					clickedDelegate: this.handleDeleteClick
				}));

				this.thermostatsRegion.show(new Views.ThermostatCheckboxCollection({
					collection: tstatCollection,
					singleControl: options.model
				}));

				this.listenTo(this.singleControlNameTextbox, "change:value", function (textBox) {
					this.model.set("name", textBox.getValue());
				});
			},
			handleDeleteClick: function () {
				var that = this;

				//Ensure model to remove is NOT a newly added single control, since these aren't server side yet
				if (this.model.get("isEdit")) {
					that.trigger("remove:tsc");
					return;
				}

				return this.model.unregister().then(function () {
					App.Consumer.Schedules.PageEventController.trigger("singleControl:removed");
					return App.salusConnector.getTSCCollection().refresh();
				});
			},
			saveTSC: function() {
				if (this.model.get("isEdit")) {
					return this.createTSC();
				} else {
					return this.updateTSC();
				}
			},
			createTSC: function () {
				// If view has been destroyed, do nothing
				if (this.isDestroyed) {
					return P.resolve();
				}

				var deviceKeys = this.thermostatsRegion.currentView.getCheckedItemKeys(),
						deviceModels,
						oemModels = [],
						name = this.singleControlNameTextbox.currentView.getValue();

				if (deviceKeys && deviceKeys.length === 0) {
					return P.reject("Need at least one device for linked devices");
				}

				if (!_.isString(name) || (_.isString(name) && name.length === 0)) {
					return P.reject("Not valid name");
				}

				deviceModels = _.map(deviceKeys, function (key) {
					var device = App.salusConnector.getDevice(key);
					oemModels.push(device.get("oem_model"));
					return {
						EUID: device.getEUID(),
						oem_model: device.get("oem_model")
					};
				});

				if (_.uniq(oemModels).length > 1) {
					return P.reject("A linked thermostat group must only be for a single type of thermostat.");
				}

				this.tscModel = new App.Models.ThermostatSingleControlModel({
					dsn: App.getCurrentGatewayDSN(),
					devices: deviceModels,
					name: name
				});

				return this.tscModel.add();
			},
			updateTSC: function () {
				// If view has been destroyed, do nothing
				if (this.isDestroyed) {
					return P.resolve();
				}

				var devices = this.thermostatsRegion.currentView.getCheckedDevices(),
						name = this.singleControlNameTextbox.currentView.getValue();

				if (devices && devices.length <= 1) {
					return P.reject("Need at least two devices for linked devices");
				}

				if (!_.isString(name) || (_.isString(name) && name.length === 0)) {
					return P.reject("Not valid name");
				}

				var oemModels = devices.map(function(dev) {return dev.oem_model;});
				if (_.uniq(oemModels).length > 1) {
					return P.reject("A linked thermostat group must only be for a single type of thermostat.");
				}

				this.model.set("devices", devices);
				this.model.set("name", name);

				return this.model.update();
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Schedules.Views.Modal.SingleControlModalWidget;
});
