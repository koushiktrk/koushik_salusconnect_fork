"use strict";

define([
	"app",
	"application/Router",
	"consumer/equipment/equipment.controller",
	"common/model/salusWebServices/rules/RuleMakerManager"
], function (App, AppRouter, Controller) {

	App.module("Consumer.Equipment", function (Equipment, App, B, Mn, $, _) {
		Equipment.Router = AppRouter.extend({
			appRoutes: {
				//equipment routes
				"equipment(/myEquipment)": "myEquipment",
				"equipment/myEquipment/(:dsn)": "equipment",
				"equipment/categories/(:type)": "category",
				"equipment/newGroup": "newGroup",
				"equipment/editGroup/(:groupId)": "editGroup",
				"equipment/groups/(:groupId)": "group",
				"equipment/messageCenter": "messageCenter",
				"equipment/tsc": "tsc"
			}
		});

		App.addInitializer(function () {
			Equipment.PageEventController = {};
			_.extend(Equipment.PageEventController, B.Events);

			return new Equipment.Router({
				controller: Controller
			});
		});
	});
});