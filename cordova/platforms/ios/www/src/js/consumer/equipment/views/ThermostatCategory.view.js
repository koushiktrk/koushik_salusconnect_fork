"use strict";

define([
	"app",
	"roundslider",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.thermostatArcView",
	"consumer/equipment/views/tsc/ThermostatSingleControlPage.view",
	"consumer/equipment/views/myEquipment/AddNewTile.view",
	"consumer/equipment/views/CreateSchedule.view"
], function (App, RoundSlider, constants, consumerTemplates, SalusViewMixin, ArcViewMixin) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {

		Views.ThermostatCategoryView = Mn.LayoutView.extend({
			id: "bb-thermostat-category-page",
			template: consumerTemplates["equipment/myEquipment/thermostatCategoryPage"],
			regions: {
				singleControlsRegion: ".bb-single-controls",
				individualControlsRegion: ".bb-individual-controls",
				scheduleMenuRegion: ".bb-schedule-menu"
			},
			ui: {
				success: "#bb-tsc-success",
				schedule: "#bb-schedule-menu"
			},
			initialize: function () {
				this.createSchedulesView = new Views.CreateScheduleView({
					categoryType: constants.categoryTypes.THERMOSTATS
				});
			},
			onRender: function () {
				var that = this;

				var promise = App.salusConnector.getDataLoadPromise(["devices", "tscs"]);

				if (promise.isFulfilled()) {
					promise = App.salusConnector.getTSCCollection().refresh();
				}

				promise.then(function () {
					var tscCollection = App.salusConnector.getTSCCollection(),
						individualControlsCollection = App.salusConnector.getIndividualControlThermostats();

					that.singleControlsRegion.show(new Views.ThermostatSingleControlsView({
						collection: tscCollection
					}));

					that.individualControlsRegion.show(new Views.ThermostatIndividualControlsView({
						collection: individualControlsCollection
					}));

					that.scheduleMenuRegion.show(that.createSchedulesView);

					// TODO: show the success area if we just came from the tsc create page and made one
					// TODO: apparently this page will only be shown when that is the case
					// TODO: the thermostat categories will be a horizontal tile page (like other categories) but
					// TODO: with sections for tsc/individual
				});
			}
		}).mixin([SalusViewMixin]);

		// create a tsc button
		Views.CreateSingleControlView = Mn.LayoutView.extend({
			className: "create-tsc row",
			template: consumerTemplates["equipment/myEquipment/createSingleControl"],
			events: {
				"click .bb-create-tsc-wrapper": "goToCreateTSCPage"
			},
			ui: {
				text: ".bb-create-tsc-text"
			},
			onRender: function () {
				if (this.model && this.model.has("i18nTextKey")) {
					this.ui.text.text(App.translate(this.model.get("i18nTextKey")));
				}
			},
			goToCreateTSCPage: function () {
				App.navigate("equipment/tsc");
			}
		}).mixin([SalusViewMixin]);

		// represents a single control row
		Views.SingleControlView = Mn.CompositeView.extend({
			template: consumerTemplates["equipment/myEquipment/thermostatSingleControl"],
			className: "single-control row",
			childView: Views.ThermostatSingleControlDeviceView, // reusing from tsc create
			childViewContainer: ".bb-devices-container",
			ui: {
				thermostatsIn: ".bb-thermostats-in",
				count: ".bb-thermostats-count",
				tstatArc: ".bb-tstat-arc"
			},
			initialize: function () {
				if (this.model) {
					this.name = this.model.get("name");
					this.count = this.model.get("devices").length;
					this.firstDevice = this.model.getDefaultDevice();
				}
			},
			onRender: function () {
				this.ui.thermostatsIn.text(App.translate("equipment.myEquipment.categories.thermostats.thermostatsIn") + " " + (this.name ? this.name : "Single Control"));
			},
			onShow: function () {
				var tempToDisplay = this.getTemperature(),
					tstatOuterWidth = this.ui.tstatArc.innerWidth();

				this.ui.tstatArc.roundSlider({
					sliderType: "min-range",
					handleShape: "round",
					circleShape: "pie",
					handleSize: "+20",
					radius: tstatOuterWidth,
					min: 5, // TODO set this based off of the min range from the actual device
					max: 35, // TODO set this based off of the max range from the actual device
					step: "0.5",
					value: tempToDisplay,
					startAngle: 315,
					width: 10,
					showTooltip: true,
					animation: true,
					readOnly: true
				});
			},
			getTemperature: function () {
				var temperatureEndpoint = null,
					temperature = 0;

				if (this.firstDevice && this.firstDevice.get("SystemMode")) {
					switch (this.firstDevice.getPropertyValue("SystemMode")) {
						case constants.thermostatModeTypes.OFF:
							App.log("Debugging: what do we do if the temperature is adjusted when the mode is off?");
							break;
						case constants.thermostatModeTypes.AUTO:
							App.log("Debugging: what do we do if the temperature is adjusted when the mode is auto?");
							break;
						case constants.thermostatModeTypes.EMERGENCYHEATING:
						case constants.thermostatModeTypes.HEAT:
							temperatureEndpoint = "HeatingSetpoint_x100";
							break;
						case constants.thermostatModeTypes.COOL:
							temperatureEndpoint = "CoolingSetpoint_x100";
							break;
						default:
							break;
					}
				} else {
					temperatureEndpoint = "temporaryTemperatureValue";
				}

				if (!_.isNull(temperatureEndpoint) && this.firstDevice.get(temperatureEndpoint)) {
					temperature = this.firstDevice.getPropertyValue(temperatureEndpoint);
					temperature = temperature / 100;
				}

				return temperature;
			},
			templateHelpers: function () {
				return {
					count: this.count > 0 ? this.count : 2,
					name: this.name || ""
				};
			}
		}).mixin([SalusViewMixin]);

		// uses thermostat arc mixin. basically a thermostat tile but readonly
		Views.IndividualControlView = Mn.ItemView.extend({
			template: consumerTemplates["equipment/myEquipment/thermostatIndividualControl"],
			className: "individual-thermostat tile-shadow col-sm-4 col-xs-3",
			attributes: {
				role: "button"
			},
			ui: {
				degreesLabel: ".bb-degrees-label"
			},
			bindings: {
				".bb-thermostat-name": {
					observe: "name"
				},
				":el": {
					observe: "RunningMode",
					onGet: function (prop) {
						return prop ? prop.getProperty() : null;
					},
					update: function ($el, val) {
						var modeClassName,
								classNameArray = [
									"idle",
									"cooling",
									"heating"
								];

						$el.removeClass(classNameArray.join(" "));

						if (_.isNull(val)) {
							modeClassName = classNameArray[0];
						} else {
							switch (val) {
								case constants.runningModeTypes.OFF:
									modeClassName = classNameArray[0];
									break;
								case constants.runningModeTypes.COOL:
									modeClassName = classNameArray[1];
									break;
								case constants.runningModeTypes.HEAT:
									modeClassName = classNameArray[2];
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
					}
				}
			},
			events: {
				"click": "goToEquipmentPage"
			},
			initialize: function () {
				// take away the click events for mode menus
				this.noMixinEvents = true;
			},
			onShow: function () {
				// utilize mixin
				if (this.handleDomLoadComplete && !this.isDestroyed) {
					this.handleDomLoadComplete();

					if (!!this.ui.tstatArc) {
						this.ui.tstatArc.roundSlider({
							readOnly: true
						});
					}
				}

				// for xs
				var setPoint = this._getWhichSetPointToRead();

				// not grabbing temporarySetPoint
				if (!_.isNull(setPoint) && setPoint.indexOf("temporary") === -1) {

					var temp = 0;
					if (this.model.has(setPoint)) {
						temp = this.model.getPropertyValue(setPoint) / 100;
					}

					this.ui.degreesLabel.text(temp.toString() + "\xB0");
				}
			},
			goToEquipmentPage: function () {
				App.navigate("equipment/myEquipment/" + this.model.get("dsn"));
			}
		}).mixin([SalusViewMixin, ArcViewMixin]);

		// single controls section
		Views.ThermostatSingleControlsView = Mn.CompositeView.extend({
			template: consumerTemplates["equipment/myEquipment/thermostatCategorySingleControls"],
			className: "single-controls",
			childView: Views.SingleControlView,
			childViewContainer: ".bb-single-controls-container",
			childViewOptions: function (model) {
				if (model) {
					var models = _.map(model.get("devices"), function (deviceObj) {
						return App.salusConnector.getDeviceByEUID(deviceObj.EUID);
					});

					return {
						collection: new B.Collection(models)
					};
				}
			},
			events: {
				"click .bb-edit-icon": "handleEditTSCClick"
			},
			initialize: function () {
				_.bindAll(this, "handleEditTSCClick");
			},
			onRender: function () {
				// this addView is different from the others  - it has spacing to the right
				if (!this.createTscView) {
					this.createTscView = new Views.CreateSingleControlView({
						model: new B.Model({
							i18nTextKey: "equipment.myEquipment.categories.thermostats.createTsc"
						})
					});
				}

				var $listContainer = this.$childViewContainer;

				if (!$listContainer) {
					//$childViewContainer will be null if collection is empty.
					$listContainer = this.$(".bb-single-controls-container");
				}

				$listContainer.prepend(this.createTscView.render().$el);
			},
			onDestroy: function () {
				this.createTscView.destroy();
			},
			handleEditTSCClick: function () {
				App.navigate("equipment/tsc");
			}
		}).mixin([SalusViewMixin]);

		// individual controls sections
		Views.ThermostatIndividualControlsView = Mn.CompositeView.extend({
			template: consumerTemplates["equipment/myEquipment/thermostatCategoryIndividualControls"],
			className: "individual-controls",
			childViewContainer: ".bb-individual-controls-container",
			childView: Views.IndividualControlView,
			onRender: function () {
				if (!this.addDeviceTile) {
					this.addDeviceTile = new App.Consumer.Equipment.Views.AddNewTileView({
						i18nTextKey: "equipment.myEquipment.groups.addNewEquipment",
						size: "regular",
						classes: "individual-thermostat add-new",
						clickDestination: "setup/pairing", // goes to gateway pairing
						swapBorderEls: true
					});
				}

				var listContainer = this.$childViewContainer;

				if (!listContainer) {
					//$childViewContainer will be null if collection is empty.
					listContainer = this.$(".bb-individual-controls-container");
				}

				listContainer.prepend(this.addDeviceTile.render().$el);
			},
			onDestroy: function () {
				this.addDeviceTile.destroy();
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Equipment.Views.ThermostatCategoryView;
});
