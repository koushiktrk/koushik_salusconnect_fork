"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/model/salusWebServices/rules/Rule.model",
	"common/model/ayla/AylaTriggerCollection"
], function (App, P, AylaConfig, AylaBackedMixin) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.RuleCollection = B.Collection.extend({
			model: App.Models.RuleModel,

			initialize: function (data, options) {
				if (options && options.dsn) {
					this.dsn = options.dsn;
				}

				this.triggers = new App.Models.AylaTriggerCollecton(null, {
					gatewayDsn: this.dsn
				});
			},

			load: function (isLowPriority) {
				var that = this,
					dsn = App.salusConnector.getDevice(this.dsn).getGatewayNodeDSN() || this.dsn, // fallback to gateway dsn
					fetchUrlModel = {
						dsn: dsn
					};

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.rule.list)(fetchUrlModel), null, "GET", null, {isLowPriority: isLowPriority}).then(function (data) {
					return that._loadTriggers(isLowPriority).then(function () {
						that.set(data.rules, {
							parse: true,
							dsn: that.dsn
						});

						// fill in any delayed action information from a linked rule
						that.each(function (rule) {
							rule._completeDelayedActions();
						});

						return that._clearOrphanedDownstreamRules().then (function () {
							App.salusConnector.trigger("loaded:ruleCollection");
						});
					});
				});
			},

			setDSN: function (dsn) {
				if (_.isString(dsn)) {
					this.dsn = dsn;

					this.triggers.setDSN(dsn);
				}
			},

			unregisterAll: function () {
				var that = this,
						dsn = App.salusConnector.getDevice(this.dsn).getGatewayNodeDSN() || this.dsn, // fallback
						urlModel = {
							dsn: dsn
						};

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.rule.list)(urlModel), null, "DELETE", "text").then(function () {
					// don't need to call remove from tile order - tile order getting deleted on gateway unregister
					that.destroy();
				});
			},

			findRuleByUxHintKeyValuePair: function (key, value) {
				return this.filter(function (rule) {
					var uxhint = rule.uxhint;
					return uxhint && uxhint[key] && uxhint[key] === value;
				});
			},

			getNonHiddenRules: function () {
				return new Models.RuleCollection(this.filter(function (rule) {
					return !rule.hiddenRule;
				}));
			},

			refresh: function (isLowPriority) {
				var promise = this.load(isLowPriority);
				App.salusConnector.setDataLoadPromise("rules", promise);

				return promise;
			},

			isLoaded: function () {
				return !!this.length; //todo
			},

			updateTriggers: function (triggers) {
				return this.triggers.updateValues(triggers);
			},

			_clearOrphanedDownstreamRules: function () {
				var that = this, orphans = [], downstreamRules;

				downstreamRules = this.filter(function (rule) {
					var ruleTriggerKey = rule.get("ruleTriggerKey");
					return ruleTriggerKey && ruleTriggerKey.indexOf("timerTriggerKey") > -1;
				});

				_.each(downstreamRules, function (rule) {
					if (!that.findWhere({timerTriggerKey: rule.get("ruleTriggerKey")})) {
						orphans.push(rule.unregister());
					}
				});

				return P.all(orphans);
			},

			getAllRecipients: function () {
				var returnObj = {
					email: [],
					sms: []
				};

				this.each(function (rule) {
					// union removes duplicates
					returnObj.email = _.union(returnObj.email, rule.getRecipients().email);
					returnObj.sms = _.union(returnObj.sms, rule.getRecipients().sms);

					// sms are objects, so make unique by looking at the phone number
					returnObj.sms = _.uniq(returnObj.sms, false, function (smsObj) {
						return smsObj.phoneNumber;
					});
				});

				return returnObj;
			},

			getTriggersForRule: function (rule, actions) {
				return this.triggers.getTriggersForRule(rule, actions);
			},

			_loadTriggers: function (isLowPriority) {
				return this.triggers.load(isLowPriority);
			}
		}).mixin([AylaBackedMixin], {
			apiWrapperObjectName: "rule"
		});
	});

	return App.Models.RuleModel;
});
