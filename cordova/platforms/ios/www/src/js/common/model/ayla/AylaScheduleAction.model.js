"use strict";
define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked"
], function (App, P, AylaConfig, AylaBackedMixin) {
	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.AylaScheduleAction = B.Model.extend({
			defaults: {
				type: "SchedulePropertyAction",
				schedule_id: null,
				value: null,
				name: null,
				key: null,
				base_type: null,
                at_start: false,
				in_range: true
			},

			setScheduleId: function (id) {
				this.set("schedule_id", id);
			},

			aParse: function (data) {
				return data.schedule_action;
			},

			add: function () {
				var that = this,
						url = _.template(AylaConfig.endpoints.device.schedules.actions)({schedule_id: this.get("schedule_id")}),
						payload = {
							"schedule_action": this.toJSON()
						};

				return App.salusConnector.makeAjaxCall(url, payload, "POST", "json").then(function (data) {
					that.set(that.aParse(data));
				});
			},

			update: function () {
                
                //旧的action的at_start是true,in_range是false,所以暂且在更新的时候替换掉他们
                this.set("at_start",false);
				this.set("in_range",true);
                
				var that = this,
						url = _.template(AylaConfig.endpoints.device.schedules.putActions)({key: this.get("key")}),
						payload = {
							"schedule_action": this.toJSON()
						};

				return App.salusConnector.makeAjaxCall(url, payload, "PUT", "json").then(function (data) {
					that.set(that.aParse(data));
				});
			},

			deepCopy: function () {
				return _.omit(this.toJSON(), ["name", "key", "base_type", "schedule_id"]); // excluding the name as different thermostats may have different properties
			}
		}).mixin([AylaBackedMixin]);

		Models.AylaScheduleActionCollection = B.Collection.extend({
			model: Models.AylaScheduleAction,

			initialize: function (data, options) {
				options = options || {};
				this.scheduleId = options.schedule_id;
			},

			load: function (isLowPriority) {
				var that = this, url = _.template(AylaConfig.endpoints.device.schedules.actions)({schedule_id: this.scheduleId });

				return App.salusConnector.makeAjaxCall(url, null, "GET", null, {isLowPriority: isLowPriority}).then(function (actions) {
					that.set(actions, { parse: true });
				});
			},

			saveAll: function () {
//				return P.all(this.map(function(action) {
//					if (action.get("key")) {
//						return action.update();
//					} else {
//						return action.add();
//					}
//				}));

                var savePromise = P.defer();
                
                this.takeTurnSave(this.models,0,savePromise);
                
                
                return savePromise.promise;

			},
            
            takeTurnSave: function (actions,i,promise){
                var that=this;
                
                if(actions){
                    if(!i){
                        i=0;
                    }
                    
                    if(actions[i]){
                        var p;
                        if (actions[i].get("key")) {
    						p=actions[i].update();
    					} else {
    						p=actions[i].add();
    					}
                        p.then(function (){
                            setTimeout(function (){
                                that.takeTurnSave(actions,++i,promise);
                            },200);
                        });
                    }else{
                        promise.resolve();
                    }
                    
                }
            },


			setScheduleActionId: function (id) {
				this.each(function (model) {
					model.setScheduleId(id);
				});
			},

			deepCopy: function () {
				return this.map(function (scheduleAction) {
					return scheduleAction.deepCopy();
				});
			}
		});
	});

	return App.Models;
});