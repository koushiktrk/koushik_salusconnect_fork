"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked"
], function (App, P, AylaConfig, AylaBackedMixin) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.GroupModel = B.Model.extend({
			defaults: {
				name: null,
				key: null,
				device_count: null,
				devices: []
			},

			idAttribute: "key",

			initialize: function () {
				this.listenTo(this, "created", this._onCreated);
			},

			add: function () {
				var that = this;

				var data = {},
					name = this.get("name"),
					devices = this.get("devices");

				if (name) {
					data.name = name;
				} else {
					P.reject("Not valid name");
				}

				if (devices.length > 0) {
					data.devices = {
						device_id: devices
					};
				} else {
					return P.reject("Need at least one device for groups");
				}

				data = this.wrapApiData(data);

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.groups.create, data, "POST").then(function (devData) {
					that.set(devData.group);
					that.trigger("created", that);
				});
			},

			refresh: function (isLowPriority) {
				this._detailPromise = this.fetch({isLowPriority: isLowPriority});

				return this._detailPromise;
			},

			/**
			 *  adds a device to this group
			 * @param deviceData can be passed as a backbone model for a device or just the device key
			 */
			addDevice: function (deviceData) {
				var that = this;

				var payload = {	device_id: this._parseDeviceData(deviceData) },
					urlModel = this.toJSON();

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.groups.addDevice)(urlModel), payload, "POST").then(function (groupData) {
					that.set(that.aParse(groupData));
				});
			},

			/**
			 *  remove a device from this group
			 * @param deviceData can be passed as a backbone model for a device or just the device key
			 */
			removeDevice: function (deviceData) {
				var that = this;

				var urlModel = this.toJSON();

				urlModel.device_id = this._parseDeviceData(deviceData);

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.groups.removeDevice)(urlModel), null, "DELETE").then(function (groupData) {
					that.set(that.aParse(groupData));
				});
			},

			updateName: function (groupName) {
				var that = this;

				var payload = { "group": { "name": groupName }},
						urlModel = this.toJSON();

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.groups.put)(urlModel), payload, "PUT").then(function (groupData) {
					that.set(that.aParse(groupData));
				});
			},

			unregister: function () {
				var that = this;

				var urlModel = this.toJSON();

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.groups.remove)(urlModel), null, "DELETE", "text").then(function (){
					that.trigger("sync", that);
				});
			},

			aParse: function (data) {
				var out, devices, deviceIds, group;

				if (data.group) {
					group = data.group;
				} else {
					group = data;
				}

				devices = group.devices || [];
				deviceIds = devices.map(function (device) {
					return device.key;
				});

				out = {
					name: group.name,
					key: group.key,
					device_count: group.device_count,
					devices: deviceIds
				};

				return out;
			},

			isLoaded: function () {
				return this._detailPromise && this._detailPromise.isLoaded();
			},

			hasDevice: function (deviceData) {
				var key = this._parseDeviceData(deviceData);
				return _.indexOf(this.get("devices"), key) !== -1;
			},

			_onCreated: function () {
				this.refresh();
			},

			/**
			 *  accepts both primitive type device key or the device model,
			 *  returns the key for use by the group
			 * @param deviceData backbone model or primitive device key
			 */
			_parseDeviceData: function (deviceData) {
				return deviceData instanceof B.Model ? deviceData.get('key') : deviceData;
			}
		}).mixin([AylaBackedMixin], {
			fetchUrl: AylaConfig.endpoints.groups.load,
			apiWrapperObjectName: "group"
		});
	});

	return App.Models.GroupModel;
});