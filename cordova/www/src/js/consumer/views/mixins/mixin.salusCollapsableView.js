"use strict";

define([
	"app",
	"collapse",
	"consumer/views/mixins/mixin.salusView"
], function (App, bootstrapCollapse, SalusViewMixin) {

	App.module("Consumer.Views.Mixins", function (Mixins) {
		Mixins.SalusCollapsableViewMixin = function () {
			this.mixin([SalusViewMixin]);

			this.setDefaults({
				isCollapsed: function () {
					return !this.$el.hasClass("in");
				},
				collapseOrExpand: function (shouldCollapse) {
					this.$el.collapse(shouldCollapse ? "hide" : "show");
				}
			});

			this.after("initialize", function () {
				this.$el.addClass("collapse");
				this.collapseOrExpand(!this.options.startExpanded);
			});
		};
	});

	return App.Consumer.Views.Mixins.SalusCollapsableViewMixin;
});
