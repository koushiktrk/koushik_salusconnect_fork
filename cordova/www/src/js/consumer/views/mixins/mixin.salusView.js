"use strict";

/**
 * The salus view is the basic view implementation that should be used by all views in the project. It is intended to be
 * mixed-in to the marionette view object and has a few basic responsibilities:
 * - hold boilerplate view code
 * - manage i18next replacement on the template
 * - intercept all a tag click events and uses the backbone router if the route in our domain
 */
define([
	"app",
	"underscore",
	"common/view/SpinnerPage.view"
], function (App, _, SpinnerView) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $) {
		Mixins.SalusView = function () {
			this.setDefaults({
				"_notImplementedAlert": function (evt) {
					window.alert("This functionality is yet to be implemented.");
					if (evt) {
						evt.stopPropagation();
					}
				},

				/**
				 * @param options
				 * 		isLight: spinner is light if true DEFAULT: dark
				 * 		textKey - key of text to be displayed, string  DEFAULT: Loading... - use "none" for no text
				 * 		greyBg : bool - show opaque bg color grey if true - default off
				 * @param $anchorEl: dom element to prepend the spinner to  DEFAULT: this.$el
				 */
				showSpinner: function (options, $anchorEl) {
					options = options || {};
					this.$anchorEl = $anchorEl || this.$el;

					if (!this.spinnerView) {
						this.spinnerView = new SpinnerView(options);
					}

					this.$spinnerView = this.spinnerView.render().$el;
					this.$anchorEl.prepend(this.$spinnerView);
					this.spinning = true;
				},

				hideSpinner: function () {
					if (this.spinnerView) {
						this.spinnerView.destroy();
						this.spinnerView = null;
						this.spinning = false;
					}
				}
			});

			this.after('_renderTemplate', function () {
				if (this.$el.children().length) {
					this.$el.i18n();
				}

				this.$("a").on("click", function (e) {
					var href = $(e.currentTarget).attr("href");

					// if the href is a hash based route with bb-tab as the beginning of the route
					// don't navigate its a bootstrap header
					if (href.indexOf("#bb-tab") === 0) {
						e.preventDefault();
					} else if (href.charAt(0) === '/') {
						App.navigate(href);
						e.preventDefault();
					} else if (href === "#noOp") {
						e.preventDefault();
					} else if ($(e.currentTarget).attr("target") === "_blank") {
						e.preventDefault();
						window.open(href, '_system');
					}
				});
			});

			this.after("destroy", function () {
				this.$("a").off();

				if (this.$spinnerView) {
					this.$spinnerView.off();
				}

				if (this.spinnerView) {
					this.spinnerView.destroy();
				}
			});
			
			this.before("destroy", function () {
				if (this.unstickit) {
					this.unstickit();
				}
			});

			this.after('render', function () {
				if (this.bindings) {
					this.stickit();
				}
			});
		};
	});

	return App.Consumer.Views.Mixins.SalusView;
});
