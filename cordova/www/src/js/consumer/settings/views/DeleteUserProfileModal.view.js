"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view",
	"common/AylaConfig"
], function (App, consumerTemplates, SalusViewMixin, SalusPrimaryButtonView, ModalCloseButton, AylaConfig) {
	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.DeleteUserProfileModalView = Mn.LayoutView.extend({
			className: "modal-body",
			regions: {
				leftBtnRegion: "#bb-left-btn-area",
				rightBtnRegion: "#bb-right-btn-area",
				errorMsgRegion: "#bb-error-msg"
			},
			ui: {
				"errorMsg" : "div.remove-user-error"
			},
			template: consumerTemplates["settings/profile/removeUserProfile"],
			initialize: function () {
				_.bindAll(this, "handleDeleteClick", "handleCancelClick");

				this.deleteBtn = new SalusPrimaryButtonView({
					id: "delete-equip-btn",
					classes: "btn-danger width100 pull-right",
					buttonTextKey: "common.labels.delete",
					clickedDelegate: this.handleDeleteClick
				});

				this.cancelBtn = new ModalCloseButton({
					id: "delete-equip-btn",
					classes: "width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.handleCancelClick
				});
			},
			handleDeleteClick: function () {
				var that = this;

				this.showSpinner();
				App.salusConnector.makeAjaxCall(AylaConfig.endpoints.userProfile.delete, null, "DELETE", "text").then(function () {
					that.ui.errorMsg.addClass("hidden");
					// hiding spinner is done by page reload on logout
					App.logout();
				}).catch(function (error) {
					// error has occurred trying to delete user profile
					App.error("Failed to delete user profile: " + error);
					that.hideSpinner();
					that.ui.errorMsg.removeClass("hidden");
				});
			},
			handleCancelClick: function() {
				App.hideModal();
			},
			onRender: function () {
				this.leftBtnRegion.show(this.cancelBtn);
				this.rightBtnRegion.show(this.deleteBtn);
			}
		}).mixin([SalusViewMixin]);

		return App.Consumer.Settings.Views.DeleteUserProfileModalView;
	});
});