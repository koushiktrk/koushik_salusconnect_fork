"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/settings/views/DeviceList.view",
	"common/model/ayla/Share.model"
], function (App, consumerTemplates, SalusView, RegisteredRegions) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.SharesLayout  = Mn.LayoutView.extend({
			template: consumerTemplates["settings/manageDevices/deviceList"],
			regions: {
				"existingShares": ".bb-existing-shares",
				"newShares": ".bb-new-shares"
			},
			events: {
				"click .bb-add-device": "handleAddDeviceClick"
			},
			ui: {
				"newDeviceForm": ".bb-new-device"
			},
			initialize: function () {
				_.bindAll(this, "handleAddDeviceClick");

				this.registerRegion("existingShares", new Views.DeviceListView({
					collection: App.salusConnector.getShareCollection()
				}));
			},
			handleAddDeviceClick: function () {
				if (!this.newShareForm) {
					this.newShareForm = new Views.DeviceForm({
						model: new App.Models.ShareModel(),
						startExpanded: true,
						className: "device-form edit-enabled",
						mode: "create"
					});
					this.newShares.show(this.newShareForm);
					this.listenTo(this.newShareForm, "cancel:click", this.closeDeviceForm);
					this.listenTo(this.newShareForm, "save:success", this.closeDeviceForm);
				}
			},
			closeDeviceForm: function () {
				if (this.newShareForm) {
					this.newShareForm.destroy();
					this.newShareForm = null;
				}
			}
		}).mixin([SalusView, RegisteredRegions], {});
	});

	return  App.Consumer.Settings.Views.SharesLayout;
});
