"use strict";

/**
 *
 * Helper class for anything involving the user
 *
 **/
define([], function () {
	var UserUtils = {};

	/**
	 * Sanitizes the user input to make it safe
	 **/
	UserUtils.sanitizeInputs = function (userInput) {
		// todo: finish this method

		return userInput;
	};

	return UserUtils;
});