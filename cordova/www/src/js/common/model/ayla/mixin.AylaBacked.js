"use strict";

/**
 * This mixin is for all models backed by Ayla
 */
define([
	"app",
	"underscore",
	"backbone",
	"bluebird",
	"common/AylaConfig"
], function (App, _, B, P, AylaConfig) {

	/**
	 * build defaults allows us to augment the various URL patterns onto defaults.
	 * @param options
	 * @returns {{}}
	 */
	var buildDefaults = function (options) {
		var out = {}, fetch, save, create;
		if (options.fetchUrl) {
			fetch = _.template(options.fetchUrl);					// the fetch URL
			save = options.saveUrl ? _.template(options.saveUrl) : fetch;			// the save url (or use get url if no save is provided)
			create = options.createUrl ? _.template(options.createUrl) : save; 	// the create url (or use get url if no save is provided)

			out._fetchUrl = fetch;
			out._saveUrl = save;
			out._createUrl = create;
		}

		// true by default, pass false if any fetch or save call does not require the user to be signed in
		out.clobberParse = options.hasOwnProperty("clobberParse") ? options.clobberParse : true;

		return out;
	};


	/**
	 * wires up a model to be backed by Ayla. This adds helpers to manage a lot of the
	 * data syncing and authentication
	 *
	 * @param options an object with configuration settings for the mixin
	 *        unpromisable: true|false -- prevent this model from being constructed with a promise return.
	 */
	var AylaModel = function (options) {
		this.mixin([

		], options);

		this.before("initialize", function () {
			this._apiWrapperObjectName = options.apiWrapperObjectName;
			this._detailPromise = null;

			_.bindAll(this, "_onPersistError", "_getResolvedPromise");

			return this;
		});

		/**
		 * before fetch set up the URL. If the object defines its own beforeFetch function call that too
		 */
		this.before("fetch", function () {
			this.url = AylaConfig.buildLocalURLForAyla(this._prepareUrl(this._fetchUrl));
			if (this._beforeFetch) {
				this._beforeFetch();
			}
		});

		/**
		 * before save set up the URL.
		 */
		this.before("save", function () {
			this.url = AylaConfig.buildLocalURLForAyla(this._prepareUrl(this._saveUrl));
		});

		var defaults = {
			// how do we know if this is defined on the server...
			apiCreatedAttribute: options.apiCreatedAttribute || "created_at",

			// properties we need locally that should not be sent to the server
			_localOnlyProperties: options.localOnlyProperties || [],

			// track the properties we have seen from the server to help decide if a property change is an update
			// or create
			//TODO: this is still not sufficient
			_serverAttributes: null,

			/**
			 * get the model to pass to rendering the URL
			 * @returns {{}}
			 * @private
			 */
			_getUrlRenderModel: function () {
				var model = {};
				if (this.idAttribute) {
					model.id = this.get(this.idAttribute);
				}
				return model;
			},

			/**
			 * build out the URL for getting to this object (handel things like ID variables and such)
			 * @param url
			 * @returns {*}
			 * @private
			 */
			_prepareUrl: function (url) {
				return _.isFunction(url) ? url(this._getUrlRenderModel()) : url;
			},

			/**
			 * helper to allow us to return a promise instead of an object for lazy loaded objects
			 * @param opts
			 * @returns {*}
			 */
			getReturnValue: function (opts) {
				if (opts.asPromise && !options.unpromisable) {
					return this.getAsPromise();
				}
				return this;
			},

			//TODO Enable this once we sort out how to get and use the correct refresh. If we try to run .refresh on a model, we might get the one here or in metadata properties or base device!
			refresh: function () {
				this._detailPromise = this.fetch({parse: true});
				return this._detailPromise;
			},

			_getResolvedPromise: function () {
				return P.resolve(this);
			},
			/**
			 * get this object as a promise. This is useful for objects that are constructed and then
			 * loaded from a remote API. By requesting an existing object as a promise you avoid needing
			 * to check and detect if the object has been loaded - as the promise will only return
			 * once the object is loaded.
			 * Note: in the event that the object is already loaded this may be synchronous, otherwise
			 * it is likely to be async.
			 * @returns {*}
			 */
			getAsPromise: function () {
				var promise;
				if (!!this._detailPromise && this._detailPromise.isPending()) {
					promise = this._detailPromise;
				} else if (!this.isLoaded()) {
					promise = this.refresh();
				} else {
					promise = this._getResolvedPromise();
				}

				return promise;
			},
			/**
			 * default is-loaded implementation. This must be overridden by the implementing class for
			 * processable objects
			 */
			isLoaded: function () {
				throw new Error("isLoaded method not defined on implementing object");
			},

			/**
			 * DEPRECATED!!!! TODO REFACTOR
			 * run save and give us a promise
			 * @param runSaveFunc
			 * @returns {*}
			 */
			saveWithPromise: function (runSaveFunc) {
				var that = this;
				var promise = new P.Promise(function (resolve, reject) {
					that.once("sync", function () {
						that.clearErrors();
						resolve(that);
					});
					runSaveFunc(resolve, reject);
				}).timeout(AylaConfig.stdTimeoutMs);
				return promise;
			},

			clearErrors: function (field) {
				if (field && this._errors) {
					if (this._errors[field]) {
						this._errors[field] = [];
						this.trigger("clear:error:" + field);
					}
				} else {
					this._errors = {};
				}
			},

			setFieldError: function (field, errKey) {
				if (!this._errors) {
					this._errors = {};
				}

				if (!this._errors[field]) {
					this._errors[field] = [];
				}
				this._errors[field].push(errKey);

				this.trigger("error:" + field, errKey);
				this.trigger("error");
			},

			/**
			 * this is a wrapper for save handling the fact that we need to pre-transform our data before saving
			 */
			persist: function () {
				var that = this;
				return this.saveWithPromise(function () {
					var model = that.renderModelForPersist();
					that.save(null, {data: JSON.stringify(model), error: that._onPersistError});
				});
			},

			_onPersistError: function (model, xhr /*, reqOpts*/) {
				var that = this;
				var respStr = xhr.responseText;
				var respJson;

				try {
					respJson = JSON.parse(respStr);
				} catch (e) {}// eat for now

				if (respJson) {
					// clear out all old errors prior to setting up new ones
					this.clearErrors();
					//iterate over each key, then over each value of the array attached to each key
					_.each(respJson, function (errs, fieldName) {
						var firstKey;
						_.each(errs, function (errText, key) {

							// if there is an array of errors, we use a different key
							// todo: verify this is true
							if (_.isArray(errText)) {
								fieldName = key;
								errText = errText[0];
							}

							// strip out all non-word characters so that localization keys are only a-z0-9
							var keyString = errText.toLowerCase().replace(/\W/gmi, "");
							if (!firstKey) {
								firstKey = keyString;
							} else if (keyString === "isinvalid") {
								return; // skip this unless it is the only one
							}
							that.setFieldError(fieldName, "common.validation.ayla." + keyString);
						});
					});
				}
			},
			wrapApiData: function (data) {
				var wrapperName = this._apiWrapperObjectName;
				var outObj = data;	// default case
				/**
				 * ayla api expects things to look like {user: {<user:props>}} rather then {<user:props>}
				 */
				if (wrapperName) {
					outObj = {};
					outObj[wrapperName] = data;
				}

				return outObj;
			},
			renderModelForPersist: function () {
				return this.renderModelForPersistBase();
			},
			/**
			 * render the ayla submitted model ofthe JSON. The big issue here is that ayla often has the form:
			 * {entityName: {property:List}} rather then {property:list} for their api pattern
			 * @returns {*}
			 */
			renderModelForPersistBase: function () {
				var omitObj = _.union(this._localOnlyProperties, this._metaDataProperties);
				var obj = _.omit(this.toJSON(), omitObj); // remove properties that should not go to server

				return this.wrapApiData(obj);
			}
		};

		_.extend(defaults, buildDefaults(options));
		this.setDefaults(defaults);
		
		this.before("destroy", function () {
			if (this.onCleanUp) {
				this.onCleanUp();
			}
		});

		/**
		 * we don't have server IDs often, so use this instead. if apiCreatedAttribute === true, assume created
		 */
		this.clobber("isNew", function () {
			if (true === this.apiCreatedAttribute) {
				return false;
			}
			return !this.has(this.apiCreatedAttribute);
		});

		/**
		 * it is extremely common for ayla to wrap responses the same way they need to be wrapped going up:
		 * eg: {user: {stuff }}. So try a default unwrap here
		 * @param data
		 * @returns {*}
		 */
		this.clobber("parse", function (data, options) {
			var out;
			if (!_.isFunction(this.aParse)) {
				if (_.has(data, this._apiWrapperObjectName)) {
					out = data[this._apiWrapperObjectName];
				} else {
					out = data;
				}
			} else {
				out = this.aParse(data, options);
			}

			this._serverAttributes = _.keys(out);
			return out;
		});

		/**
		 * in order to queue up fetches they are moved off to another thread which waits for the user model to be
		 * populated, thus ensuring a correct user is logged in.
		 */
		this.clobber("sync", function () {
			if (this.clobberSync) {
				var that = this;

				return App.salusConnector.getSessionUserPromise().then(function () {
					return B.sync.apply(that, arguments);
				});
			} else {
				return B.sync.apply(this, arguments);
			}
		});
	};

	return AylaModel;
});
