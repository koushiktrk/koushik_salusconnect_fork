# You may add here your
# server {
#	...
# }
# statements for each of your virtual hosts to this file

##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# http://wiki.nginx.org/Pitfalls
# http://wiki.nginx.org/QuickStart
# http://wiki.nginx.org/Configuration
#
# Generally, you will want to move this file somewhere, and start with a clean
# file but keep this around for reference. Or just disable in sites-enabled.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

upstream ayla_user_endpoint {
  server user.aylanetworks.com:443;
}

upstream ayla_device_endpoint {
  server ads-dev.aylanetworks.com:443;
}

upstream salus_user_endpoint {
  server salus-web-services.appspot.com:443;
}

upstream salus_aws_endpoint {
  server ec2-52-25-165-98.us-west-2.compute.amazonaws.com:443;
}

# settings the dev environment to eu for the use of computime employees since most gateways are on the eu silo
# this endpoint should point to US dev server in a correct world
upstream salus_connect_endpoint {
  server eu.salusconnect.io:443;
}


upstream eu-salus-web-svc_endpoint {
	server eu-salus-web-svc.appspot.com:443;
}


server {
	#Vagrant share_folders and Nginx sendfile don't play nicely. Turning if off.
	sendfile off;

	listen 80;

	index index.html;

	# Make site accessible from http://localhost/
	server_name localhost;
	# server_name localhost-salus-connected.digitalfoundry.com;

    # Enables cross-origin requests to Ayla user endpoints
    # Matches a url like this "user.aylanetworks.com/users/sign_in.json" or
    # like this "user.aylanetworks.com/users.json",
    # match is referenced as $1
    location ~^/user.aylanetworks.com/(.*)$ {
      proxy_pass https://ayla_user_endpoint/$1?$args;
      proxy_set_header Host user.aylanetworks.com;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      expires -1;
    }

    # Enables cross-origin requests to Ayla device endpoints
    # TODO there is a known issue where certain adblock browser extensions
    # will block calls to ads-dev if they detect ".com/ads".
    # This will affect dev and prod urls
    location ~^/ads-dev.aylanetworks.com/(.*)$ {
      proxy_pass https://ayla_device_endpoint/$1?$args;
      proxy_set_header Host ads-dev.aylanetworks.com;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      expires -1;
    }

    # Enables cross-origin requests to Salus web services endpoints (a wrapper for Ayla calls)
    location ~^/salus-web-services.appspot.com/(.*)$ {
      proxy_pass https://salus_user_endpoint/$1?$args;
      proxy_set_header Host salus-web-services.appspot.com;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      expires -1;
    }

    # Enables cross-origin requests to Salus web services endpoints (a wrapper for Ayla calls)
    location ~^/eu-salus-web-svc.appspot.com/(.*)$ {
      proxy_pass https://eu-salus-web-svc_endpoint/$1?$args;
      proxy_set_header Host eu-salus-web-svc.appspot.com;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      expires -1;
    }

     # Enables cross-origin requests to Salus web services endpoints (a wrapper for Ayla calls)
    location ~^/ec2.com/(.*)$ {
      proxy_pass https://salus_aws_endpoint/$1?$args;
      proxy_set_header Host ec2-52-25-165-98.us-west-2.compute.amazonaws.com;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      expires -1;
    }

    # Enables cross-origin requests to Salus web services endpoints (a wrapper for Ayla calls)
    location ~^/salusconnect.io/(.*)$ {
      proxy_pass https://salus_connect_endpoint/$1?$args;
      proxy_set_header Host eu.salusconnect.io;
      proxy_set_header X-Real-IP $remote_addr;
      #proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for; #Disabled as eu.salusconnect.io will set the x-forward-for header when connecting to ayla.
      expires -1;
    }

    location / {
        # First attempt to serve request as file, then
        # as directory, then load the index.html then fall back to displaying a 404.
        try_files $uri $uri/ /index.html =404;
        # Uncomment to enable naxsi on this location
        # include /etc/nginx/naxsi.rules
        root /vagrant/web;
    }

    # all locations under the following base directories
    location ~ ^/(css/|fonts/|images/|js/|translations/|js/) {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ =404;
        root /vagrant/web;
        expires -1;
    }
	
	#Don't want to cache the index.html file.
	location ~ ^/index.html$ {
        try_files $uri $uri/ =404;
        root /vagrant/web;
        expires -1;
    }
}


